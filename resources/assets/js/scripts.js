/*jshint browser:true, devel:true, unused:false */
/*global google */
;(function($) {

"use strict";
$('.new_post').css('display', 'none');

var $body = $('body');
var body = $('body');
// var $head = $('head');
// var $mainWrapper = $('#main-wrapper');

var contact = $(body).find('.contact-button');
var contactWindow = $(contact).find('.contact-details');



$(contact).on('click', function(e){
  $(this).find(contactWindow).toggle();
  e.preventDefault();
});

var share = $(body).find('.share-button');
var shareWindow = $(share).find('.contact-share');

$(share).on('click', function(e){
  $(this).find(shareWindow).toggle();
  e.preventDefault();
});


$('.slider-range-container').each(function(){
    if ( $.fn.slider ) {

      var self = $(this),
      slider = self.find( '.slider-range' ),
      min = slider.data( 'min' ) ? slider.data( 'min' ) : 100,
      max = slider.data( 'max' ) ? slider.data( 'max' ) : 2000,
      step = slider.data( 'step' ) ? slider.data( 'step' ) : 100,
      default_min = slider.data( 'default-min' ) ? slider.data( 'default-min' ) : 100,
      default_max = slider.data( 'default-max' ) ? slider.data( 'default-max' ) : 500,
      currency = slider.data( 'currency' ) ? slider.data( 'currency' ) : '$',
      input_from = self.find( '.range-from' ),
      input_to = self.find( '.range-to' );

      input_from.val( currency + ' ' + default_min );
      input_to.val( currency + ' ' + default_max );

      slider.slider({
        range: true,
        min: min,
        max: max,
        step: step,
        values: [ default_min, default_max ],
        slide: function( event, ui ) {
          input_from.val( currency + ' ' + ui.values[0] );
          input_to.val( currency + ' ' + ui.values[1] );
        }
      });
    }
});

$('.custom-select').select2();

 
// Mediaqueries
// ---------------------------------------------------------
// var XS = window.matchMedia('(max-width:767px)');
// var SM = window.matchMedia('(min-width:768px) and (max-width:991px)');
// var MD = window.matchMedia('(min-width:992px) and (max-width:1199px)');
// var LG = window.matchMedia('(min-width:1200px)');
// var XXS = window.matchMedia('(max-width:480px)');
// var SM_XS = window.matchMedia('(max-width:991px)');
// var LG_MD = window.matchMedia('(min-width:992px)');







// Touch
// ---------------------------------------------------------
var dragging = false;

$body.on('touchmove', function() {
	dragging = true;
});

$body.on('touchstart', function() {
	dragging = false;
});



// Set Background Image
// ---------------------------------------------------------
$('.has-bg-image').each(function () {
  var $this = $(this),

      image = $this.data('bg-image'),
      color = $this.data('bg-color'),
      opacity = $this.data('bg-opacity'),

      $content = $('<div/>', { 'class': 'content' }),
      $background = $('<div/>', { 'class': 'background' });

  if (opacity) {
    $this.children().wrapAll($content);
    $this.append($background);

    $this.css({
      'background-image': 'url(' + image + ')'
    });

    $background.css({
      'background-color': '#' + color,
      'opacity': opacity
    });
  } else {
    $this.css({
      'background-image': 'url(' + image + ')',
      'background-color': '#' + color
    });
  }
});



// Superfish Menus
// ---------------------------------------------------------
if ($.fn.superfish) {
  $('.sf-menu').superfish();
} else {
  console.warn('not loaded -> superfish.min.js and hoverIntent.js');
}



// Mobile Sidebar
// ---------------------------------------------------------
$('.mobile-sidebar-toggle').on('click', function () {
  $body.toggleClass('mobile-sidebar-active');
  return false;
});

$('.mobile-sidebar-open').on('click', function () {
  $body.addClass('mobile-sidebar-active');
  return false;
});

$('.mobile-sidebar-close').on('click', function () {
  $body.removeClass('mobile-sidebar-active');
  return false;
});



// UOU Tabs
// ---------------------------------------------------------
if ($.fn.uouTabs) {
  $('.uou-tabs').uouTabs();
} else {
  console.warn('not loaded -> uou-tabs.js');
}



// UOU Accordions
// ---------------------------------------------------------
if ($.fn.uouAccordions) {
  $('.uou-accordions').uouAccordions();
} else {
  console.warn('not loaded -> uou-accordions.js');
}



// UOU Alers
// ---------------------------------------------------------
$('.alert').each(function () {
  var $this = $(this);

  if ($this.hasClass('alert-dismissible')) {
    $this.children('.close').on('click', function (event) {
      event.preventDefault();

      $this.remove();
    });
  }
});





// Placeholder functionality for selects (forms)
// ---------------------------------------------------------
function selectPlaceholder(el) {
  var $el = $(el);

  if ($el.val() === 'placeholder') {
    $el.addClass('placeholder');
  } else {
    $el.removeClass('placeholder');
  }
}

$('select').each(function () {
  selectPlaceholder(this);
}).change(function () {
  selectPlaceholder(this);
});





// ---------------------------------------------------------
// BLOCKS
// BLOCKS
// BLOCKS
// BLOCKS
// BLOCKS
// ---------------------------------------------------------





// .uou-block-1a
// ---------------------------------------------------------
$('.uou-block-1a').each(function () {
  var $block = $(this);

  // search
  $block.find('.search').each(function () {
    var $this = $(this);

    $this.find('.toggle').on('click', function (event) {
      event.preventDefault();
      $this.addClass('active');
      setTimeout(function () {
        $this.find('.search-input').focus();
      }, 100);
    });

    $this.find('input[type="text"]').on('blur', function () {
      $this.removeClass('active');
    });
  });

  // language
  $block.find('.language').each(function () {
    var $this = $(this);

    $this.find('.toggle').on('click', function (event) {
      event.preventDefault();

      if (!$this.hasClass('active')) {
        $this.addClass('active');
      } else {
        $this.removeClass('active');
      }
    });
  });
});



// .uou-block-1b
// ---------------------------------------------------------
$('.uou-block-1b').each(function () {
  var $block = $(this);

  // language
  $block.find('.language').each(function () {
    var $this = $(this);

    $this.find('.toggle').on('click', function (event) {
      event.preventDefault();

      if (!$this.hasClass('active')) {
        $this.addClass('active');
      } else {
        $this.removeClass('active');
      }
    });
  });
});



// .uou-block-1e
// ---------------------------------------------------------
$('.uou-block-1e').each(function () {
  var $block = $(this);

  // language
  $block.find('.language').each(function () {
    var $this = $(this);

    $this.find('.toggle').on('click', function (event) {
      event.preventDefault();

      if (!$this.hasClass('active')) {
        $this.addClass('active');
      } else {
        $this.removeClass('active');
      }
    });
  });
});



// .uou-block-5b
// ---------------------------------------------------------
$('.uou-block-5b').each(function () {
  var $block = $(this),
      $tabs = $block.find('.tabs > li');

  $tabs.on('click', function () {
    var $this = $(this),
        target = $this.data('target');

    if (!$this.hasClass('active')) {
      $block.find('.' + target).addClass('active').siblings('blockquote').removeClass('active');

      $tabs.removeClass('active');
      $this.addClass('active');

      return false;
    }
  });
});



// .uou-block-5c
// ---------------------------------------------------------
$('.uou-block-5c').each(function () {
  var $block = $(this);

  if ($.fn.flexslider) {
    $block.find('.flexslider').flexslider({
      slideshowSpeed: 10000,
      animationSpeed: 1000,
      prevText: '',
      nextText: '',
      controlNav: false,
      smoothHeight: true
    });
  } else {
    console.warn('not loaded -> jquery.flexslider-min.js');
  }
});



// .uou-block-7g
// ---------------------------------------------------------
$('.uou-block-7g').each(function () {
  var $block = $(this),
      $badge = $block.find('.badge'),
      badgeColor = $block.data('badge-color');

  if (badgeColor) {
    $badge.css('background-color', '#' + badgeColor);
  }
});



// .uou-block-7h
// ---------------------------------------------------------
$('.uou-block-7h').each(function () {
  var $block = $(this);

  if ($.fn.flexslider) {
    $block.find('.flexslider').flexslider({
      slideshowSpeed: 10000,
      animationSpeed: 1000,
      prevText: '',
      nextText: '',
      directionNav: false,
      smoothHeight: true
    });
  } else {
    console.warn('not loaded -> jquery.flexslider-min.js');
  }
});



// .uou-block-11a
// ---------------------------------------------------------
$('.uou-block-11a').each(function () {
  var $block = $(this);

  // nav
  $block.find('.main-nav').each(function () {
    var $this = $(this).children('ul');

    $this.find('li').each(function () {
      var $this = $(this);

      if ($this.children('ul').length > 0) {
        $this.addClass('has-submenu');
        $this.append('<span class="arrow"></span>');
      }
    });

    var $submenus = $this.find('.has-submenu');

    $submenus.children('.arrow').on('click', function (event) {
      var $this = $(this),
          $li = $this.parent('li');

      if (!$li.hasClass('active')) {
        $li.addClass('active');
        $li.children('ul').slideDown();
      } else {
        $li.removeClass('active');
        $li.children('ul').slideUp();
      }
    });
  });
});








// .uou-block-12a
// ---------------------------------------------------------
$('.uou-block-12a').each(function () {
  var $block = $(this),
      $map = $block.find('.map-container .map');

  // Map
  var map,
      height = $map.data('height'),
      centerLat = $map.data('center-lat'),
      centerLng = $map.data('center-lng');

  $map.css('height', height + 'px');

  function initialize () {
    var mapOptions = {
      scrollwheel: false,
      zoom: 14,
      center: new google.maps.LatLng(centerLat, centerLng)
    };

    map = new google.maps.Map($map[0], mapOptions);

    for (var i = 0; i < markers.length; i++) {
      var marker = markers[i];

      new google.maps.Marker({
        position: new google.maps.LatLng(marker.lat, marker.lng),
        map: map,
        title: marker.title
      });
    }
  }

  google.maps.event.addDomListener(window, 'load', initialize);

  google.maps.event.addDomListener(window, 'resize', function () {
    var center = map.getCenter();
    google.maps.event.trigger(map, 'resize');
    map.setCenter(center);
  });
});



// .uou-block-12b
// ---------------------------------------------------------
$('.uou-block-12b').each(function () {
  var $block = $(this),
      $map = $block.find('.map-container .map');

  // Map
  var map,
      height = $map.data('height'),
      centerLat = $map.data('center-lat'),
      centerLng = $map.data('center-lng');

  $map.css('height', height + 'px');

  function initialize () {
    var mapOptions = {
      scrollwheel: false,
      zoom: 14,
      center: new google.maps.LatLng(centerLat, centerLng)
    };

    map = new google.maps.Map($map[0], mapOptions);

    for (var i = 0; i < markers.length; i++) {
      var marker = markers[i];

      new google.maps.Marker({
        position: new google.maps.LatLng(marker.lat, marker.lng),
        map: map,
        title: marker.title
      });
    }

  }

  google.maps.event.addDomListener(window, 'load', initialize);

  google.maps.event.addDomListener(window, 'resize', function () {
    var center = map.getCenter();
    google.maps.event.trigger(map, 'resize');
    map.setCenter(center);
  });
});



// .uou-block-12c
// ---------------------------------------------------------
$('.uou-block-12c').each(function () {
  var $block = $(this),
      $map = $block.find('.map-container .map');

  // Map
  var map,
      height = $map.data('height'),
      centerLat = $map.data('center-lat'),
      centerLng = $map.data('center-lng');

  $map.css('height', height + 'px');

  function initialize () {
    var mapOptions = {
      scrollwheel: false,
      zoom: 14,
      center: new google.maps.LatLng(centerLat, centerLng)
    };

    map = new google.maps.Map($map[0], mapOptions);

    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(centerLat, centerLng),
      map: map,
      title: ''
    });
  }

  google.maps.event.addDomListener(window, 'load', initialize);

  google.maps.event.addDomListener(window, 'resize', function () {
    var center = map.getCenter();
    google.maps.event.trigger(map, 'resize');
    map.setCenter(center);
  });
});


}(jQuery));







// uou-toggle-content
$('.content-main h6 a').on('click', function(e){
    e.preventDefault();
    $(this).toggleClass('active');
    $(this).parent().next(".content-hidden").toggleClass('active');
  });
/*-----------------------------------------------------------------------------------*/
/*  APP LEVEL SCRIPTS
/*-----------------------------------------------------------------------------------*/


// MESSAGES 
$('.show_students').livequery('click', function(data) {
  // body...
  var link = $('.show_students a.sh_st').data('href');
  var status = $('.show_students a.sh_st').data('status');

  if(status=='0'){
    $.get(link, function(data) {
      $('.student-list').html(data);
    });
    $('.show_students a.sh_st').attr('data-status', '1');
  }else{

  }
});


$('.show_messages').livequery('click', function(data) {
  // body...
  var link = $('.show_messages a.sh_mgs').data('href');
  var status = $('.show_messages a.sh_mgs').data('status');
  status = '0';
  if(status=='0'){
    $.get(link, function(data) {
      $('.all-messages').html(data);
    });
    $('.show_messages a.sh_mgs').attr('data-status', '1');
  }
});

$('.show_new_message').livequery('click', function(data) {
  // body...
  var link = $(this).data('href');
   
    $.get(link, function(data) {
      $('.conversations').html(data);
    });
    
  
});


//GET FOLLOWERS LIST
$('.handles').livequery('keypress', function(data) {
  // body...
  var link = $(this).data('href');
  var TextInput = $(this).val();

  
   
    $(this).autocomplete({
                minLength: 1,
                source: 
                function(req, add){
                    $.ajax({
                        url: link,
                        dataType: 'json',
                        type: 'POST',
                        data: { s: TextInput },
                        success:    
                        function(x){
                            if(x.response =="true"){
                                add(x.message);
                            }
                        }
                    });
                },
                select: function(event, ui) {
                  $( "#handleid" ).val( ui.item.handleid );
                  $( ".handles" ).val( ui.item.value );

                  $.get(ui.item.link, function(data) {
                    $('.page_chat').html(data);
                  });
                  return false;
               }
            });

});



$('.message_platform').livequery('click', function(data) {
  // body...
  var link = $(this).data('href');
  var tag = $(this).data('tag');
  var secondary_link = $(this).data('link');

  
 
    $.get(link+'/'+tag, function(data) {
      $('.conversations').html(data);
     $('#msgtext').focus();
    }); 

    setInterval(function(){ 
       $.get(secondary_link+'/'+tag, function(data) {
            $('.chat_holder_div').html(data);
            $('.conversations').scrollTop($('.conversations')[0].scrollHeight); 
          }); 

    }, 2500);
 

});
 


$('.send_message').livequery('click', function(event) {
  /* Act on the event */

  event.preventDefault();

  var $link = $('#reply_form').attr('action');
  //alert($link);
  var $form = $('#reply_form').serializeArray();
  //alert($form);
  $.post($link, $form, function(data) {

    $('.chat').append(data);
  

    $("#msgtext").val('');

    /*optional stuff to do after success */

  });

});

$('#msgtext').livequery('keydown', function(event) {
  /* Act on the event */
  if(event.keyCode == 13 && !event.ctrlKey ){
    event.preventDefault();

    var $link = $('#reply_form').attr('action');
    //alert($link);
    var $form = $('#reply_form').serializeArray();
    //alert($form);
    $.post($link, $form, function(data) {

      $('.chat').append(data);
    

      $("#msgtext").val('');
       $('.chat_holder_div').scrollTop($('.chat_holder_div')[0].scrollHeight); 

      /*optional stuff to do after success */
    });
  }else if(event.keyCode == 13 && event.ctrlKey){
   $(this).val(function(i,val){
            return val + "\n";
        });
  }

});



// SEND INVITES SCRIPTS

$('.send_invite').click(function(event) {
  /* Act on the event */
  var $link = $('#invites').attr('action');
  var $form = $('#invites').serializeArray();
  $.post($link, $form, function(data) {

    $('#invitationsList').prepend(data);
  

    $("#invites").trigger('reset');

    /*optional stuff to do after success */
  });

});

  $('.editThis').livequery('click',function(event) {
 
    event.preventDefault();
    $("#profile-out").fadeOut('slow',function(){ $("#profile-in").fadeIn();});
    $(".editThis").fadeOut();
    $(".saveThis").delay(500).fadeIn().html('Close');



  });



  $('.saveThis').livequery('click',function(event) {
 
    event.preventDefault();
    $("#profile-in").fadeOut('slow',function(){ $("#profile-out").fadeIn();});
    $(".saveThis").fadeOut();
    $(".editThis").delay(500).fadeIn();
  });

  $('.show_details').livequery('click',function(event) {
 
    event.preventDefault();
    
    var link = $(this).data('href');
    $.get(link, function(data) {
      $('#sidebar').html(data);
    });
  });



  $('.revoke').livequery('click',function(event) {
 
    event.preventDefault();
  var $link = $(this).data('href');
  if(confirm('You are about to revoke an Invitation, Proceed?')){
  $.get($link, function(data) {
      if(data=='completed'){
          var $t = $(this);
          var $th = $t.parents();
            $t.parent().html('').delay(1000).fadeOut(500);
            $th.closest('tr').delay(1500).fadeOut(500);
            $('.message').html("<div class='alert alert-success'>Invitation Revoked</div>");

        }else if(data=='failed'){
          $('.message').html("<div class='alert alert-danger'>Could Not Revoke this Invitation</div>");
        }else{
          $('.message').html("<div class='alert alert-info'>Invalid Revoke Operation</div>");

        }
  }) } 
    /*optional stuff to do after success */
  });


  


    var ratingOverall = $('.rating-overall');
    if (ratingOverall.length > 0) {
        ratingOverall.raty({
            path: '<?=site_url()?>assets/img',
            readOnly: true,
            score: function() {
                return $(this).attr('data-score');
            }
        });
    }
    var ratingIndividual = $('.rating-individual');
    if (ratingIndividual.length > 0) {
        ratingIndividual.raty({
            path: '<?=site_url()?>assets/img',
            readOnly: true,
            score: function() {
                return $(this).attr('data-score');
            }
        });
    }
    var ratingUser = $('.rating-user');
    if (ratingUser.length > 0) {
        $('.rating-user .inner').raty({
            path: '<?=site_url()?>assets/img',
            starOff : 'big-star-off.png',
            starOn  : 'big-star-on.png',
            width: 150,
            //target : '#hint',
            targetType : 'number',
            targetFormat : 'Rating: {score}',
            click: function(score, evt) {
                showRatingForm();
            }
        });
    }


//GET FOLLOWERS LIST
$('.saveThisSession').livequery('click', function(data) {
  // body...
  data.preventDefault();
  var link = $("#session_create_form").attr('action');
  var $form = $('#session_create_form').serializeArray();
 
  $.post(link, $form, function(data) {

    $('.student-list').html(data);
    $("#session_create_form").reset();
    /*optional stuff to do after success */
  });


});


$('.editSession').livequery('click', function(e) {
  // body...
  e.preventDefault();
  var link = $(this).data('href');
  
 
  $.get(link, function(data) {

     $(".edit_session").html(data);
    /*optional stuff to do after success */
  });


});


 // $('#examples').DataTable({  "language": {
 //            "lengthMenu": "_MENU_ ",
 //            "zeroRecords": "Nothing found - sorry" }  });

 $('.class_sessions').livequery('click', function (e) {
   // body...
   var link = $(this).data('href');

   $.get(link,function (data) {
     $("#table_body").html(data).fadeIn();
   })
 });

 $('.student_class_sessions').livequery('click', function (e) {
   // body...
   var link = $(this).data('href');

   $.get(link,function (data) {
     $("#sidebar_Page").html(data).fadeIn();
   })
 });
$('#example').livequery('keypress', function (e) {
  // Declare variables
  var input, filter, table, tr, td, i;
  input = document.getElementById("searchProd");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
});


$('.session_name').livequery('change',function(e){
    
      var $sessionid = $(this).val();
      var $link = $(this).data('href')+'/'+$sessionid;
      $('#go').click(function(event) {
          event.preventDefault();
          $.get($link, function(data) {
            $("#mypage").html(data);
          });
    });
});
$('.join_classroom').livequery('click',function(e){
    
          e.preventDefault();
      
      $(this).children('.goto').html("Processing....");

      var $link = $(this).data('href');
      var $this = $(this);
      
      $.ajax({
          url   : $link,
          type  : 'GET',
          cache : false,
          data  : {
            format: 'json'
          },
          
          dataType: 'json',
          success : function(data) {
            var button = data.button;
            var message = data.message;
            var linkback = data.linkback;
            var status = data.status;
            if(status=='1'){

              $this.html(button);
              $this.attr('href', linkback);
              $this.attr('target', '_blank');

            
              $this.removeAttr('data-href');
              $this.removeAttr('data-goto');
              $('.alertInfo').html(message);

            }else if(status=='3'){
              $this.html(button);
              $this.attr('href', linkback);
              $this.attr('target', '_blank');
            
              $this.removeAttr('data-href');
              $this.removeAttr('data-goto');
              $('.alertInfo').html(message);
            }else{
              $(this).attr('href', linkback);
              $('.alertInfo').html(message);
            }

          } }) ;
    
});
$('#addProjectStudent').livequery('click', function(e){
   e.preventDefault();
  var link = $("#myform").attr('action');
  var $form = $('#myform').serializeArray();
  $.post(link, $form, function(data) {
    $("#studentsInSession").html(data);
  });
});


$('.rem').livequery('click', function(e){
   e.preventDefault();
  var link = $(this).data('href');
  if(confirm("Are you sure you want to remove this Student?")){
    $.get(link, function(data) {
      $("#studentsInSession").html(data);
    });
  }else{
    return false;
  }
});

$('.getStudentDetails').livequery('click', function(e){
 
   e.preventDefault();
  var link = $(this).data('href');
  
  $.get(link, function(data) {
    $("#studentProfile").html(data);
  });
});

$('.approveTopic').livequery('click', function(e){
   e.preventDefault();
   var link = $("#approveTopic").attr('action');
  var $form = $('#approveTopic').serializeArray();
  $.post(link, $form, function(data) {
   
    $("#studentProfile").html("<div class='loading'>&nbsp;</div>").delay(50000);
    $("#studentProfile").delay(50000).fadeOut('slow');
    $("#studentProfile").fadeIn('slow').html(data);
    // $("#studentProfile").html(data);
  });
});

$(document).ready( function() {
$("#txtEditor").Editor();                    
$("abbr").timeago();

 $('.create_Post').livequery('click', function (argument) {
  argument.preventDefault();
  $('.post_content').fadeOut();
  $('.new_post').fadeIn();
  $('.create_Post').html('See Posts');
  $('.create_Post').addClass('view_Post');
  $('.create_Post').removeClass('btn-info');
  $('.create_Post').addClass('btn-danger');

  $('.create_Post').removeClass('create_Post');


}); 


  $('.view_Post').livequery('click', function (argument) {
  argument.preventDefault();
  $('.new_post').fadeOut();
  $('.post_content').fadeIn();
  $('.view_Post').html('Create Post');
  $('.view_Post').addClass('create_Post');
  $('.view_Post').removeClass('btn-danger');
  $('.view_Post').addClass('btn-info');
  $('.view_Post').removeClass('view_Post');


}); 


});


