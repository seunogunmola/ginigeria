
(function() {
    "use strict";

    // custom scrollbar

    $("html").niceScroll({styler:"fb",cursorcolor:"#65cea7", cursorwidth: '6', cursorborderradius: '0px', background: '#424f63', spacebarenabled:false, cursorborder: '0',  zindex: '1000'});

    $(".left-side").niceScroll({styler:"fb",cursorcolor:"#65cea7", cursorwidth: '3', cursorborderradius: '0px', background: '#424f63', spacebarenabled:false, cursorborder: '0'});


    $(".left-side").getNiceScroll();
    if ($('body').hasClass('left-side-collapsed')) {
        $(".left-side").getNiceScroll().hide();
    };
	
	
	$('#deletemodal').on('show.bs.modal', function (event) {
	   var button = $(event.relatedTarget) // Button that triggered the modal
	   var id = button.data('id'); // Extract info from data-* attributes
	   var url = button.data('url'); // Extract info from data-* attributes
	   var modal = $(this)
	   
	   modal.find('.modal-title').text('Delete '+button.data('label'));
	   modal.find('#proceed').attr('data-id',id);
	   modal.find('#proceed').attr('data-url',url);
	  
	  });


		$('#proceed').click(function (e) {
			var f = $(this);
			
			f.addClass("disabled");  
			var id = f.attr("data-id");
			var add = f.attr("data-url");
			
			$.ajax({
				url : add,
				type: "POST",
				data : "id="+id,
				success:function(data) 
				{
					if(data!=0)
					{
						$('#deletemodal').modal('hide');
						$("#successmessage").text("Successfully Deleted");
						$("#successbox").fadeIn("slow");
						setTimeout(function(){$("#successbox").fadeOut("slow");}, 3000);
						f.removeClass("disabled fa fa-spinner fa-pulse");                
						$("#navigationttr"+id).fadeOut("slow");
					 } 
				   else 
					{
					  alert("bad request");  
					}
				
			},
				error: function(jqXHR, textStatus, errorThrown) 
				{
					
					alert(errorThrown);
						
				}
			}); 

		});

    $("#addprice").click(function(){
	 //determine the number of trs showing on display page first
	 var filecount =  $("#pricecount").val();
	 var fileselectoptions =  $("#unitid1").html();
	 //increment the filecount to rename the new tr elements
              var count = parseInt(filecount) + 1;
              if (count <= 5)
              {
                  var tr1 = '<tr id="unittr'+count+'"><td><select class="form-control" name="unitid'+count+'">' + fileselectoptions + '</select></td>';
                  var tr2 = '<td><div class="form-group"><div class="col-lg-12"><input type="text" placeholder="Price" class="form-control" name="unitprice'+count+ '" value="" id="unitprice'+count+'" maxlength="10"></div></div></td>';
                  var tr3 = '<td><div class="form-group"><div class="col-lg-12"><input type="text" placeholder="Description" class="form-control" name="unitdescription'+count+'" value="" id="unitdescription'+count+'" maxlength="200"></div></div></td>';
                  var tr4 = '<td><div class="form-group"><div class="col-lg-12"><input type="text" placeholder="Discount" class="form-control" name="unitdiscount'+count+'" value="" id="unitdiscount'+count+'" maxlength="2"></div></div></td></tr>';

                  $("#pricebody").append(tr1 + tr2 + tr3 + tr4);

                  $("#pricecount").val(count);

              }
      });
      
      
      $('#reply').click(function (e) {
       $("#messagebox").toggle(); 
   });
	
 $("#attachmore").click(function(){
	 //determine the number of trs showing on display page first
	 var filecount =  $("#filecount").val();
	 //increment the filecount to rename the new tr elements
     var count = parseInt(filecount) + 1;
        if(count <= 5)
          {
			var filebox = '<input type="file" name="file'+count+ '" class="form-control" id="file'+count+'" />';
            $("#filebody").append(filebox);
            $("#filecount").val(count);
          }
      });
	  
    // Toggle Left Menu
   jQuery('.menu-list > a').click(function() {
      
      var parent = jQuery(this).parent();
      var sub = parent.find('> ul');
      
      if(!jQuery('body').hasClass('left-side-collapsed')) {
         if(sub.is(':visible')) {
            sub.slideUp(200, function(){
               parent.removeClass('nav-active');
               jQuery('.main-content').css({height: ''});
               mainContentHeightAdjust();
            });
         } else {
            visibleSubMenuClose();
            parent.addClass('nav-active');
            sub.slideDown(200, function(){
                mainContentHeightAdjust();
            });
         }
      }
      return false;
   });

   function visibleSubMenuClose() {
      jQuery('.menu-list').each(function() {
         var t = jQuery(this);
         if(t.hasClass('nav-active')) {
            t.find('> ul').slideUp(200, function(){
               t.removeClass('nav-active');
            });
         }
      });
   }

   function mainContentHeightAdjust() {
      // Adjust main content height
      var docHeight = jQuery(document).height();
      if(docHeight > jQuery('.main-content').height())
         jQuery('.main-content').height(docHeight);
   }

   //  class add mouse hover
   jQuery('.custom-nav > li').hover(function(){
      jQuery(this).addClass('nav-hover');
   }, function(){
      jQuery(this).removeClass('nav-hover');
   });


   // Menu Toggle
   jQuery('.toggle-btn').click(function(){
       $(".left-side").getNiceScroll().hide();
       
       if ($('body').hasClass('left-side-collapsed')) {
           $(".left-side").getNiceScroll().hide();
       }
      var body = jQuery('body');
      var bodyposition = body.css('position');

      if(bodyposition != 'relative') {

         if(!body.hasClass('left-side-collapsed')) {
            body.addClass('left-side-collapsed');
            jQuery('.custom-nav ul').attr('style','');

            jQuery(this).addClass('menu-collapsed');

         } else {
            body.removeClass('left-side-collapsed chat-view');
            jQuery('.custom-nav li.active ul').css({display: 'block'});

            jQuery(this).removeClass('menu-collapsed');

         }
      } else {

         if(body.hasClass('left-side-show'))
            body.removeClass('left-side-show');
         else
            body.addClass('left-side-show');

         mainContentHeightAdjust();
      }

   });
   

   searchform_reposition();

   jQuery(window).resize(function(){

      if(jQuery('body').css('position') == 'relative') {

         jQuery('body').removeClass('left-side-collapsed');

      } else {

         jQuery('body').css({left: '', marginRight: ''});
      }

      searchform_reposition();

   });

   function searchform_reposition() {
      if(jQuery('.searchform').css('position') == 'relative') {
         jQuery('.searchform').insertBefore('.left-side-inner .logged-user');
      } else {
         jQuery('.searchform').insertBefore('.menu-right');
      }
   }

    // panel collapsible
    $('.panel .tools .fa').click(function () {
        var el = $(this).parents(".panel").children(".panel-body");
        if ($(this).hasClass("fa-chevron-down")) {
            $(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            $(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200); }
    });

    $('.todo-check label').click(function () {
        $(this).parents('li').children('.todo-title').toggleClass('line-through');
    });

    $(document).on('click', '.todo-remove', function () {
        $(this).closest("li").remove();
        return false;
    });

    $("#sortable-todo").sortable();


    // panel close
    $('.panel .tools .fa-times').click(function () {
        $(this).parents(".panel").parent().remove();
    });



    // tool tips

    $('.tooltips').tooltip();

    // popovers

    $('.popovers').popover();








})(jQuery);