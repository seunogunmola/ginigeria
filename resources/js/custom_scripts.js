function check_all() {

    administrator = document.getElementById('selecctall');
    checkboxes = document.getElementsByName('privileges[]');

    if (administrator.checked == true) {

        administrator.checked = source.checked;
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes.checked = true;


        }
    }
    else {

        var count = 0;
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            if (checkboxes[i].checked === true) {
                ++count;
            }
        }

        if (count == checkboxes.length) {
            administrator = document.getElementById('selecctall');
            administrator.checked = true;
        } else {
            administrator = document.getElementById('selecctall');
            administrator.checked = false;
        }
    }
}


function getFaculties(institutionId) {
    if (institutionId !== '') {
        $.ajax({
            url: "/ajax/fetchInsitutionFaculties?institution_id=" + institutionId + '&loadDepartments=1',
            success: function (result_data) {
                $('#facultyPlaceholder').html(result_data);
            }
        });
    }
}

function getDepartments(institutionId, facultyId) {
    if (institutionId !== '' && facultyId !== '') {
        $.ajax({
            url: "/ajax/fetchInsitutionDepartments?institution_id=" + institutionId + '&faculty_id=' + facultyId,
            success: function (result_data) {
                $('#departmentPlaceholder').html(result_data);
            }
        });
    }
}

