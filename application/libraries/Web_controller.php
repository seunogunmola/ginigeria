<?php

class Web_controller extends Base_Controller {

    function __construct() {
        parent::__construct();
        $this->data['error_message'] = '';
        $this->data['message'] = ''; 
        $this->data['userIsLoggedin'] = $userIsLoggedin = $this->session->userdata('userLoggedin');
        if(isset($userIsLoggedin) && $userIsLoggedin==true){
            $this->load->model('Faculties_model');
            $this->load->model('Departments_model');
            $this->load->model('Students_model');
            $this->load->model('Classroom_model');
            $this->load->model('Lecturers_model');
            $this->load->model('Assignments_model');
            $this->load->model('Notes_model');
            $this->load->model('Broadcasts_model');
            $this->load->model('Messages_model');
            $this->load->model('Audit_model');
            $this->load->model('Students_class_model');
            $this->load->model('Thesis_students_model');
            $this->load->model('Thesis_chapters_model');
            
            $this->userdata=$this->data['userdata'] = $this->Students_model->get($this->session->userdata['activeUserdata']->id);
            $this->currentUsername = $this->userdata->fullname;
            $this->institution_id = $this->userdata->institution_id;
            $this->data['currentSession'] = $this->Audit_model->getCurrentSession($this->institution_id);
            $this->currentSession = $this->data['currentSession'];

            $this->userid = $this->userdata->uniqueid;
            $this->user_id = $this->userdata->id;
            $this->data['privileges'] = explode(' ',$this->session->userdata['activeUserdata']->privileges);
            $this->privileges = $this->userdata->privileges;
            if($this->privileges=='student'){
                //CHECK IF STUDENT HAS THESIS
                $data = $this->Thesis_students_model->get_by(['student_uniqueid'=>  $this->userid]);
                if(count($data)){
                    $this->data['studentHasThesis']=true;
                    $this->data['studentThesisData'] = $data;
                }
                else{
                    $this->data['studentHasThesis']=false;
                }
            }
        }else{
            redirect('web','refresh');
        }
    }


    public function _get_unique_code($table, $field, $type, $length)
    {
        $new_code = random_string($type, $length);

        $match_code = $this->db->get_where($table, array($field=>$new_code))->row();
        if(count($match_code)==1){
            return $this->_get_unique_code($table, $field, $type, $length);
        }else{
            return $new_code;
        }
    }

    public function _log_activity($title, $action_details)
    {
        $this->load->model('Audit_model');
        $this->Audit_model->logAction($title ,$action_details,$this->schoolData->uniqueid);
    }



    }



