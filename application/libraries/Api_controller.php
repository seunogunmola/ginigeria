<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Api_controller  extends REST_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('classroom_model');
        $this->load->model('assignments_model');
        $this->load->model('broadcasts_model');
        $this->load->model('users_model'); 
        $this->load->model('notes_model');
        $this->load->model('lecturers_model');
        $this->load->model('students_class_model');
        $this->load->model('Feeds_model');
        $this->load->model('Feed_comments_model');
        $this->load->model('Feed_likes_model');
        $this->load->model('Chats_model');
        $this->load->model('News_model');
        $this->load->model('auth_model');
        $this->load->model('otp_model');
        $this->load->model('Classroom_model');
        $this->load->model('Thesis_students_model');
        $this->load->model('Thesis_chapters_model');
        $this->load->model('Thesis_comments_model');
        $this->load->model('Students_model');
    }
    
    
    protected function validateToken(){
        $headers = $this->input->request_headers();
        $tokenData = array();
        if (array_key_exists('Auth', $headers) && !empty($headers['Auth'])) {
            $token = str_replace('Bearer ','',$headers['Auth']);
            $decodedToken = AUTHORIZATION::validateToken($token);
            if ($decodedToken != false || $decodedToken!=NULL) {
                $tokenData['status']=true;
                $tokenData['data']=$decodedToken;
            }
            else{
                $tokenData['status']=false;
            }
        }
        else{
            $tokenData['status']=false;
        }
        return $tokenData;
    }
    
}
