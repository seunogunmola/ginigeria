<?php

class Admin_controller extends Base_Controller {

    function __construct() {
        parent::__construct();
        $this->data['error_message'] = '';
        $this->data['message'] = '';
        //$this->analyzeUrl($this->data['current_url']);        
        #CONFIRM IF USER IS LOGGEDIN ELSE REDIRECT TO LOGIN
        if(!in_array( uri_string(), array('admin/auth/login')))
        {
            $userIsLoggedin = $this->session->userdata('userLoggedin');
            if(isset($userIsLoggedin) && $userIsLoggedin==true){
                $this->data['userdata'] = $userdata = $this->session->userdata['activeUserdata'];
                $this->currentUsername = $userdata->fullname;
                $this->data['privileges'] = explode(' ',$this->session->userdata['activeUserdata']->privileges);
            }
            else{
                redirect('admin/auth/login');
            }
        }
        $this->load->model('Admin_model');
        $this->load->model('customers_model');
        $this->load->model('credit_model');
        $this->load->model('debit_model');
        $this->load->model('balance_model');
        $this->load->model('loans_model');
        $this->load->model('shares_model');
        $this->load->model('shares_balance_model');
         #GET DATATABLE SCRIPT
        $this->getDataTableScripts();
    }






    }



