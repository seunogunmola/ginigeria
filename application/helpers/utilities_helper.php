<?php
function print_data($data){
    echo "<pre>";
    print_r($data);
    exit;
}
function add_meta_title($string) {
    $CI =&get_instance();
    $CI->data['meta_title'] = e($string). ' - '. $CI->data['meta_title'];

}
function getResource($resourceName){
    return base_url('resources/'.$resourceName);
}

function getAlertMessage($message,$type = 'danger') {
    if ($type != 'danger') {
       return '<div class = "alert alert-'.$type.' alert-dismissible" style = "margin-left:10px; margin-right:10px;" > <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'. $message ."</div>";
    }
    else {
        return '<div align="center" class = "alert alert-'.$type.' alert-dismissible"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> '. $message ."</div>";
    }
}


function get_faculty($id)
{
     $THIS =& get_instance();   
     $faculty =  $THIS->db->get_where('t_faculties',['id'=>$id])->row();
     if(count($faculty)){
        return $faculty;
     }else{
        $faculty = new stdClass();
        $faculty->faculty_name = '';
        $faculty->id = '';
        
        return $faculty;

     }

}
function get_error($msg) {
    return '<div class="alert alert-danger">' . $msg . '</div>';
}
function getBtn($url,$type){
    if ($type == 'edit') {
        return '<a href = "'.$url.'"  title = "Click this Button to Edit" style = "margin:5px;" data-toggle = "tooltip"> <i class = "fa fa-edit"></i></a>';
    }
    elseif ($type == 'delete') {
                return '<a href = "'.$url.'" title = "Click this Button to Delete" data-toggle = "tooltip" onclick = "return confirm(\'Are you sure you want to delete? \n It cannot be undone \')"> <i class = "fa fa-times-circle"></i></a>';
    }
    elseif ($type == 'view') {
                return '<a href = "'.$url.'"  target = "_blank" title = "Click this Button to view more info" data-toggle = "tooltip"> <i class = "fa fa-search"  ></i></a>';
    }

}

function getExcerpt($article,$numwords = 50) {
    $string = '';
    $url = 'articles/'.intval($article->article_id).'/'.$article->slug;
    $string .= '<h2>'.anchor($url,e($article->title)).'</h2>';
    $string .= '<p> <span class = "glyphicon glyphicon-time"></span> Posted on : '.e($article->publication_date).'</p>';
    $string .= '<p>'.e(limit_to_words(strip_tags($article->body),$numwords)).'</p>';
    $string .= anchor($url,'Read More');
    return $string;
}


function limit_to_words($string,$numwords) {
    $excerpt = explode(' ',$string,$numwords);

    if (count($excerpt) >= $numwords) {
        array_pop($excerpt);
    }
    $excerpt = implode(' ', $excerpt);
    return $excerpt;
}

function e($string){
    return htmlentities($string);
}

function article_link($article) {

    $url = 'articles/'.intval($article->article_id).'/'.$article->slug;
    $url = '<h2>'.anchor($url,e($article->title)).'</h2>';
    return $url;
}

function article_links($articles) {
    $string = '<ul>';
    foreach ($articles as $article):
        $url = article_link($article);
        $string .= '<li>'.$url.'</li>';
    endforeach;
    $string .= '</ul>';

    return $string;

}


function hashstring($stringtohash) {
    return hash('sha1',$stringtohash);
}


function getStates()
{
			return array('ABIA',
			'ADAMAWA',
			'AKWA IBOM',
			'ANAMBRA',
			'BAUCHI',
			'BAYELSA',
			'BENUE',
			'BORNO',
			'CROSS RIVER',
			'DELTA',
			'EBONYI',
			'EDO',
			'EKITI',
			'ENUGU',
			'GOMBE',
			'IMO',
			'JIGAWA',
			'KADUNA',
			'KANO',
			'KATSINA',
			'KEBBI',
			'KOGI',
			'KWARA',
			'LAGOS',
			'NASSARAWA',
			'NIGER',
			'OGUN',
			'ONDO',
			'OSUN',
			'OYO',
			'PLATEAU',
			'RIVERS',
			'SOKOTO',
			'TARABA',
			'YOBE',
			'ZAMFARA');

        }
function getCountries()
{
    return array('NIGERIA');

        }

function sendsms($senderid, $message, $receiver) {
    $message = urlencode($message); 
    $sender= urlencode($senderid); 
    $mobile = $receiver; 
    $url = 'https://www.multitexter.com/tools/geturl/Sms.php?username=ogunmola.net@gmail.com&password=ogunmola&sender='.$sender.'&message='.$message .'&flash=0&recipients='.$mobile.'&forcednd=1';    
    
    $ch = curl_init(); 
    curl_setopt($ch,CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
    curl_setopt($ch, CURLOPT_HEADER, 0); 
    $resp = curl_exec($ch); 
    curl_close($ch);
}

function generateQrCode($input){
    
   
    $include_path =  APPPATH."third_party/phpqrcode/qrlib.php";
    include($include_path);
    $PNG_TEMP_DIR = 'resources'.DIRECTORY_SEPARATOR.'qrcodes'.DIRECTORY_SEPARATOR;
    if(!is_dir($PNG_TEMP_DIR)){
        mkdir($PNG_TEMP_DIR);
        }
        $PNG_WEB_DIR = 'resources/qrcodes/';
        $filename = $PNG_TEMP_DIR.$input .'.png';
        $errorCorrectionLevel = 'L';
        $matrixPointSize = 4;
        QRcode::png($input, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
        return base_url($filename);
}

 function dayth($d)
   {
                $day = array('','1st','2nd','3rd','4th','5th','6th','7th','8th','9th','10th','11th','12th','13th','14th','15th','16th','17th','18th','19th','20th','21st','22nd','23rd','24th','25th','26th','27th','28th','29th','30th','31st');
		$d = intval($d);
		$isday = $day[$d];
		return $isday;  
    }
    
    
function month($m)
   {
        $month = array("","January","February","March","April","May","June","July","August","September","October","November","December");
	$m = intval($m);
	$ismonth = $month[$m];
        return $ismonth;   
    }
    
 function dayofmonthtime($date)
 {
	$dat = explode(" ",$date);
	$day = explode("-",$dat[0]);
	return dayth($day[2])." of ".month($day[1]).", ".$day[0]." at ".explode(':',$dat[1])[0].':'.explode(':',$dat[1])[1];
 }
 

function label($value, $type)
{
     
    return "<span class='label label-$type'>$value</span>";
}

 
 function show_rating($rating)
 {
    $rate = round($rating, 0, PHP_ROUND_HALF_DOWN);
    $decimal = in_array($rating, array(0,1,2,3,4,5))? '0':'1';
    
    $star='';
    for($i=1; $i<=round($rating, 0, PHP_ROUND_HALF_DOWN); $i++){
        $star .= '<i class="fa fa-star"></i> '; 
    }

    $star .= ($decimal==1)? '<i class="fa fa-star-half-o"></i> ':'';  
    
    
    $total = 5 - (  $rate + $decimal);

    for($x=1; $x<=$total; $x++){
        $star .= '<i class="fa fa-star-o"></i> '; 
    }


    echo $star;
 }
 
 function getStatusDropDown($fieldName,$currentValue){
    $options = array(''=>'Select Option','1'=>'Enabled','0'=>'Disabled');
    return form_dropdown($fieldName, $options, set_value($fieldName,$currentValue), 'class="form-control" required');
 }
 
 function getDropDown($fieldName,$options,$currentValue){
    return form_dropdown($fieldName, $options, set_value($fieldName,$currentValue), 'class="form-control" required');
 }
 
 function getStatusLabel($status){
     $label = "";
     if($status==0){
         $label = "<label class='label label-danger'> Disabled </label>";
     }
     else{
         $label = "<label class='label label-success'> Enabled </label>";
     }
     return $label;
 }
 
 function getApprovalLabel($status){
     $label = "";
     if($status==0){
         $label = "<label class='label label-danger'> Pending </label>";
     }
     else{
         $label = "<label class='label label-success'> Approved </label>";
     }
     return $label;
 }
 
 function GenderDropDown($name,$setValue){
     $options = array(''=>'Select Gender','Male'=>'Male','Female'=>'Female');
     return form_dropdown($name,$options,set_value($name,$setValue),'class = "form-control" required');
 }