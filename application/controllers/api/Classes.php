<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Classes extends Api_controller {

    public function __construct() {
        parent::__construct();
    }

    public function create_post(){
        $tokenData = $this->validateToken();
        if ($tokenData['status']==false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
        } 
        $data = json_decode(file_get_contents('php://input'));
        //CHECK IF CLASS NAME EXISTS
        $institutionid = $tokenData['data']->institution_id;
        $lecturerid = $tokenData['data']->id;
        $departmentid = $tokenData['data']->department_id;
        $classroomName = $data->classroom_name;
        $class_id = isset($data->classroom_id) ? $data->classroom_id : NULL;
        
        if($class_id==NULL){
            $classNameExists = $this->Classroom_model->classNameExists($classroomName,$institutionid,$departmentid);
        }
        else{
            $classNameExists=false;
        }
        
        if($classNameExists==true){
            $this->response('classname already exists',REST_Controller::HTTP_CONFLICT);
        }
        else{
            //CREATE CLASS
            $classroomData = [
                'classroom_name'=>$data->classroom_name,
                'classroom_bio'=>$data->classroom_bio,
                'class_limit'=>$data->classroom_limit,
                'classroom_image'=>$data->classroom_image,
                'lecturer_id'=>$lecturerid,
                'institution_id'=>$institutionid,
                'department_id'=>$departmentid,
                'status'=>1,
            ];
            $saved=$this->Classroom_model->save($classroomData,$class_id);
            if($saved){
                $message = $class_id==NULL?"class created":"class updated";
                $this->response($message,REST_Controller::HTTP_CREATED);
            }
            else{
                $this->response('an error occurred',REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
            
        }
    }
    
    public function list_get() {
        $tokenData = $this->validateToken();
        if ($tokenData['status']===false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            exit;
        }
            $classes = $this->students_class_model->getAdvancedClassPool($tokenData['data']);
			
            if(count($classes)){
                $payload = array();
                foreach($classes as $class){
					$user = $this->users_model->getPayload($class['lecturer_id']);
                    $class['user'] = $user;
                    $payload[]=$class;
                }
				
                $response['status']=true;  
                $response['classes']=$payload; 
								
            }
            else{
                $response['status']=false;
                $response['message']='no classes found';                
            }
            $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function join_post(){
        $tokenData = $this->validateToken();
        if ($tokenData['status']===false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            exit;
        } else {
            $user_uniqueid = $tokenData['data']->uniqueid;
        }
        $data = json_decode(file_get_contents('php://input'));
        $class_id = $data->class_id;
        //CHECK IF CLASS EXISTS
        $classData = $this->Classroom_model->get($class_id);
        if(count($classData)==0){
            $this->response('invalid class id',  REST_Controller::HTTP_NOT_FOUND);    
        }
        //CHECK IF STUDENT HAS ALREADY ENROLLED IN CLASS
        $studentIsEnrolled = $this->students_class_model->get_by(['student_uniqueid'=>$user_uniqueid,'class_id'=>$class_id]);
        if(count($studentIsEnrolled)){
            $this->response('You are already enrolled in this class',REST_Controller::HTTP_OK);
        }
        else{
            $enrolled = $this->students_class_model->enrollStudentInClass($class_id,$user_uniqueid,$tokenData['data']->institution_id);
            
            if($enrolled==true){
                $this->response('student enrolled successfully',  REST_Controller::HTTP_OK);    
            }
            else{
                $this->response('student could not be enrolled',  REST_Controller::HTTP_OK);    
            }
        }
    }
    
    public function unjoin_post(){
        $tokenData = $this->validateToken();
        if ($tokenData['status']===false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            exit;
        } else {
            $user_uniqueid = $tokenData['data']->uniqueid;
        }
        $data = json_decode(file_get_contents('php://input'));
        $class_id = $data->classroom_id;
        //CHECK IF CLASS EXISTS
        $classData = $this->Classroom_model->get($class_id);
        if(count($classData)==0){
            $this->response('invalid class id',  REST_Controller::HTTP_NOT_FOUND);    
        }
        //DELETE STUDENT FROM CLASS
        $deleted = $this->students_class_model->delete_where(['student_uniqueid'=>$user_uniqueid,'class_id'=>$class_id]);
        
        if(count($deleted)){
            $this->response('student un-enrolled successfully',  REST_Controller::HTTP_OK);    
        }
        else{
            $this->response('an error occured',  REST_Controller::HTTP_NOT_FOUND);                
        }
    }
    
    public function delete_post(){
        $tokenData = $this->validateToken();
        if ($tokenData['status']==false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
        } 
        $lecturerId = $tokenData['data']->id;
        $data = json_decode(file_get_contents('php://input'));
        //GET CLASS DATA
        $classData = $this->classroom_model->get($data->classroom_id);
        
        if(count($classData)){
            //IS LECTURER THE OWNER OF THE CLASS?
            if($lecturerId!=$classData->lecturer_id){
                $this->response('class doesnt belong to lecturer',REST_Controller::HTTP_UNAUTHORIZED);    
            }
            else{
                $classHasData=false;
                if($classHasData==false){
                    $deleted = $this->classroom_model->delete($classData->id);
                    if($deleted){
                        $this->response('class deleted',  REST_Controller::HTTP_OK);            
                    }
                    else{
                        $this->response('invalid class id', REST_Controller::HTTP_NOT_MODIFIED);            
                    }
                }
            }
        }
        else{
            $this->response('invalid class id',  REST_Controller::HTTP_NOT_FOUND);
        }
    }


}
