<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Feeds extends Api_controller {

    public function __construct() {
        parent::__construct();
    }

    public function create_post() {
        $tokenData = $this->validateToken();
        if ($tokenData['status'] === false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            exit;
        }

        $institution_id = $tokenData['data']->institution_id;
        $user_id = $tokenData['data']->id;

        $feedData = json_decode(file_get_contents('php://input'));
        $content = $feedData->content;
        @$class_id = $feedData->class_id;
        if (isset($content) && $content != '') {
            $data = array();
            $data['content'] = $content;
            $data['image_url'] = $feedData->image_url;
            $data['file_url'] = $feedData->file_url;
            $data['web_link'] = $feedData->web_link;
            $data['user_id'] = $tokenData['data']->id;
            $data['institution_id'] = $tokenData['data']->institution_id;
            $data['status'] = true;
            if (isset($class_id) && !empty($class_id)) {
                $data['class_id'] = $class_id;
            }
            $saved = $this->Feeds_model->save($data);

            if ($saved) {
                $response['status'] = true;
                $this->set_response($response, 200);
            } else {
                $response['status'] = false;
                $this->set_response($response, 404);
            }
        } else {
            $response['status'] = false;
            $response['message'] = 'nothing sent';
            $this->set_response($response, 404);
        }
    }

    public function list_get() {
        $tokenData = $this->validateToken();
        if ($tokenData['status'] === false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            exit;
        }
        $where = array(
            't_feed.institution_id' => $tokenData['data']->institution_id,
                //  'department_id'=>$tokenData['data']->department_id,
        );
        

        $this->db->select('t_feed.*');
        $this->db->select('count(t_feed_comments.id) as comments_count');
        $this->db->select('count(t_feed_likes.id) as likes_count');
        $this->db->join('t_feed_comments', 't_feed.id = t_feed_comments.feed_id', 'left');
        $this->db->join('t_feed_likes', 't_feed.id = t_feed_likes.feed_id', 'left');
        $this->db->group_by('t_feed.id');
        $this->db->order_by('datecreated desc');        
        $feeds = $this->Feeds_model->get_by($where);
        
        if (count($feeds)) {
            $payload = array();
            foreach ($feeds as $feed) {
                $feed->user = $this->users_model->getPayload($feed->user_id);
                $payload[] = $feed;
            }
            $response['status'] = true;
            $response['feeds'] = $payload;
            $this->set_response($response, 200);
        } else {
            $response['status'] = false;
            $response['message'] = 'no feeds found';
            $this->set_response($response, 404);
        }
    }

    public function class_post() {
        $tokenData = $this->validateToken();
        if ($tokenData['status'] === false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            exit;
        }

        $data = json_decode(file_get_contents('php://input'));
        $class_id = $data->class_id;
        if (!isset($class_id)) {
            $this->set_response('no class id sent', REST_Controller::HTTP_BAD_REQUEST);
        }

        $where = array(
            't_feed.institution_id' => $tokenData['data']->institution_id,
            't_feed.class_id' => $class_id,
                //  'department_id'=>$tokenData['data']->department_id,
        );
        $this->db->select('t_feed.*');
        $this->db->select('count(t_feed_comments.id) as comments_count');
        $this->db->select('count(t_feed_likes.id) as likes_count');
        $this->db->join('t_feed_comments', 't_feed.id = t_feed_comments.feed_id', 'left');
        $this->db->join('t_feed_likes', 't_feed.id = t_feed_likes.feed_id', 'left');
        $this->db->group_by('t_feed.id');
        $this->db->order_by('datecreated desc');
        $feeds = $this->Feeds_model->get_by($where);
        if (count($feeds)) {
            $response['status'] = true;
            $response['feeds'] = $feeds;
            $this->set_response($response, 200);
        } else {
            $response['status'] = false;
            $response['message'] = 'no feeds found';
            $this->set_response($response, 404);
        }
    }

    public function like_post() {
        $tokenData = $this->validateToken();
        if ($tokenData['status'] === false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            exit;
        }

        $institution_id = $tokenData['data']->institution_id;
        $user_id = $tokenData['data']->id;
        $feedData = json_decode(file_get_contents('php://input'));
        $feedId = $feedData->feed_id;
        if (isset($feedId) && $feedId != '') {
            //HAS USER ALREADY LIKED FEED?
            $likeExists = $this->Feed_likes_model->userAlreadyLikedFeed($feedId, $user_id);
            if ($likeExists == false) {
                $data = array();
                $data['feed_id'] = $feedId;
                $data['user_id'] = $user_id;
                $data['institution_id'] = $tokenData['data']->institution_id;
                $data['status'] = true;
                $saved = $this->Feed_likes_model->save($data);
                if ($saved) {
                    $response['status'] = true;
                    $this->set_response($response, 200);
                } else {
                    $response['status'] = false;
                    $this->set_response($response, 404);
                }
            } else {
                $response['status'] = false;
                $this->set_response($response, 304);
            }
        } else {
            $response['status'] = false;
            $response['message'] = 'nothing sent';
            $this->set_response($response, 404);
        }
    }
    
    public function comment_post() {
        $tokenData = $this->validateToken();
        if ($tokenData['status'] === false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            exit;
        }

        $institution_id = $tokenData['data']->institution_id;
        $user_id = $tokenData['data']->id;
        $feedData = json_decode(file_get_contents('php://input'));
        $feedId = $feedData->feed_id;
        $comment = $feedData->comment;
        if (isset($feedId) && isset($comment)) {
            //HAS USER ALREADY LIKED FEED?
            $commentExists = $this->Feed_comments_model->similarCommentExists($feedId,$user_id,$comment);
            if ($commentExists == false) {
                $data = array();
                $data['feed_id'] = $feedId;
                $data['user_id'] = $user_id;
                $data['comment'] = $comment;
                $data['institution_id'] = $tokenData['data']->institution_id;
                $data['status'] = true;
                $saved = $this->Feed_comments_model->save($data);
                if ($saved) {
                    $response['status'] = true;
                    $this->set_response($response, 200);
                } else {
                    $response['status'] = false;
                    $this->set_response($response, 404);
                }
            } else {
                $response['status'] = false;
                $this->set_response($response, 304);
            }
        } else {
            $response['status'] = false;
            $response['message'] = 'nothing sent';
            $this->set_response($response, 404);
        }
    }
    
    public function feedsByUser_post() {
        $tokenData = $this->validateToken();
        if ($tokenData['status'] === false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            exit;
        }
        $institution_id = $tokenData['data']->institution_id;
        $feedData = json_decode(file_get_contents('php://input'));
        $user_id = $feedData->user_id;
        if (isset($user_id)) {
                $where = array(
                    't_feed.institution_id' => $tokenData['data']->institution_id,
                    't_feed.user_id' => $user_id,
                );
                $this->db->select('t_feed.*');
                $this->db->select('count(t_feed_comments.id) as comments_count');
                $this->db->select('count(t_feed_likes.id) as likes_count');
                $this->db->join('t_feed_comments', 't_feed.id = t_feed_comments.feed_id', 'left');
                $this->db->join('t_feed_likes', 't_feed.id = t_feed_likes.feed_id', 'left');
                $this->db->group_by('t_feed.id');
                $this->db->order_by('datecreated desc');
                $feeds = $this->Feeds_model->get_by($where);
                
                if(count($feeds)){
                foreach ($feeds as $feed):
                    $feed->user = $this->users_model->getPayload($feed->user_id);
                    $payload[]=$feed;
                endforeach;
                    $response['status'] = true;
                    $response['feeds'] = $payload;
                    $this->set_response($response, 200);                
                }
                else{
                    $response['status'] = false;
                    $response['message'] = 'no feeds found';
                    $this->set_response($response, 404);                
                }
            
        } else {
            $response['status'] = false;
            $response['message'] = 'nothing sent';
            $this->set_response($response, 404);
        }
    }
    
    public function comments_post() {
        $tokenData = $this->validateToken();
        if ($tokenData['status'] === false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            exit;
        }
        $institution_id = $tokenData['data']->institution_id;
        $feedData = json_decode(file_get_contents('php://input'));
        $feedId = $feedData->feed_id;
        if (isset($feedId)) {
            $this->db->order_by('datecreated desc');
            $comments = $this->Feed_comments_model->get_by(['feed_id'=>$feedId]);
            
            if(count($comments)){
                $payload = array();
                foreach ($comments as $comment):
                    $comment->user = $this->users_model->getPayload($comment->user_id);
                    $payload[]=$comment;
                endforeach;
                $response['status'] = false;
                $response['comments'] = $payload;
                $this->set_response($response, 200);                
            }
            else{
                $response['status'] = false;
                $response['message'] = 'no comments found';
                $this->set_response($response, 404);                
            }
            
        } else {
            $response['status'] = false;
            $response['message'] = 'nothing sent';
            $this->set_response($response, 404);
        }
    }
    
    public function delete_post() {
        $tokenData = $this->validateToken();
        if ($tokenData['status'] === false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            exit;
        }
        $feedData = json_decode(file_get_contents('php://input'));
        $feedId = $feedData->feed_id;
        if (isset($feedId) && $feedId != '') {
            //CHECK FOR FEED DATA
            $feedData = $this->Feeds_model->get($feedId);            
            
            if(count($feedData)){ 
                //DELETE FEED          
                $deleted = $this->Feeds_model->delete($feedId);            
                if($deleted){
                    //DELETE FEED COMMENTS
                    $where = array('feed_id'=>$feedId);
                    $this->Feed_comments_model->delete_where($where);
                    $this->Feed_likes_model->delete_where($where);
                    
                    $response['status'] = true;
                    $response['message'] = 'feed deleted';
                    $this->set_response($response, 200);                
                }
                else{
                    $response['status'] = false;
                    $response['message'] = 'an error occurred';
                    $this->set_response($response, 404);                
                }
            }
            else{
                    $response['status'] = false;
                    $response['message'] = 'invalid feed id';
                    $this->set_response($response, 404);                 
            }
        } else {
            $response['status'] = false;
            $response['message'] = 'nothing sent';
            $this->set_response($response, 404);
        }
    }    
    
    
    
    
    

}
