<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Max-Age: 1000');
defined('BASEPATH') OR exit('No direct script access allowed');

class Notes extends Api_controller {

    public function __construct() {
        parent::__construct();
    }

    public function create_post(){
        $tokenData = $this->validateToken();
        if ($tokenData['status']==false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
        } 
        $requestData = json_decode(file_get_contents('php://input'));

        //CHECK IF ASSIGNMENT EXISTS
        $noteExists = $this->notes_model->noteExists($requestData->title,$tokenData['data']->institution_id,$tokenData['data']->department_id);
        if($noteExists==true){
            $this->response('note already exists',REST_Controller::HTTP_CONFLICT);
        }
        
        $data['title'] = $requestData->title;
        $data['content'] = $requestData->content;
        $data['file_location'] = $requestData->file_location;
        $data['class_id'] = $requestData->class_id;
        $data['institution_id'] = $tokenData['data']->institution_id;
        $data['lecturer_id'] = $tokenData['data']->id;
        $data['department_id']  = $tokenData['data']->department_id;
        $data['status']  = 1;
        $saved = $this->notes_model->save($data,NULL);
        
        if($saved){
            $this->response('note saved',REST_Controller::HTTP_CREATED);
        }
        else{
            $this->response('an error occurred',REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    
    public function list_get() {
        $tokenData = $this->validateToken();
        
        if ($tokenData['status']===false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            exit;
        }
            $institution_id = $tokenData['data']->institution_id;
            $params = ['institution_id'=>$institution_id];
            
            // $privilege = $tokenData['data']->privileges;
            // if($privilege=='lecturer'){
            //     $params['lecturer_id']=$tokenData['data']->id;
            // }
            
            $notes = $this->notes_model->get_by($params);
            

            if(count($notes)){
                $payload = array();
                foreach($notes as $note){
                    $note->user = $this->users_model->getPayload($note->lecturer_id);
                    $payload[]=$note;
                }
                $response['status']=true;  
                $response['notes']=$payload;                
                $this->set_response($response, REST_Controller::HTTP_OK);
            }
            else{
                $response['status']=false;
                $response['message']='no notes found';                
                $this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
            }
            
    }

    public function single_post(){
        $tokenData = $this->validateToken();
        if ($tokenData['status']==false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
        } 
        $requestData = json_decode(file_get_contents('php://input'));
        if(!isset($requestData)){
            $this->set_response('invalid request', REST_Controller::HTTP_BAD_REQUEST);
        }
        else{
        $note_id = $requestData->note_id;

        if(isset($note_id) && !empty($note_id)){
            $noteData = $this->notes_model->get($note_id);
            if(count($noteData)){
                $noteData->user = $this->users_model->getPayload($noteData->lecturer_id);
                $response['status']=true;
                $response['assignment']=$noteData;                
                $this->set_response($response,200);            
            }
            else{
                $response['status']=false;
                $response['message']='not found';                
                $this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
            }
        }
        else{
            $this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
        }
        }
    }
    
    public function delete_post(){
        $tokenData = $this->validateToken();
        if ($tokenData['status']==false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
        } 
        $requestData = json_decode(file_get_contents('php://input'));
        if(empty($requestData)){
            $this->set_response('invalid request', REST_Controller::HTTP_BAD_REQUEST);
        }
        else{
        $note_id = $requestData->note_id;
        if(isset($note_id) && !empty($note_id)): 
            //CHECK IF ASSIGNMENT EXISTS
            $notesData = $this->notes_model->get($note_id);
            if(count($notesData)){
                $deleted = $this->notes_model->delete($note_id);
                if($deleted){
                    $response['status']=true;           
                    $this->set_response($response,200);         
                }
                else{
                    $this->set_response($response,301);         
                }
            }
            else{
                $response['status']=false;
                $response['message']='not found';                
                $this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
            }
        endif;
        }
    }
}
