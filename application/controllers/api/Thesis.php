<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Thesis extends Api_controller {
    public function __construct() {
        parent::__construct();
    }

    public function createStudent_post() {
            $tokenData = $this->validateToken();
            if ($tokenData['status']===false) {
                $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            }
            $thesis_data = json_decode(file_get_contents('php://input'));
            
            $institution_id = $tokenData['data']->institution_id;
            $student_id = $thesis_data->student_id;
            
            if(isset($institution_id) && isset($student_id)){
                //CHECK IF STUDENT ALREADY HAS A THESIS
                $thesisStatus = $this->Thesis_students_model->getThesisStatus($student_id,$institution_id);
                if(count($thesisStatus)){
                    $response['status']=true;                
                    $response['message']="student already has thesis";                
                    $this->set_response($response, 405);                       
                }
                else{
                    $data = array();
                    $data['student_id']=$student_id;  
                    $data['thesis_title']=$thesis_data->thesis_title;  
                    $data['lecturer_id']=$tokenData['data']->id;  
                    $data['department_id']=$tokenData['data']->department_id; 
                    $data['institution_id']=$institution_id; 
                    $data['status']=true;  
                    $saved = $this->Thesis_students_model->save($data);
                    if($saved){
                        $response['status']=true;                
                        $response['message']="success";
                        $this->set_response($response, 200);   
                    }
                    else{
                        $response['status']=false;                
                        $this->set_response($response, 404);      
                    }   
                }
            }
            else{
                $response['status']=false;
                $response['message']='nothing sent';                
                $this->set_response($response, 404);
            }
    }
    
    public function viewStudents_get() {
            $tokenData = $this->validateToken();
            if ($tokenData['status']===false) {
                $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            }
            $institution_id = $tokenData['data']->institution_id;
            $lecturer_id = $tokenData['data']->id;
            
            if(isset($lecturer_id) && isset($institution_id)){
                $lecturerStudents = $this->Thesis_students_model->getLecturersStudents($lecturer_id,$institution_id);
                if(!count($lecturerStudents)){
                    $response['status']=false;                
                    $response['message']="no thesis students found";                
                    $this->set_response($response,405);                       
                }
                else{
                    $payload = array();
                    foreach($lecturerStudents as $student):
                        $student->user = $this->users_model->getPayload($student->student_id);
                        $student->deleteAble = $this->Thesis_chapters_model->countChapters($student->id) > 0 ? 0 : 1;
                        $payload[]=$student;
                    endforeach;
                   
                    $response['status']=true;                
                    $response['thesisStudents']=$payload;                
                    $this->set_response($response, 200);   
                }
            }
            else{
                $response['status']=false;
                $response['message']='nothing sent';                
                $this->set_response($response, 404);
            }
    }
     
    public function createChapter_post() {
            $tokenData = $this->validateToken();
            if ($tokenData['status']===false) {
                $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            }
            $postData = json_decode(file_get_contents('php://input'));
            $institution_id = $tokenData['data']->institution_id;
            $thesis_id = $postData->thesis_id;
            $chapter_title = $postData->chapter_title;
            
            if(isset($thesis_id) && isset($chapter_title)){
                $chapterStatus = $this->Thesis_chapters_model->getChapterStatus($thesis_id,$chapter_title);
                
                if(count($chapterStatus)){
                    $response['status']=true;                
                    $response['message']="a chapter with the title already exists";                
                    $this->set_response($response, 405);                       
                }
                else{
                    $data['thesis_id']=$thesis_id; 
                    $data['chapter_title']=$chapter_title; 
                    $data['chapter_description']=$postData->chapter_description; 
                    $data['chapter_file']=$postData->chapter_file; 
                    $data['student_id']=$tokenData['data']->id; 
                    $data['department_id']=$tokenData['data']->department_id; 
                    $data['institution_id']=$institution_id; 
                    $data['status']=true;  
                    $saved = $this->Thesis_chapters_model->save($data);
                    if($saved){
                       $response['status']=true;                
                        $this->set_response($response, 200);   
                    }
                    else{
                        $response['status']=false;                
                        $this->set_response($response, 404);      
                    }   
                }
            }
            else{
                $response['status']=false;
                $response['message']='nothing sent';                
                $this->set_response($response, 404);
            }
    }
    
    public function viewChapters_post() {
            $tokenData = $this->validateToken();
            if ($tokenData['status']===false) {
                $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            }
            
            $postData = json_decode(file_get_contents('php://input'));
            $institution_id = $tokenData['data']->institution_id;
            $thesis_id = $postData->thesis_id;
            if(isset($thesis_id)){
                $thesisChapters = $this->Thesis_chapters_model->get_by(['thesis_id'=>$thesis_id]);
                if(count($thesisChapters)){
                    $response['status']=true;                
                    $response['chapter']=$thesisChapters;                
                    $this->set_response($response,200);                       
                }
                else{
                    $response['status']=false;                
                    $response['message']="no chapters found";                
                    $this->set_response($response, 404);      
                }
            }
            else{
                $response['status']=false;
                $response['message']='nothing sent';                
                $this->set_response($response, 404);
            }
    }
    
    public function postComment_post() {
            $tokenData = $this->validateToken();
            if ($tokenData['status']===false) {
                $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            }
            
            $postData = json_decode(file_get_contents('php://input'));
            $institution_id = $tokenData['data']->institution_id;
            $thesis_id = $postData->thesis_id;
            
            if(isset($thesis_id)){
                    $data=array();
                    $data['lecturer_id ']=$tokenData['data']->id;
                    $data['thesis_id']=$thesis_id;
                    $data['comment']=$postData->comment;
                    $data['chapter_id']=$postData->chapter_id;
                    $data['comment_file']=$postData->comment_file;
                    $data['comment_link']=$postData->comment_link;
                    $data['institution_id']=$institution_id;
                    $data['status']=0;
                    $saved = $this->Thesis_comments_model->save($data);
                    if($saved){
                        $response['status'] = true;
                        $response['message'] = "comment saved";
                        $this->set_response($response,200);  
                        
                    }
                    else{
                        $response['status'] = false;
                        $response['message'] = "error";
                        $this->set_response($response,404);
                    }                
            }
            else{
                $response['status']=false;
                $response['message']='nothing sent';                
                $this->set_response($response, 404);
            }
    }
    
    public function viewComments_post() {
            $tokenData = $this->validateToken();
            if ($tokenData['status']===false) {
                $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            }
            
            $postData = json_decode(file_get_contents('php://input'));
            $institution_id = $tokenData['data']->institution_id;
            $thesis_id = $postData->thesis_id;
            $chapter_id = $postData->chapter_id;
            
            if(isset($thesis_id)){
                    $where = ['thesis_id'=>$thesis_id];
                    
                    if(isset($chapter_id) && $chapter_id!=NULL){
                        $where['chapter_id'] = $chapter_id;
                    }
                    
                    $thesisComments = $this->Thesis_comments_model->get_by($where);
                    if(count($thesisComments)){
                        $payload = array();
                        foreach($thesisComments as $comment):
                            $comment->user = $this->users_model->getPayload($comment->lecturer_id);
                            $payload[]= $comment;
                        endforeach;
                        $response['status'] = true;
                        
                        $response['comments'] = $payload;
                        $this->set_response($response,200);  
                        
                    }
                    else{
                        $response['status'] = false;
                        $response['message'] = "no comments found";
                        $this->set_response($response,404);
                    }                
            }
            else{
                $response['status']=false;
                $response['message']='nothing sent';                
                $this->set_response($response, 404);
            }
    }
    
    public function status_get() {
            $tokenData = $this->validateToken();
            if ($tokenData['status']===false) {
                $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
                exit;
            }
            
            $status = $this->Thesis_students_model->getThesisStatus($tokenData['data']->id,$tokenData['data']->institution_id);
            if($status!=NULL){
                $status->user = $this->users_model->getPayload($status->lecturer_id);
                $response['status']=true;  
                $response['thesis']=$status;                
                $this->set_response($response, 200);
            }
            else{
                $response['status']=false;
                $response['message']='No thesis yet';                
                $this->set_response($response, 404);
            }
    }
    
    public function myThesis_get() {
            $tokenData = $this->validateToken();
            if ($tokenData['status']===false) {
                $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            }
            $student_id = $tokenData['data']->id;
            //$student_id = 73;
            $thesisData = $this->Thesis_students_model->get_by(['student_id'=>$student_id],true);
            if(count($thesisData)){
                $thesisData->user = $this->users_model->getPayload($thesisData->lecturer_id);
                $response['status']=true;  
                $response['thesis']=$thesisData;                
                $this->set_response($response, 200);
            }
            else{
                $response['status']=false;
                $response['message']='No thesis yet';                
                $this->set_response($response, 404);
            }
    }
    
    public function updateThesis_post() {
            $tokenData = $this->validateToken();
            if ($tokenData['status']===false) {
                $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            }
            $data = json_decode(file_get_contents('php://input'));
            
            $student_id = $tokenData['data']->id;
            $thesis_id = $data->thesis_id;
            
            if(isset($student_id) && isset($thesis_id)){
                //GET THESIS DATA
                $thesisData = $this->Thesis_students_model->get($thesis_id);
                if(count($thesisData)){
                    if($thesisData->student_id != $student_id){
                        $response['status']=false;                
                        $response['message']="student mismatch";                
                        $this->set_response($response, 404);                          
                    }
                    else{
                        $updateData = array();
                        $updateData['thesis_title'] = $data->thesis_title;
                        $updateData['thesis_abstract'] = $data->thesis_abstract;
                        $updateData['thesis_keywords'] = $data->thesis_keywords;
                        $updateData['thesis_motivations'] = $data->thesis_motivations;
                        $updateData['thesis_document'] = $data->thesis_document;
                        
                        $updated = $this->Thesis_students_model->save($updateData,$thesis_id);
                        
                        if($updated){
                            $response['status']=false;                
                            $response['message']="thesis updated";                
                            $this->set_response($response, 202);                              
                        }
                        else{
                            $response['status']=false;                
                            $response['message']="an error occured";                
                            $this->set_response($response, 404);  
                        }
                    }
                }
                else{
                    $response['status']=false;                
                    $response['message']="thesis not found";                
                    $this->set_response($response, 404);  
                }
            }
            else{
                $response['status']=false;
                $response['message']='nothing sent';                
                $this->set_response($response, 404);
            }
    }
    
    public function delete_post() {
            $tokenData = $this->validateToken();
            if ($tokenData['status']===false) {
                $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            }
            
            $postData = json_decode(file_get_contents('php://input'));
            $thesis_id = $postData->thesis_id;
            
            if(isset($thesis_id)){
                    $deleted = $this->Thesis_students_model->delete($thesis_id);
                    if($deleted){
                        $response['status'] = true;
                        $response['message'] = "deleted";
                        $this->set_response($response,200);  
                        
                    }
                    else{
                        $response['status'] = false;
                        $response['message'] = "error";
                        $this->set_response($response,404);
                    }                
            }
            else{
                $response['status']=false;
                $response['message']='nothing sent';                
                $this->set_response($response, 404);
            }
    }

}
