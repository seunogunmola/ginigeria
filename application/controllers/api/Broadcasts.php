<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Broadcasts extends Api_controller {

    public function __construct() {
        parent::__construct();
    }

    public function create_post(){
        $tokenData = $this->validateToken();
        if ($tokenData['status']==false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
        } 
        $requestData = json_decode(file_get_contents('php://input'));        
        //CHECK IF ASSIGNMENT EXISTS
        //$noteExists = $this->notes_model->noteExists($requestData->title,$tokenData['data']->institution_id,$tokenData['data']->department_id);
        //if($noteExists==true){
          //  $this->response('note already exists',REST_Controller::HTTP_CONFLICT);
        //}
        
        $data['title'] = $requestData->title;
        $data['content'] = $requestData->content;
        // $data['class_id'] = $requestData->class_id;
        $data['institution_id'] = $tokenData['data']->institution_id;
        $data['postedby'] = $tokenData['data']->id;
        $data['department_id']  = $tokenData['data']->department_id;
        $data['status']  = 1;
        
        $saved = $this->News_model->save($data,NULL);
        
        

        if($saved){
            $this->response('broadcast saved',REST_Controller::HTTP_CREATED);
        }
        else{
            $this->response('an error occurred',REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    
    public function list_get() {
        $tokenData = $this->validateToken();
        if ($tokenData['status']===false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            exit;
        }
            $institution_id = $tokenData['data']->institution_id;
            $params = ['institution_id'=>$institution_id];
            $notes = $this->notes_model->get_by($params);
            if(count($classes)){
                $response['status']=true;  
                $response['notes']=$notes;                
                $this->set_response($response, REST_Controller::HTTP_OK);
            }
            else{
                $response['status']=false;
                $response['message']='no notes found';                
                $this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
            }
            
    }
    
    public function delete_post(){
        $tokenData = $this->validateToken();
        if ($tokenData['status']==false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
        } 
        $lecturerId = $tokenData['data']->id;
        $data = json_decode(file_get_contents('php://input'));
        //GET CLASS DATA
        $noteData = $this->News_model->get($data->note_id);
        
        if(count($noteData)){
            //IS LECTURER THE OWNER OF THE CLASS?
            if($lecturerId!=$noteData->lecturer_id){
                $this->response('note doesnt belong to lecturer',REST_Controller::HTTP_UNAUTHORIZED);    
            }
            else{
                $classHasData=false;
                if($classHasData==false){
                    $deleted = $this->News_model->delete($noteData->id);
                    if($deleted){
                        $this->response('note deleted',  REST_Controller::HTTP_OK);            
                    }
                    else{
                        $this->response('invalid note id', REST_Controller::HTTP_NOT_MODIFIED);            
                    }
                }
            }
        }
        else{
            $this->response('invalid note id',  REST_Controller::HTTP_NOT_FOUND);
        }
    }



}
