<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Assignments extends Api_controller {

    public function __construct() {
        parent::__construct();
    }

    public function create_post(){
        $tokenData = $this->validateToken();
        if ($tokenData['status']==false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
        } 
        $requestData = json_decode(file_get_contents('php://input'));

        //CHECK IF ASSIGNMENT EXISTS
        $assignmentExists = $this->assignments_model->assignmentExists($requestData->title,$tokenData['data']->institution_id,$tokenData['data']->department_id,$requestData->class_id);
        if($assignmentExists==true){
            $this->response('assignment already exists',REST_Controller::HTTP_CONFLICT);
        }
        
        $data['title'] = $requestData->title;
        $data['content'] = $requestData->content;
        $data['file_location'] = $requestData->file_location;
        $data['class_id'] = $requestData->class_id;
        $data['date_posted'] = date('Y-m-d');
        $data['submission_date'] = $requestData->submission_date;
        $data['institution_id'] = $tokenData['data']->institution_id;
        $data['lecturer_id'] = $tokenData['data']->id;
        $data['department_id']  = $tokenData['data']->department_id;
        $data['status']  = 1;
        $saved = $this->assignments_model->save($data,NULL);
        
        if($saved){
            $this->response('assignment created',REST_Controller::HTTP_CREATED);
        }
        else{
            $this->response('an error occurred',REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    
    public function list_get() {
        $tokenData = $this->validateToken();
            if ($tokenData['status']===false) {
                $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
                exit;
            }
            
            $where = array(
                't_assignments.institution_id'=>$tokenData['data']->institution_id,
            );
            
            // $privilege = $tokenData['data']->privileges;
            // if($privilege=='lecturer'){
            //     $where['lecturer_id']=$tokenData['data']->id;
            // }
            $this->db->order_by('datecreated desc');
            
            $assignments = $this->assignments_model->get_by($where);
            
            if(count($assignments)){
                $payload = array();
                foreach($assignments as $assignment){
                    $assignment->user = $this->users_model->getPayload($assignment->lecturer_id);
                    $payload[]=$assignment;
                }                
                $response['status']=true;  
                $response['assignments']=$payload;                
                $this->set_response($response, 200);
            }
            else{
                $response['status']=false;
                $response['message']='no assignments found';                
                $this->set_response($response, 404);
            }
    }

    public function single_post(){
        $tokenData = $this->validateToken();
        if ($tokenData['status']==false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
        } 
        $requestData = json_decode(file_get_contents('php://input'));
        if(empty($requestData)){
            $this->set_response('invalid request', REST_Controller::HTTP_BAD_REQUEST);
        }
        else{
        $assignment_id = $requestData->assignment_id;
        if(isset($assignment_id) && !empty($assignment_id)): 
            //CHECK IF ASSIGNMENT EXISTS
            $assignmentData = $this->assignments_model->get($assignment_id);
            if(count($assignmentData)){
                $assignmentData->user = $this->users_model->getPayload($assignmentData->lecturer_id);
                $response['status']=true;
                $response['assignment']=$assignmentData;                
                $this->set_response($response,200);            
            }
            else{
                $response['status']=false;
                $response['message']='not found';                
                $this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
            }
        endif;
        }
    }
    
    public function delete_post(){
        $tokenData = $this->validateToken();
        if ($tokenData['status']==false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
        } 
        $requestData = json_decode(file_get_contents('php://input'));
        if(empty($requestData)){
            $this->set_response('invalid request', REST_Controller::HTTP_BAD_REQUEST);
        }
        else{
        $assignment_id = $requestData->assignment_id;
        if(isset($assignment_id) && !empty($assignment_id)): 
            //CHECK IF ASSIGNMENT EXISTS
            $assignmentData = $this->assignments_model->get($assignment_id);
            if(count($assignmentData)){
                $deleted = $this->assignments_model->delete($assignment_id);
                if($deleted){
                    $response['status']=true;           
                    $this->set_response($response,200);         
                }
                else{
                    $this->set_response($response,301);         
                }
            }
            else{
                $response['status']=false;
                $response['message']='not found';                
                $this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
            }
        endif;
        }
    }

}
