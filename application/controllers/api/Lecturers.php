<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Lecturers extends Api_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users_model');
    }


    public function list_get() {
        $tokenData = $this->validateToken();
        if ($tokenData['status']===false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
        }
            $lecturers = $this->lecturers_model->getInstitutionLecturersWithDepartment($tokenData['data']->institution_id,$tokenData['data']->department_id);
            if(count($lecturers)){
                $response['status']=true;  
                $response['lecturers']=$lecturers;                
                $this->set_response($response, 200);
            }
            else{
                $response['status']=false;
                $response['message']='no lecturers found';                
                $this->set_response($response, 404);
            }
            
    }



}
