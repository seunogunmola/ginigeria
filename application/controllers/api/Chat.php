<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Chat extends Api_controller {
    public function __construct() {
        parent::__construct();
    }

    public function send_post() {
            $tokenData = $this->validateToken();
            if ($tokenData['status']===false) {
                $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
                exit;
            }
            $data = json_decode(file_get_contents('php://input'));
            $message = $data->message;
            $receiver_id = $data->receiver_id;
            
            if(isset($message) && isset($receiver_id)){
                $data = array();
                $data['message']=$message;  
                $data['receiver_id']=$receiver_id;  
                $data['message_sender'] = $tokenData['data']->id;
                $data['institution_id'] = $tokenData['data']->institution_id;
                $data['status']=true;  
                $saved = $this->Chats_model->save($data);
                if($saved){
                    $response['status']=true;                
                    $this->set_response($response, 200);   
                }
                else{
                    $response['status']=false;                
                    $this->set_response($response, 404);   
                }
            }
            else{
                $response['status']=false;
                $response['message']='nothing sent';                
                $this->set_response($response, 404);
            }
    }
    
    public function single_post() {
            $tokenData = $this->validateToken();
            if ($tokenData['status']===false) {
                $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
                exit;
            }
            $data = json_decode(file_get_contents('php://input'));
            $receiver_id = $data->receiver_id;
            if(!isset($user_id) || empty($user_id)){
                $this->set_response('invalid user id', REST_Controller::HTTP_BAD_REQUEST);
            }
            
            $sql = "SELECT t_chat_messages.*,t_users.fullname "
                    . " FROM t_chat_messages "
                    . " INNER JOIN t_users ON t_users.id = t_chat_messages.receiver_id"
                    . " WHERE message_sender = '".$tokenData['data']->id."' AND receiver_id = '".$receiver_id."'";
            $result = $this->db->query($sql)->result();
            if(count($result)){
                $response = array();
                foreach($result as $chat):
                    $response[$chat->fullname][] = array(
                        'message'=>$chat->message,
                        'datecreated'=>$chat->datecreated,
                    );
                endforeach;
                $this->set_response($response,  REST_Controller::HTTP_OK);
            }
            else{
                $response['status']=false;
                $response['message']='no messages found';                
                $this->set_response($response, 404);
            }
    }
    
    public function history_get() {
            $tokenData = $this->validateToken();
            if ($tokenData['status']===false) {
                $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
                exit;
            }
            
            $history = $this->Chats_model->getChatHistory($tokenData['data']->id);
           
            if(count($history)){
                $response['status']=true;  
                $response['chats']=$history;                
                $this->set_response($response, 200);
            }
            else{
                $response['status']=false;
                $response['message']='no chats found';                
                $this->set_response($response, 404);
            }
    }
    
    

}
