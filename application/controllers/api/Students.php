<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Students extends Api_controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function list_get() {
        $tokenData = $this->validateToken();
        if ($tokenData['status']===false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
        }
        $this->db->where(['department_id'=>$tokenData['data']->department_id,'institution_id'=>$tokenData['data']->institution_id,'privileges'=>'student']);
        $this->db->where('id!=',$tokenData['data']->id);
        $students = $this->Students_model->get();

            if(count($students)){
                $response['status']=true;  
                $response['students']=$students;                
                $this->set_response($response, 200);
            }
            else{
                $response['status']=false;
                $response['message']='no students found';                
                $this->set_response($response, 404);
            }
    }
}
