<?php
//defined('BASEPATH') OR exit('No direct script access allowed');
class Upload extends Api_controller {
    public function __construct() {
        parent::__construct();
    }

    public function file_post() {
        
            // $tokenData = $this->validateToken();
            // var_dump($tokenData);exit;
            // if ($tokenData['status']===false) {
            //     $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            // }
            
            
            $name = $_FILES['file']["name"];
            
            $uploadUrl = './resources/uploads/others';
            
            $config = array(
                'upload_path' => $uploadUrl,
                'allowed_types' => 'doc|docx|DOC|DOCX|pdf|PDF',
                'max_size' => '10048',
                'overwrite' => true,
                'file_name' => $name,
                'remove_spaces' => TRUE
            );      
            
            
                $this->load->library('upload', $config);
                
                if (!$this->upload->do_upload('file')) {
                    $error = array('error' => $this->upload->display_errors());
                    
                    $statusCode = 404;
                    $response['message'] = $error;
                }
                else{
                    $statusCode = 200;
                    $response['message'] = "File Uploaded Successfully";
                    $response['file_url'] = base_url().'resources/uploads/others/'.$name;
                }                                 
                $this->set_response($response,$statusCode);
    }
    
    
    public function delete_post() {
            $tokenData = $this->validateToken();
            if ($tokenData['status']===false) {
                $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            }
            
            $postData = json_decode(file_get_contents('php://input'));
            $thesis_id = $postData->thesis_id;
            
            if(isset($thesis_id)){
                    $deleted = $this->Thesis_students_model->delete($thesis_id);
                    if($deleted){
                        $response['status'] = true;
                        $response['message'] = "deleted";
                        $this->set_response($response,200);  
                        
                    }
                    else{
                        $response['status'] = false;
                        $response['message'] = "error";
                        $this->set_response($response,404);
                    }                
            }
            else{
                $response['status']=false;
                $response['message']='nothing sent';                
                $this->set_response($response, 404);
            }
    }

}
