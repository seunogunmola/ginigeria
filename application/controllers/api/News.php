<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class News extends Api_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users_model');
    }

    public function list_get() {
        $tokenData = $this->validateToken();
        if ($tokenData['status']===false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
            exit;
        }
            $where = array(
                'institution_id'=>$tokenData['data']->institution_id,
                'institution_id'=>$tokenData['data']->institution_id,
            );
            $this->db->order_by('datecreated desc');
            $news = $this->News_model->get_by($where);
            if(count($news)){
                $payload = array();
                foreach($news as $news_item){
                    $news_item->user = $this->users_model->getPayload($news_item->postedby);
                    $payload[]=$news_item;
                }
                $response['status']=true;  
                $response['news']=$payload;                
                $this->set_response($response, 200);
            }
            else{
                $response['status']=false;
                $response['message']='no news found';                
                $this->set_response($response, 404);
            }
            
    }

    public function delete_post(){
        $tokenData = $this->validateToken();
        if ($tokenData['status']==false) {
            $this->set_response("invalid token", REST_Controller::HTTP_BAD_REQUEST);
        } 
        $requestData = json_decode(file_get_contents('php://input'));
        if(empty($requestData)){
            $this->set_response('invalid request', REST_Controller::HTTP_BAD_REQUEST);
        }
        else{
            $news_id = $requestData->news_id;
            if(isset($news_id) && !empty($news_id)): 
                $newsData = $this->News_model->get($news_id);
                if(count($newsData)){
                    $deleted = $this->News_model->delete($news_id);
                    if($deleted){
                        $response['status']=true;           
                        $this->set_response($response,200);         
                    }
                    else{
                        $this->set_response($response,301);         
                    }
                }
                else{
                    $response['status']=false;
                    $response['message']='not found';                
                    $this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
                }
            endif;
        }
    }


}
