<?php
class Ajax extends Admin_controller {
    public function __construct() {
        parent::__construct();
    }
    
    
    public function fetchInsitutionFaculties(){
        $institution_id=$this->input->get('institution_id');
        $loadDepartments=$this->input->get('loadDepartments');
        $loadDepartmentToggle="";
        if($loadDepartments==1){
            $loadDepartmentToggle = "onchange='getDepartments(".$institution_id.",this.value)'";
        }
        if(isset($institution_id)){
            $this->db->select('faculty_id,faculty_name');
            $this->db->order_by('faculty_name asc');
            $this->db->join('t_faculties','t_faculties.id=t_institutions_faculties.faculty_id');
            $faculties = $this->FacultyAssignModel->getInstitutionsFaculties($institution_id,false);
            if($faculties!=false){
                $options=array();
                $options['']='Select Option';
                foreach($faculties as $faculty):
                    $options[$faculty->faculty_id]=$faculty->faculty_name;
                endforeach;
                echo form_dropdown('faculty_id',$options,set_value('faculty_id'),'class="form-control" required '.$loadDepartmentToggle.'');
            }
            else{
                echo getAlertMessage("No Faculties assigned to this institution");    
            }
        }
        else{
            echo getAlertMessage("Error Loading Faculties");
        }
    }
    public function fetchInsitutionDepartments(){
        $institution_id=$this->input->get('institution_id');
        $faculty_id=$this->input->get('faculty_id');

        if(isset($institution_id) && isset($faculty_id)){
            $departments = $this->departmentAssignModel->getInstitutionsDepartments($institution_id,$faculty_id);
            if($departments!=false){
                $options=array();
                $options['']='Select Option';
                foreach($departments as $department):
                    $options[$department->department_id]=$department->department_name;
                endforeach;
                echo form_dropdown('department_id',$options,set_value('department_id'),'class="form-control" required');
            }
            else{
                echo getAlertMessage("No Departments assigned to this Faculty");    
            }
        }
        else{
            echo getAlertMessage("Error Loading Departments");
        }
    }
    
}
