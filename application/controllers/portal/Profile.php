<?php
/*Make the world a better place
 * Write Code the developer coming after You and the ones coming after them
 */

class Profile extends Portal_controller{
    //You know what a Constructor Function does dont You?
    //If You dont know, kindly go and return your Salary
    public function __construct() {
        parent::__construct();
    }
    
    public function edit(){

       $this->data['user'] = $this->users_model->get_by(['uniqueid'=>$this->data['userdata']->uniqueid],TRUE); 
       
       if(!count($this->data['user'])){
           redirect(site_url('portal/auth/login'));  
       }
     
        //GET VALIDATION RULES
        $rules = $this->users_model->rules;
        unset($rules['status']);
        //SET VALIDATION RULES
        $this->form_validation->set_rules($rules);
        //FIRE RULES
        if($this->form_validation->run()==TRUE){
            //RULES PASSED
            $data = $this->input->post();
            $id = $this->data['user']->id;
            $data['profile_complete'] = 1;
            $saved = $this->users_model->save($data,$id);
                if($saved){
                    $message = "Profile updated Successfully";
                    $this->session->set_flashdata('success',$message);
                    $action_details = "A new user with username : ".$data['email_address']." was updated  by user:".$this->currentUsername." on ".date('Y-m-d h:i:s');
                    $this->Audit_model->logAction("USER UPDATE",$action_details,$institution_id=NULL);                    
                    redirect('portal/profile/edit/'.$uniqueid);
                }
                else{
                    $this->session->set_flashdata('error',"An error Occurred.Please try again");
                    redirect('portal/profile/edit/'.$uniqueid);
                }
            }
        else{
            $this->data['error_message'] = trim(validation_errors())!=FALSE?getAlertMessage(validation_errors()):'';
        }
        
        $this->data['subview'] = 'portal/profile_page';
        $this->load->view('portal/_layout_main',  $this->data);
    }
}
