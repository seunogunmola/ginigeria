<?php

class Loan extends Customer_controller{
    //You know what a Constructor Function does dont You?
    //If You dont know, kindly go and return your Salary
    public function __construct() {
        parent::__construct();
    }
    
    public function index(){
        //GET BALANCES
        #GET SAVINGS BALANCE
        $savingsBalance = $this->balance_model->get_by(['customerid'=>$this->customerid],true);
        $this->data['savings_balance'] = count($savingsBalance) ? $savingsBalance->balance : 0;
        #GET SHARES BALANCE
        $sharesBalance = $this->shares_balance_model->get_by(['customerid'=>$this->customerid],true);
        $this->data['shares_balance'] = count($sharesBalance) ? $sharesBalance->balance : 0;
        
        $totalNetworth = $this->data['savings_balance'];     
        $this->data['totalNetworth'] = $totalNetworth;
        $rules = $this->loans_model->rules;
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run() == true){
            $data = $this->input->post();            
            $this->data['loanAmount'] = $data['loanAmount'];
            $this->data['duration'] = $data['duration'];    
            $this->data['interest_rate'] = $data['loanAmount'] < (2 * $this->data['totalNetworth']) ? 1 : 2;
        }
        else{
            $this->data['error_message'] = trim(validation_errors())!=false ? validation_errors(): '';
        }
        $this->data['subview'] = 'customer/loans/calculator';
        $this->load->view('customer/_layout_main',  $this->data);
    }  
    
    public function apply($amount=NULL,$duration=NULL){
        $this->data['loanAmount'] = $this->data['loanDuration'] = 0;
        if($amount!==NULL){
            $this->data['loanAmount'] = $amount;
        }
        if($duration!==NULL){
            $this->data['loanDuration'] = $duration;
        }
        //GET BALANCES
        #GET SAVINGS BALANCE
        $savingsBalance = $this->balance_model->get_by(['customerid'=>$this->customerid],true);
        $this->data['savings_balance'] = count($savingsBalance) ? $savingsBalance->balance : 0;
        #GET SHARES BALANCE
        $sharesBalance = $this->shares_balance_model->get_by(['customerid'=>$this->customerid],true);
        $this->data['shares_balance'] = count($sharesBalance) ? $sharesBalance->balance : 0;
        
        $totalBalance = $this->data['savings_balance'];
               
        $rules = $this->loans_model->applicationRules;        
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run() == true){            
            $data = $this->input->post();         
            
            //CHECK IF CUSTOMER EXCEEDS MAXIMUM
            $loanAmount = $data['loanAmount'];
            
            if($loanAmount > (3 * $totalBalance)){
                $this->data['error_message'] = getAlertMessage("You cannot apply for a loan above ". 3 * $totalBalance);
            }
            else{
                //CHECK IF GUARANTORS HAVE ENOUGH CAPACITY
                $guarantorAmount = $loanAmount - $totalBalance;
                $guarantorWorth = $totalGuarantorWorth = 0;
                foreach($data['guarantors'] as $guarantor){
                    $guarantorWorth = $this->customers_model->getNetworth($guarantor); 
                    $totalGuarantorWorth += $guarantorWorth;
                }
                
                if($totalGuarantorWorth < $guarantorAmount){
                    $this->data['error_message'] = getAlertMessage("Your Guarantors do not have up to ". $currency.$guarantorAmount."<br/> You cannot proceed with the application");
                }
                else{
                    //ALL CORRECT..
                    #SAVE LOAN APPLICATION
                    $loanData['customerid'] = $this->customerid;
                    $loanData['loanAmount'] = $data['loanAmount'];
                    $loanData['duration'] = $data['duration'];
                    $loanData['loanid'] = $this->loans_model->generate_unique_id($len = 5, 'loanid');
                    var_dump($loanData);exit;
                }
                
            }
            echo $loanAmount;
            exit;
        }
        else{
            $this->data['error_message'] = trim(validation_errors())!=false ? validation_errors(): '';
        }
        $this->data['existing_customers'] = $this->customers_model->get_by(['status'=>'1']);
        $this->data['subview'] = 'customer/loans/apply';
        $this->load->view('customer/_layout_main',  $this->data);
    }    
     
    
    public function history(){
        $this->db->select('t_customers.fullname,t_credits.*');
        $this->db->where('t_credits.customerid',$this->data['userdata']->uniqueid);
        $this->db->join('t_customers','t_customers.uniqueid = t_credits.customerid','inner');
        $this->data['transactions'] = $this->credit_model->get();
        $this->data['subview'] = 'customer/operations/credit_history_page';
        $this->load->view('customer/_layout_main',  $this->data);
    }    
    
    public function receipt($transactionid){
        if(!isset($transactionid)){
            redirect(site_url);
        }
        else{       
            $this->db->select('t_customers.fullname,t_credits.*');
            $this->db->join('t_customers','t_customers.uniqueid = t_credits.customerid','inner');
            $this->data['transaction'] = $this->credit_model->get_by(['MD5(t_credits.id)'=>$transactionid],true);
            $this->data['transaction_type'] = "CREDIT";
            $this->data['subview'] = 'customer/operations/receipt_page';
            $this->load->view('customer/_layout_main',  $this->data);
        }
    }    
    
}
