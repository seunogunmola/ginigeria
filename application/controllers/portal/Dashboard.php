<?php
//THIS CONTROLLER MANAGES ADMIN USER DASHBOARD
class Dashboard extends Portal_controller{
    function __construct() {
        parent::__construct();
    }
    
    public function index(){
        
        $this->data['subview'] = 'portal/dashboard_page';
        $this->load->view('portal/_layout_main',  $this->data);
    }
   

}
