<?php

class Credit extends Customer_controller{
    //You know what a Constructor Function does dont You?
    //If You dont know, kindly go and return your Salary
    public function __construct() {
        parent::__construct();
    }
    
    public function index(){
        $rules = $this->credit_model->rules;
        unset($rules['customerid']);
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run() == true){
            $data = $this->input->post();
            $data['customerid'] = $this->data['userdata']->uniqueid;
            $data['performedBy'] = $this->currentUsername;
            $saved = $this->credit_model->save($data);
            
            if($saved){
                //UPDATE LEDGER
                $this->balance_model->updateLedger($data['customerid'],$data['amount'],'CREDIT');
                
                $this->session->set_flashdata('success','Transaction Saved Successfully');
                redirect('customer/credit');
            }
            else{
                $this->session->set_flashdata('error','An error occurred');
                redirect('customer/credit');                
            }
            
        }
        $this->data['existing_customers'] = $this->customers_model->get();
        $this->data['subview'] = 'customer/operations/credit_page';
        $this->load->view('customer/_layout_main',  $this->data);
    }    
    
    public function history(){
        $this->db->select('t_customers.fullname,t_credits.*');
        $this->db->where('t_credits.customerid',$this->data['userdata']->uniqueid);
        $this->db->join('t_customers','t_customers.uniqueid = t_credits.customerid','inner');
        $this->data['transactions'] = $this->credit_model->get();
        $this->data['subview'] = 'customer/operations/credit_history_page';
        $this->load->view('customer/_layout_main',  $this->data);
    }    
    
    public function receipt($transactionid){
        if(!isset($transactionid)){
            redirect(site_url);
        }
        else{       
            $this->db->select('t_customers.fullname,t_credits.*');
            $this->db->join('t_customers','t_customers.uniqueid = t_credits.customerid','inner');
            $this->data['transaction'] = $this->credit_model->get_by(['MD5(t_credits.id)'=>$transactionid],true);
            $this->data['transaction_type'] = "CREDIT";
            $this->data['subview'] = 'customer/operations/receipt_page';
            $this->load->view('customer/_layout_main',  $this->data);
        }
    }    
    
}
