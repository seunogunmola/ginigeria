<?php

class Config extends Admin_controller {

    function __construct() {
        parent::__construct();
        $this->load->model('sessions_model');
        $this->load->model('activeSessions_model');
    }

    public function attachInstitutionFaculties() {

        $this->db->order_by('name asc');
        $this->data['institutions'] = $this->Institutions_model->get();

        $this->db->order_by('faculty_name asc');
        $this->data['faculties'] = $this->Faculties_model->get();

        $rules = $this->FacultyAssignModel->rules;

        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == TRUE) {
            $data = $this->input->post();
            $faculties = isset($data['faculties']) ? $data['faculties'] : NULL;
            $institution_id = $data['institution_id'];
            if ($institution_id == "") {
                $this->session->set_flashdata('error', 'The Institution Field is Required');
                redirect('admin/config/attachInstitutionFaculties');
            }

            if (count($faculties) == 0) {
                $this->session->set_flashdata('error', 'You must select at least one Faculty to be assigned');
                redirect('admin/config/attachInstitutionFaculties');
            } else {
                //GET INSTITUTION'S EXISTING FACULTIES
                $assignedFaculties = $this->FacultyAssignModel->getInstitutionsFaculties($institution_id, true);
                //IF INSTITUTION DOESNT HAVE EXISTING FACULTIES, INIT AN EMPTY ARRAY
                $assignedFacultiesArray = is_array($assignedFaculties) ? $assignedFaculties : array();
                $duplicateFacultiesArray = array();
                $savedCounter = 0;
                foreach ($faculties as $faculty):
                    //THIS IS NECESSARY TO PREVENT DUPLICATE ASIGNMENTS
                    if (in_array($faculty, $assignedFacultiesArray) == true) {
                        $duplicateFacultiesArray[] = $faculty;
                        continue;
                    } else {
                        $saved = $saved = $this->FacultyAssignModel->save(array('faculty_id' => $faculty, 'institution_id' => $institution_id, 'added_by' => $this->currentUsername));
                        if ($saved) {
                            $savedCounter++;
                        }
                    }
                endforeach;

                if ($savedCounter > 0) {
                    $institutionName = $this->Institutions_model->getName($institution_id);
                    $this->session->set_flashdata('success', $savedCounter . ' Faculties assigned to ' . $institutionName . " Successfully");
                } else {
                    if (count($duplicateFacultiesArray) > 0) {
                        $this->session->set_flashdata('error', "No Assignment was made because the selected Faculties have already been assigned");
                    } else {
                        $this->session->set_flashdata('error', "An error occured, please try again later");
                    }
                }
                redirect('admin/config/attachInstitutionFaculties');
            }
        } else {
            $data['error_message'] = trim(validation_errors() != FALSE ? validation_errors() : '');
        }
        $this->data['subview'] = 'admin/config/attachInstitutionFaculties';
        $this->load->view('admin/_layout_main', $this->data);
    }

    public function viewFacultyAssignments($hashedId = NULL) {
        if ($hashedId == NULL) {
            $sql = "SELECT institution_id, count(faculty_id) as facultiesCount,name FROM t_institutions_faculties"
                    . " INNER JOIN t_institutions ON t_institutions_faculties.institution_id = t_institutions.id"
                    . " GROUP BY institution_id"
                    . " ORDER BY name asc";
            $this->data['facultiesAssigned'] = $this->db->query($sql)->result();

            $this->data['subview'] = 'admin/config/viewInstitutionFaculties';
            $this->load->view('admin/_layout_main', $this->data);
        } else {
            $sql = "SELECT institution_id,name,faculty_name FROM t_institutions_faculties"
                    . " INNER JOIN t_institutions ON t_institutions_faculties.institution_id = t_institutions.id"
                    . " INNER JOIN t_faculties ON t_institutions_faculties.faculty_id = t_faculties.id"
                    . " WHERE md5(institution_id)='" . $hashedId . "'"
                    . " ORDER BY faculty_name asc";
            $this->data['facultiesAssigned'] = $this->db->query($sql)->result();
            if (count($this->data['facultiesAssigned'])) {
                $this->data['subview'] = 'admin/config/viewIndividualInstitutionFaculties';
                $this->load->view('admin/_layout_main', $this->data);
            } else {
                redirect('admin/config/viewFacultyAssignments');
            }
        }
    }

    public function attachInstitutionDepartments() {
        $this->data['institutions'] = $this->Institutions_model->getInstitutionsArray();

        $this->db->order_by('department_name asc');
        $this->data['departments'] = $this->Departments_model->get();

        $rules = $this->departmentAssignModel->rules;

        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == TRUE) {
            $data = $this->input->post();
            $departments = isset($data['departments']) ? $data['departments'] : NULL;
            $institution_id = $data['institution_id'];
            $faculty_id = $data['faculty_id'];
            if (count($departments) == 0) {
                $this->session->set_flashdata('error', 'You must select at least one Department to be assigned');
                redirect('admin/config/attachInstitutionDepartments');
            } else {
                //GET INSTITUTION'S EXISTING FACULTIES
                $assignedDepartments = $this->departmentAssignModel->getInstitutionsDepartments($institution_id, true);
                //IF INSTITUTION DOESNT HAVE EXISTING FACULTIES, INIT AN EMPTY ARRAY
                $assignedDepartmentsArray = is_array($assignedDepartments) ? $assignedDepartments : array();
                $duplicateDepartmentsArray = array();
                $savedCounter = 0;
                foreach ($departments as $department):
                    //THIS IS NECESSARY TO PREVENT DUPLICATE ASIGNMENTS
                    if (in_array($department, $assignedDepartmentsArray) == true) {
                        $duplicateDepartmentsArray[] = $department;
                        continue;
                    } else {
                        $saved = $saved = $this->departmentAssignModel->save(array('department_id' => $department, 'faculty_id' => $faculty_id, 'institution_id' => $institution_id, 'added_by' => $this->currentUsername));
                        if ($saved) {
                            $savedCounter++;
                        }
                    }
                endforeach;

                if ($savedCounter > 0) {
                    $institutionName = $this->Institutions_model->getName($institution_id);
                    $this->session->set_flashdata('success', $savedCounter . ' Departments assigned to ' . $institutionName . " Successfully");
                } else {
                    if (count($duplicateFacultiesArray) > 0) {
                        $this->session->set_flashdata('error', "No Assignment was made because the selected Faculties have already been assigned");
                    } else {
                        $this->session->set_flashdata('error', "An error occured, please try again later");
                    }
                }
                redirect('admin/config/attachInstitutionDepartments');
            }
        } else {
            $data['error_message'] = trim(validation_errors() != FALSE ? validation_errors() : '');
        }
        $this->data['subview'] = 'admin/config/attachInstitutionDepartments';
        $this->load->view('admin/_layout_main', $this->data);
    }

    public function viewDepartmentAssignments($hashedId = NULL) {
        if ($hashedId == NULL) {
            $sql = "SELECT institution_id, count(department_id) as departmentsCount,name FROM t_institutions_departments"
                    . " INNER JOIN t_institutions ON t_institutions_departments.institution_id = t_institutions.id"
                    . " GROUP BY institution_id"
                    . " ORDER BY name asc";
            $this->data['departmentsAssigned'] = $this->db->query($sql)->result();

            $this->data['subview'] = 'admin/config/viewInstitutionDepartments';
            $this->load->view('admin/_layout_main', $this->data);
        } else {
            $sql = "SELECT institution_id,name,faculty_name,department_name FROM t_institutions_departments"
                    . " INNER JOIN t_institutions ON t_institutions_departments.institution_id = t_institutions.id"
                    . " INNER JOIN t_faculties ON t_institutions_departments.faculty_id = t_faculties.id"
                    . " INNER JOIN t_departments ON t_institutions_departments.department_id = t_departments.id"
                    . " WHERE md5(institution_id)='" . $hashedId . "'"
                    . " ORDER BY faculty_name asc,department_name asc";
            $this->data['departmentsAssigned'] = $this->db->query($sql)->result();
            if (count($this->data['departmentsAssigned'])) {
                $this->data['subview'] = 'admin/config/viewIndividualInstitutionDepartments';
                $this->load->view('admin/_layout_main', $this->data);
            } else {
                redirect('admin/config/viewFacultyAssignments');
            }
        }
    }

    public function createSession($id = NULL, $delete = FALSE) {
        if ($delete == true && $id != NULL) {
            $sessionInUse = false;
            if ($sessionInUse == false) {
                $deleted = $this->sessions_model->delete($id);
                if ($deleted) {
                    $this->session->set_flashdata('success', 'Session deleted successfully');
                } else {
                    $this->session->set_flashdata('error', 'An error occurred while deleting session');
                }
            } else {
                $this->session->set_flashdata('error', 'The session is in use by Institutions, You cannot delete it');
            }
            redirect('admin/config/createSession');
        }

        $rules = $this->sessions_model->createRules;
        $this->form_validation->set_rules($rules) or die('Didnt set any rules');
        if ($this->form_validation->run() == TRUE) {
            $data = $this->input->post();
            //CHECK IF SESSION NAME ALREADY EXISTS
            $nameExists = $this->sessions_model->checkValue('session_name', $data['session_name']);
            if ($nameExists == true) {
                $this->data['error_message'] = getAlertMessage('Error : A session with the name ' . $data['session_name'] . ' already exists on the system.');
            } else {
                $data['created_by'] = $this->currentUsername;
                $saved = $this->sessions_model->save($data);
                if ($saved) {
                    $this->session->set_flashdata('success', 'Session ' . $data['session_name'] . ' created successfully');
                    redirect('admin/config/createSession');
                } else {
                    $this->data['error_message'] = getAlertMessage('An error occurred. Please try again');
                }
            }
        } else {
            $this->data['error_message'] = trim(validation_errors()) != false ? getAlertMessage(validation_errors()) : '';
        }

        $this->data['sessions'] = $this->sessions_model->get();
        $this->data['subview'] = 'admin/config/createSession';
        $this->load->view('admin/_layout_main', $this->data);
    }

    public function activateSession() {

        $this->db->order_by('name asc');
        $this->data['institutions'] = $this->Institutions_model->get();
        $this->data['sessions'] = $this->sessions_model->get();
        
        $rules = $this->activeSessions_model->rules;
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == TRUE) {
            $data = $this->input->post();
            //CHECK EXISTING SESSION
            $existingSession = $this->activeSessions_model->get_by(['institution_id'=>$data['institution_id']],true);
            if(count($existingSession > 0)){
                //update
                $saved = $this->activeSessions_model->save($data,$existingSession->id);
            }
            else{
                //insert
                $saved = $this->activeSessions_model->save($data);
            }
            if($saved){
                $this->session->set_flashdata('success','Session Activated Successfully');
            }
            else{
                $this->session->set_flashdata('error','An error occured while activating session');
            }
            redirect('admin/config/activateSession');
        }
        else {
            $data['error_message'] = trim(validation_errors() != FALSE ? validation_errors() : '');
        }
        
        $sql="SELECT t_institutions.name as institution_name, t_sessions.session_name, t_active_session.* FROM"
                . " t_active_session "
                . " INNER JOIN t_institutions ON t_institutions.id = t_active_session.institution_id "
                . " INNER JOIN t_sessions ON t_sessions.id = t_active_session.session_id "
                . " ORDER BY institution_name asc";
        $this->data['activeSessions'] = $this->db->query($sql)->result();

        $this->data['subview'] = 'admin/config/activateSession';
        $this->load->view('admin/_layout_main', $this->data);
    }

}
