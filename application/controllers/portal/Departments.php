<?php
class Departments extends Admin_controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('Departments_model');
    }
    
    public function create($hashedId=NULL){
        $this->data['hashedId']=$hashedId;
        //IF UNIQUE ID IS NULL, INIT NEW USER
        if($hashedId==NULL){
            $this->data['department'] = $this->Departments_model->get_new();    
        }
        else{
            $this->data['department'] = $this->Departments_model->get_by(['MD5(id)'=>$hashedId],true); 
            //THE SUPPLIED ID DOESNT BELONG TO ANY DEPARTMENT
            if(!count($this->data['department'])){
                $this->session->set_flashdata('error','Department details not found');
                redirect('admin/departments/create');
            }
        }
        //GET VALIDATION RULES
        $rules = $this->Departments_model->rules;
        //SET VALIDATION RULES
        $this->form_validation->set_rules($rules);
        //FIRE RULES
        if($this->form_validation->run()==TRUE){
            //RULES PASSED
            $data = $this->input->post();
            //CHECK DEPARTMENT NAME
            $nameExists = $this->Departments_model->checkValue($field='department_name',$data['department_name'],$this->data['department']->id);
            $codeExists = $this->Departments_model->checkValue($field='department_code',$data['department_code'],$this->data['department']->id);
            if($nameExists == true){
                $this->data['error_message'] = getAlertMessage('Department Name: '.$data['department_name'].' already exists on our System. Please choose a different name or use existing departments. Sey You get?');
            }
            else if($codeExists == true){
                $this->data['error_message'] = getAlertMessage('Department Code: '.$data['department_code'].' already exists on our System. Please choose a different code or use existing departments. Sey You get?');
            }
            else{
                if($hashedId==NULL){
                    $data['added_by'] = $this->currentUsername;
                }
                $saved = $this->Departments_model->save($data,$this->data['department']->id);
                if($saved){
                    if($hashedId==NULL){
                        $this->session->set_flashdata('success','New Department created Successfully');    
                        $action_details = "A new Department with name : ".$data['department_name']." was created by user:".$this->currentUsername." on ".date('Y-m-d h:i:s');
                        $this->Audit_model->logAction("DEPARTMENT CREATION",$action_details,$institution_id=NULL);
                        redirect('admin/departments/create');
                    }
                    else{
                        $this->session->set_flashdata('success','Department Updated Successfully');    
                        $action_details = "A Department with name : ".$data['department_name']." was updated by user:".$this->currentUsername." on ".date('Y-m-d h:i:s');
                        $this->Audit_model->logAction("DEPARTMENT UPDATE",$action_details,$institution_id=NULL);
                        redirect('admin/departments/view');
                    }
                }
                else{
                    $this->data['error_message'] = getAlertMessage('No Vex o, An Error occurred while creating Institution, You should try again');
                }
            }
        }
        
        //GET EXISTING USERS
        $this->data['departments'] = $this->Departments_model->get();
        $this->data['subview'] = 'admin/departments/createPage';
        $this->load->view('admin/_layout_main',  $this->data);
    }
    public function view($hashedId=NULL){
        if($hashedId==NULL){
            //GET ALL FACULTIES
            $this->data['departments'] = $this->Departments_model->get();
            $this->data['subview'] = 'admin/departments/viewPage';
            $this->load->view('admin/_layout_main',  $this->data);
        }
        else{
            //GET SINGLE DEPARTMENT
            $this->data['department'] = $this->Departments_model->get_by(['MD5(id)'=>$hashedId],true);    
            if(count($this->data['department'])){
                $this->data['subview'] = 'admin/departments/singleViewPage';
                $this->load->view('admin/_layout_main',  $this->data);
            }
            else{
                $this->session->set_flashdata('error','Department details not found');
                redirect('admin/departments/create');   
            }
            
        }

    }
    
    public function delete($hashedId){
        $where = array('MD5(id)'=>$hashedId);
        $instData = $this->Departments_model->get_by($where,TRUE);
        if(count($instData)){
            $deleted = $this->Departments_model->delete($instData->id);
            if($deleted){
                //PERFORM A TABULAR RAZA...GOOD IMPLEMENTATION BUT BAD FOR NIGERIAN USERS
                    //$this->Departments_model->tabularRaza($userid);
                //LOG AUDIT
                $action_details = "A Department with name : ".$instData->department_name." was deleted by user:".$this->currentUsernme." on ".date('Y-m-d h:i:s');
                $this->Audit_model->logAction("DEPARTMENT DELETED",$action_details,$institution_id=NULL);
                //REDIRECT 
                $this->session->set_flashdata('success','Department deleted Successfully');
                redirect('admin/departments/view');
            }
            else{
                //THE VILLAGE PEOPLE DIDNOT ALLOW IT TO DELETE
                //REDIRECT 
                $this->session->set_flashdata('error','An Error Occurred while trying to delete, You shoud try again');
                redirect('admin/departments/view');
            }
        }
        else{
            $this->session->set_flashdata('error','No Records Found');
            redirect('admin/departments/view');
        }
    }
}
