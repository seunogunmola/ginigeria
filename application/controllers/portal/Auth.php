<?php
/*Code is Poetry, write to Entertain the next programmer
Auth Controller handles Authentication for the Admin Users
 */
class Auth extends Base_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('users_model');
        $this->load->model('password_model');
    }
    
    public function login($passwordRecovery=NULL) {
       
        if($passwordRecovery!= NULL){
            $this->data['passwordRecovery'] = true;
        }
        //GET VALIDATION RULES FROM ADMIN MODEL
        $rules = $this->users_model->login_rules;
        //SET THE RULES
        $this->form_validation->set_rules($rules);
        //CHECK IF THE RULES ARE MET
        if($this->form_validation->run()==TRUE){            
            //COLLECT DATA FROM LOGIN FORM
            $data = $this->input->post();
            //AUTHENTICATE THE USER
            $this->db->where('privileges','graduate');
            $status = $this->users_model->performLogin($data['email_address'],$data['password']);
            
            if($status==FALSE){
                $this->data['error_message'] = getAlertMessage('Invalid Username/Password <br/> Please try again or Register if You dont have an Account yet');
            }
            else if($status==="disabled"){                
                $this->data['error_message'] = getAlertMessage('Your account has not been activated yet. <br/> Contact Admin via : 08023680464,08099605346');
            }
            else if($status===true){
                //LOG AUDIT ACTION
                $action_details = "A user with username : ".$data['email_address']." logged in Successfully at ".date('Y-m-d h:i:s');
                $this->Audit_model->logAction("LOGIN",$action_details);
                redirect('portal/dashboard');
            }                    
        }
        //TRIGGER AN ERROR IF VALIDATION ERRORS EXIST
        else {
            $this->data['error_message'] = trim(validation_errors())!=FALSE?getAlertMessage(validation_errors()):'';
        }
        //LOAD LOGIN VIEW
        $this->load->view('portal/login_page',  $this->data);
    }
    
    public function logout(){
        //DESTROY ALL ACTIVE SESSION DATA
        $this->session->unset_userdata('userLoggedin');
        $this->session->unset_userdata('activeUserdata');
        //LOG AUDIT ACTION
        $action_details = "A user with username : ".$data['email_address']." logged out Successfully at ".date('Y-m-d h:i:s');
        $this->Audit_model->logAction("LOGOUT",$action_details);
        //REDIRECT TO LOGIN
        redirect('portal/auth/login');
    }
    
    public function passwordRecovery(){
        
        $this->form_validation->set_rules('email_address','Email Address','trim|required');
        if($this->form_validation->run()==true){
            $email_address = $this->input->post('email_address');
            
            //CHECK IF EMAIL EXISTS
        //CHECK FOR USER
            $this->db->where('email_address',$email_address);
            $userData = $this->users_model->get(NULL,true);            
            if(count($userData)){
                    $token = abs(mt_rand(111111, 999999));
                    //SEND PASSWORD REQUEST
                    //REMOVE ANY PREVIOUS REQUESTS
                    $sql = "DELETE FROM t_password_change_requests WHERE customerid = '".$userData->uniqueid."'";
                    $this->db->query($sql);
                    //SAVE NEW REQUEST
                    $requestData = array('customerid'=>$userData->uniqueid,'token'=>$token);
                    $this->password_model->save($requestData);
                    //SEND RECOVERY EMAIL TO USER
                    //SEND EMAIL                    
                    $content = "<div style='font-size:20px'> Dear  ".$userData->fullname.", <br/>"
                            . "We got your request to change your Password <br/>"
                            . "Click the button below to Reset your Password <br/>"
                            . "<a href='".site_url('portal/auth/changePassword/'.$userData->uniqueid.'/'.$token)."'  target='_blank'> <button style='padding:1em;color:white; border-radius: 50px 50px 50px 50px; background:green;font-size:20px;'> Change your Password </button></a> </div>";                
                    
                    $emailSent = $this->sendEmail($userData->fullname, $email_address, $content, $subject = "PASSWORD RESET ON MANIFOLD SPECTRUM");
                    
                    if($emailSent){
                            $message = "A Password Recovery Email has been sent to : $email_address. <br/> Please check your email and follow the instructions to reset your password ";
                            $this->session->set_flashdata('success',$message);                        
                    }
                    }
            else{
                $message = "Error: The Email Address You entered doesnt not exists on our database.";
                $this->session->set_flashdata('error',$message);
            }           
        }
        else{            
            $message = trim(validation_errors())!=false? validation_errors():'';
            $this->session->set_flashdata('error',$message);
        }
        redirect('portal/auth/login/true');
    }
    
    
    function changePassword($uniqueid,$token){
        //CHECK IF UNIQUEID WAS SUPPLIED
        if(!isset($uniqueid)){
            redirect(site_url('portal/auth/login'));
        }
        else{
            //CHECK IF CUSTOMER EXISTS
            $customerData = $this->users_model->get_by(['uniqueid'=>$uniqueid],true);            
            if(count($customerData)){
                //CHECK IF THERE IS ANY UN-USED REQUEST
                $where = array('customerid'=>$uniqueid,'token'=>$token);
                $existingRequest = $this->password_model->get_by($where,true);
                
                if(!count($existingRequest)){
                    $this->session->set_flashdata('error','The link You followed has expired');
                    redirect('portal/auth/login');                    
                }
                
                //CHECK IF FORM WAS SUBMITTED
                $this->form_validation->set_rules('password','New Password','trim|required');
                $this->form_validation->set_rules('confirm_password','Confirm New Password','trim|required|matches[password]');
                
                if($this->form_validation->run() == true){
                    //GRAB PASSWORD
                    $password = $this->input->post('password');
                    
                   //HASH PASSWORD
                    $hashedPassword = $this->users_model->hash($password);
                    
                    //UPDATHE ACCOUNT
                    $updateData= array('password'=>$hashedPassword);
                    
                    $saved = $this->users_model->save($updateData,$customerData->id);
                    if($saved){
                        $this->password_model->delete_where(['customerid'=>$uniqueid]);
                        
                        $message = "Your Password was Changed Successfully.You can now login to your account.";
                        $this->session->set_flashdata('success',$message);    
                        redirect(site_url('portal/auth/login'));
                    }
                    else{
                        $message = "An error occurred. Please try again";
                        $this->session->set_flashdata('error',$message);    
                    }
                    
                }
                else{
                    //TRIGGER ANY VALIDATION ERRORS
                    $this->data['error_message'] = trim(validation_errors())!=false ? getAlertMessage(validation_errors()) : '';
                }
                
                $this->data['customerData'] = $customerData;
                //LOAD VIEW
                $this->load->view('portal/changePassword',  $this->data);                
            }
            else{
                $this->session->set_flashdata('error','Invalid Customer Data');
                redirect('portal/auth/login');
            }
        }
    }
    
    public function register() {
        //GET VALIDATION RULES FROM ADMIN MODEL
        $rules = $this->Users_model->rules;
        //SET THE RULES
        $this->form_validation->set_rules($rules);
        //CHECK IF THE RULES ARE MET
        if($this->form_validation->run()==TRUE){            
            //COLLECT DATA FROM LOGIN FORM
            $data = $this->input->post();
            
            //CHECK IF EMAIL EXISTS
            $emailExists = $this->Users_model->checkValue('email_address',$data['email_address'],$id=NULL);
            
            if($emailExists == false){
                $data['status'] = 1;
                $data['privileges'] = 'graduate';
                $data['password'] = $this->Users_model->hash($data['password']);
                unset($data['confirm_password']);
                $saved = $this->Users_model->save($data);

                if($saved){
//                    //SEND EMAIL
//                    $fullname = $data['surname']." ".$data['firstname']." ".$data['othernames'];
//                    $details = "<h4> Welcome to Graduates Initiative Network </h4>";
//                    $details.= "Your account was created successfully with the follow details <br/>"
//                            . " Email Address : ".$data['email_addresss'];
//                    $this->sendEmail($fullname, $data['email_address'], $details, $subject = 'Welcome to Graduates Initiative Network');
//                    
//                    //SEND SMS
//                    $message = "Welcome to Graduate Initiative Network \n Your account has been created with the Email ".$data['email_address'];
//                    sendsms(config_item('smsid'), $message , $data['phone_number']);
//                   
                    $this->session->set_flashdata('success','You account was created successfully');
                    redirect('portal/auth/login');
                }
                else{
                    $this->data['error_message'] = getAlertMessage('An error occurred while creating your account. Please try again');
                }
                
            }
            else{
                $this->data['error_message'] = getAlertMessage('The Email Address '.$data['email_address'].' already exists');
            }
        }
        //TRIGGER AN ERROR IF VALIDATION ERRORS EXIST
        else {
            $this->data['error_message'] = trim(validation_errors())!=FALSE?getAlertMessage(validation_errors()):'';
        }
        //LOAD LOGIN VIEW
        $this->load->view('portal/register_page',  $this->data);
    }    
}
