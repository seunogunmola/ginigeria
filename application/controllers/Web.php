<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends Base_controller {

    public function __construct() {
        parent::__construct();
    }
        
    public function index(){
        $this->data['subview'] = 'web/welcome_page';
        $this->load->view('web/templates/layout_main',  $this->data);
    }
    
    public function about(){
        $this->data['subview'] = 'web/about_page';
        $this->load->view('web/templates/layout_main',  $this->data);
    }
    
    public function contact(){
        $this->data['subview'] = 'web/contact_page';
        $this->load->view('web/templates/layout_main',  $this->data);
    }
   
    
    public function register(){
        $rules = array(
            'fullname'=>array('field'=>'fullname','label'=>'Fullname','rules'=>'trim|required'),          
            'phone_number'=>array('field'=>'phone_number','label'=>'Phone Number','rules'=>'trim|required|min_length[11]|max_length[14]'),
            'email_address'=>array('field'=>'email_address','label'=>'Email Address','rules'=>'trim|required|is_unique[t_users.email_address]'),
            'password'=>array('field'=>'password','label'=>'Password','rules'=>'trim|required'),
            'confirm_password'=>array('field'=>'confirm_password','label'=>'Confirm Password ','rules'=>'trim|required|matches[password]'),
        );
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run()==TRUE){
            $data = $this->input->post();
            
            //CHECK IF EMAIL ALREADY EXISTS
            $nameExists = $this->Customers_model->checkValue('email_address',$data['email_address']);
            
            if($nameExists==TRUE){
                $this->data['error_message'] = getAlertMessage('The Email Address You provided already exists on our Database. If You already have an account, You should LOGIN instead');   
            }
            else{
                $data['uniqueid'] = $this->Customers_model->generate_unique_id($len = 10, 'uniqueid');
                $data['status'] = 0;

                unset($data['confirm_password']);
                $data['password'] = $this->Customers_model->hash($data['password']);

                $saved = $this->Customers_model->save($data);

                if($saved){
                    
                    //SEND EMAIL
                    $content =  "<table width='80%' border='1' style='border-collapse:collapse'>"
                            . "<tr> <td>Fullname<td><td>". $data['fullname']."</td></tr>"
                            . "<tr> <td>Phone Number<td><td>". $data['phone_number']."</td></tr>"
                            . "<tr> <td>Email Address<td><td>". $data['email_address']."</td></tr>"
                            . "</table>";
                    
                    $details = "Dear  ".$data['fullname'].","
                            . "<h4>Welcome to Manifolds Spectrum Multipurpose Society </h4>"
                            . "Your account was created Successfully. <br/>"
                            . "Find your registration details below;"
                            . $content
                            . "<h4 style='color:crimson'>Next Steps </h4>"
                            . "Your account will be activated in the next 24 hours. <br/>"
                            . "If You do not get an activation email in 24 hours, kindly call any of these Numbers to Activate your Account <br/>"
                            . " 08023680464,08099605346,08060132644 <br/>"
                            . "After activating your account, You can click the button below to login <br/>"
                            . "<a href='https://manifoldscoop.com/customer/auth/login'> <button style='padding:1em;color:white;background:blue;font-size:20px;'> Login to Your Account </button></a>";
                    
                    
                    $control_email = "<h4>New User Registration</h4>"
                            . "A new Member registered on Manifolds Website. Find the registration details below;"
                            . $content
                            . "Kindly Login and Activate the Member's Account<br/>"
                            . "<a href='https://manifoldscoop.com/admin/auth/login'> <button style='padding:2em;color:white;background:green;font-size:20px;'> Click Here to Login</button></a>";
                    
                    
                    //SEND USER MAIL
                    $this->sendEmail($data['fullname'], $data['email_address'], $details, $subject = "WELCOME TO MANIFOLDS SPECTRUM");
                    //SEND CONTROL MAIL
                    $this->sendEmail('MANIFOLD ADMIN', config_item('control_email'), $control_email, $subject = "NEW USER REGISTRATION");
                    
                    $this->session->set_flashdata('success',"Your Account was created Successfully. <a href = '".site_url('customer/auth/login') ."'> Click Here to Login </a>");
                    //LOG AUDIT ACTION
                    $this->load->model('Audit_model');
                    $action_details = "A new user with email : ".$data['email_address']."registered on School Connect on: ".date('Y-m-d h:i:s');
                    $this->Audit_model->logAction("USER REGISTRATION",$action_details,$institution_id=NULL);
                    redirect('web/register');
                }
                else{
                    $this->data['error_message'] = getAlertMessage('An Error Occurred while creating your account, You should try again');
                }
            }
        }
        else{
            $this->data['error_message'] = trim(validation_errors())!=FALSE?getAlertMessage(validation_errors()):'';
        }
        $this->data['subview'] = 'web/register_page';
        $this->load->view('web/templates/layout_main',  $this->data);        
    }
    
       
}
