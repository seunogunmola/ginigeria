<?php

class Credit extends Admin_controller{
    //You know what a Constructor Function does dont You?
    //If You dont know, kindly go and return your Salary
    public function __construct() {
        parent::__construct();
    }
    
    public function index(){
        $rules = $this->credit_model->rules;
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run() == true){
            $data = $this->input->post();
            $data['performedBy'] = $this->currentUsername;
            $data['status'] = 0;
            
            $saved = $this->credit_model->save($data);
            
            if($saved){
                //UPDATE LEDGER                
                $this->session->set_flashdata('success','Transaction Posted Successfully');
                redirect('admin/credit');
            }
            else{
                $this->session->set_flashdata('error','An error occurred');
                redirect('admin/credit');                
            }
            
        }
        $this->data['existing_customers'] = $this->customers_model->get_by(['status'=>'1']);
        $this->data['subview'] = 'admin/operations/credit_page';
        $this->load->view('admin/_layout_main',  $this->data);
    }    
    
    public function pending(){
        $this->db->select('t_customers.fullname,t_credits.*');
        $this->db->join('t_customers','t_customers.uniqueid = t_credits.customerid','inner');
        $this->db->where('t_credits.status = 0');
        $this->data['transactions'] = $this->credit_model->get();
        $this->data['subview'] = 'admin/operations/pending_credit_page';
        $this->load->view('admin/_layout_main',  $this->data);
    }    
    
    public function history(){
        $this->db->select('t_customers.fullname,t_credits.*');
        $this->db->join('t_customers','t_customers.uniqueid = t_credits.customerid','inner');
        $this->data['transactions'] = $this->credit_model->get();
        $this->data['subview'] = 'admin/operations/credit_history_page';
        $this->load->view('admin/_layout_main',  $this->data);
    }    
    
    public function approve($transactionid){
        if(!isset($transactionid)){
            redirect('admin/credit/pending');
        }
        else{
            //GET TRANSACTION DATA
            $transactionData = $this->credit_model->get_by(['MD5(t_credits.id)'=>$transactionid],true);
            
            if(count($transactionData)) {
                //UPDATE LEDGER                
                $ledgerUpdated = $this->balance_model->updateLedger($transactionData->customerid,$transactionData->amount,'CREDIT');
                
                if($ledgerUpdated){
                    //APPROVE TRANSACTION
                    $approved = ['status'=>'1'];
                    $saved = $this->credit_model->save($approved,$transactionData->id);
                    
                    if($saved){
                        //SEND SMS
                        $customerData = $this->customers_model->get_by(['uniqueid'=>$transactionData->customerid],true);
                        $name = ucwords(strtolower($customerData->fullname));
                        
                        $message = "Dear $name, Your  $transactionData->amount Naira contribution made on $transactionData->transaction_date has been updated. Please check your dashboard to confirm. Thank You";
                        
                        $sent = sendsms("Manifolds", $message, $customerData->phone_number);
                        
//UPDATE AUDIT LOG
                        $this->session->set_flashdata('success','Transaction Approved Successfully');
                        redirect('admin/credit/pending');
                    }
                    else{
                        $this->session->set_flashdata('error','An error occurred. Please try again');
                        redirect('admin/credit/pending');                        
                    }
                }
                else{
                        $this->session->set_flashdata('error','An error occurred. Please try again');
                        redirect('admin/credit/pending');                      
                }
                
                $this->data['transaction_type'] = "CREDIT";
                $this->data['subview'] = 'admin/operations/receipt_page';
                $this->load->view('admin/_layout_main',  $this->data);
            }
            else{
                $this->session->set_flashdata('error','Invalid Transaction Data');
                redirect('admin/credit/pending');
            }
        }
    }     
    
    public function receipt($transactionid){
        if(!isset($transactionid)){
            redirect(site_url);
        }
        else{       
            $this->db->select('t_customers.fullname,t_credits.*');
            $this->db->join('t_customers','t_customers.uniqueid = t_credits.customerid','inner');
//            $this->db->where('t_credits.status=1');
            $this->data['transaction'] = $this->credit_model->get_by(['MD5(t_credits.id)'=>$transactionid],true);
            $this->data['transaction_type'] = "CREDIT";
            $this->data['subview'] = 'admin/operations/receipt_page';
            $this->load->view('admin/_layout_main',  $this->data);
        }
    }    
    
    public function delete($transactionid){
        if(!isset($transactionid)){
            redirect('admin/credit/history');
        }
        else{
            //GET TRANSACTION DATA
            $transactionData = $this->credit_model->get_by(['MD5(t_credits.id)'=>$transactionid],true);

            if(count($transactionData)) {
                //UPDATE LEDGER                
                $ledgerUpdated = $this->balance_model->updateLedger($transactionData->customerid,$transactionData->amount,'DEBIT');
                
                if($ledgerUpdated){
                    $deleted = $this->credit_model->delete($transactionData->id);

                    if($deleted){
                        //UPDATE AUDIT LOG
                        $this->session->set_flashdata('success','Transaction Deleted Successfully');
                        redirect('admin/credit/history');
                    }
                    else{
                        $this->session->set_flashdata('error','An error occurred. Please try again');
                        redirect('admin/credit/history');                  
                    }
                }
                else{
                        $this->session->set_flashdata('error','An error occurred. Please try again');
                        redirect('admin/credit/history');                       
                }
            }
            else{
                $this->session->set_flashdata('error','Invalid Transaction Data');
                redirect('admin/credit/history');   
            }
        }
    }         
    
}
