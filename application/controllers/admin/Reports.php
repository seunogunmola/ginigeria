<?php

class Reports extends Admin_controller{
    //You know what a Constructor Function does dont You?
    //If You dont know, kindly go and return your Salary
    public function __construct() {
        parent::__construct();
    }
       
    public function credit(){
        $this->data['report_title'] = "Credit Transactions Report";
        $transaction_channel = " All";
        if($this->input->post()){
            $fromDate = $this->input->post('fromDate');
            $toDate = $this->input->post('toDate');
            
            $channel = $this->input->post('channel');                        
            if($channel !== 'all'){
                $this->db->where('channel',$channel);
                $transaction_channel = $channel;
            }
            
            if($fromDate > $toDate){
                $this->data['error_message'] = "Error: Your FROM DATE cannot be greater than your TO DATE";
            }
            else{
                $this->db->where("t_credits.transaction_date between '".$fromDate."' AND '".$toDate."'");
                $this->data['report_title'] = "Credit Transactions Report between ".$fromDate." and $toDate";
                           
            }
        }
        $this->data['report_title'] .= " via ". $transaction_channel ." Channel";
        $this->db->select('t_customers.fullname,t_credits.*');
        $this->db->join('t_customers','t_customers.uniqueid = t_credits.customerid','inner');
        $this->db->order_by('t_credits.transaction_date desc');
        $this->data['transactions'] = $this->credit_model->get();
        
        $this->data['subview'] = 'admin/reports/credit_report';
        $this->load->view('admin/_layout_main',  $this->data);
    }    
    
    public function debit(){
        $this->data['report_title'] = "Debit Transactions Report";
        $transaction_channel = " All";
        
        if($this->input->post()){
            $fromDate = $this->input->post('fromDate');
            $toDate = $this->input->post('toDate');
            
            $channel = $this->input->post('channel');                        
            if($channel !== 'all'){
                $this->db->where('channel',$channel);
                $transaction_channel = $channel;
            }            
            
            if($fromDate > $toDate){
                $this->data['error_message'] = "Error: Your FROM DATE cannot be greater than your TO DATE";
            }
            else{
                
                $this->db->where("t_debits.transaction_date between '".$fromDate."' AND '".$toDate."'");
                $this->data['report_title'] = "Debit Transactions Report between ".$fromDate." and $toDate";
                           
            }
        }
        
        $this->data['report_title'] .= " via ". $transaction_channel ." Channel";
        
        $this->db->select('t_customers.fullname,t_debits.*');
        $this->db->join('t_customers','t_customers.uniqueid = t_debits.customerid','inner');
        $this->db->order_by('t_debits.transaction_date desc');
        $this->data['transactions'] = $this->debit_model->get();
        
        $this->data['subview'] = 'admin/reports/debit_report';
        $this->load->view('admin/_layout_main',  $this->data);
} 

    public function individual(){
        
        
        if($this->input->post()){
            $customerid = $this->input->post('customerid'); 
            //GET CUSTOMER DETAILS
            $this->db->select('UPPER(fullname) as fullname');
            $customerData = $this->customers_model->get_by(['uniqueid'=>$customerid],true);
            
            $this->data['report_title'] = "Individual Member Transactions Report for ".$customerData->fullname;
            $fromDate = $this->input->post('fromDate');
            $toDate = $this->input->post('toDate');
            
            if(trim($fromDate)!=false && trim($toDate)!=false) {
                
                if($fromDate > $toDate){
                    $this->data['error_message'] = "Error: Your FROM DATE cannot be greater than your TO DATE";
                }
                else {
                    $this->db->where("transaction_date BETWEEN $fromDate AND $toDate"); 
                    $this->data['report_title'] = "Individual Member Transactions Report for $customerData->fullname between $fromDate and $toDate";
                }
            }
                $this->db->where('customerid',$customerid);
                $this->db->join('t_customers','t_customers.uniqueid = t_debits.customerid','inner');
                $this->db->order_by('t_debits.transaction_date desc');
                $this->data['debit_transactions'] = $this->debit_model->get();     
                
                $this->db->where('customerid',$customerid);
                $this->db->join('t_customers','t_customers.uniqueid = t_credits.customerid','inner');
                $this->db->order_by('t_credits.transaction_date desc');
                $this->data['credit_transactions'] = $this->credit_model->get();   
                
        }
        $this->data['transactions'] = NULL;
        $this->db->order_by('fullname asc');
        $this->data['existing_customers'] = $this->customers_model->get_by(['status'=>'1']);
        
        $this->data['subview'] = 'admin/reports/individual_report';
        $this->load->view('admin/_layout_main',  $this->data);
}    
     
   
    public function receipt($transactionid){
        if(!isset($transactionid)){
            redirect(site_url);
        }
        else{       
            $this->db->select('t_customers.fullname,t_credits.*');
            $this->db->join('t_customers','t_customers.uniqueid = t_credits.customerid','inner');
//            $this->db->where('t_credits.status=1');
            $this->data['transaction'] = $this->credit_model->get_by(['MD5(t_credits.id)'=>$transactionid],true);
            $this->data['transaction_type'] = "CREDIT";
            $this->data['subview'] = 'admin/operations/receipt_page';
            $this->load->view('admin/_layout_main',  $this->data);
        }
    }    
    
}
