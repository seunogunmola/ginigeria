<?php
/*Make the world a better place
 * Write Code the developer coming after You and the ones coming after them
 */
//THIS CONTROLLER MANAGES ADMIN USERS
class Customers extends Admin_controller{
    //You know what a Constructor Function does dont You?
    //If You dont know, kindly go and return your Salary
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pending=FALSE){
        if($pending == TRUE){
            $this->db->where('status','0');
            $this->data['title'] = "Customers that have not been activated";
        }
        else{
            $this->data['title'] = "All Customers";
        }
        $this->data['existing_customers'] = $this->customers_model->get();
        $this->data['subview'] = 'admin/customers_page';
        $this->load->view('admin/_layout_main',  $this->data);
    }    
    
    public function create($uniqueid=NULL){
        $this->data['uniqueid']=$uniqueid;
        //IF UNIQUE ID IS NULL, INIT NEW USER
        if($uniqueid==NULL){
            $this->data['user'] = $this->customers_model->get_new();    
        }
        else{
            $this->data['user'] = $this->customers_model->get_by(['uniqueid'=>$uniqueid],TRUE);        
        }
        //GET VALIDATION RULES
        $rules = $this->customers_model->rules;

        $id=NULL;
        if($uniqueid!=NULL){
            $id = $this->data['user']->id;
            unset($rules['password']);
        }
        //SET VALIDATION RULES
        $this->form_validation->set_rules($rules);
        //FIRE RULES
        if($this->form_validation->run()==TRUE){
            //RULES PASSED
            $data = $this->input->post();
            
            //CHECK IF EMAIL EXISTS
            
            $emailExists = $this->customers_model->checkEmail($data['email_address'],$id);
            if($emailExists==TRUE){
                $this->data['error_message'] = getAlertMessage('Email <mark>'.$data['email_address'].'</mark> already exists on the Database');
            }
            else{
                $data['password']=  $this->customers_model->hash($data['phone_number']);    
                if($uniqueid==NULL){
                    $data['uniqueid']=  $this->customers_model->generate_unique_id($len = 12, 'uniqueid');
                }
                $saved = $this->customers_model->save($data,$id);
                if($saved){
                    $message = $uniqueid==NULL?'New Customer created Successfully':'Customer Updated Successfully';
                    $this->session->set_flashdata('success',$message);
                    if($uniqueid==NULL){
                    //LOG AUDIT ACTION
                        $action_details = "A new user with username : ".$data['email_address']." was created by user:".$this->currentUsername." on ".date('Y-m-d h:i:s');
                        $this->Audit_model->logAction("USER CREATION",$action_details,$institution_id=NULL);
                    }
                    else{
                        $action_details = "A new Customer with username : ".$data['email_address']." was updated  by user:".$this->currentUsername." on ".date('Y-m-d h:i:s');
                        $this->Audit_model->logAction("USER UPDATE",$action_details,$institution_id=NULL);                    
                    }
                    redirect('admin/customers');
                }
                else{
                    $this->data['error_message'] = getAlertMessage('No Vex o, An Error occurred while creating User, You should try again');
                }
            }
        }
        else{
            $this->data['error_message'] = trim(validation_errors())!=FALSE?getAlertMessage(validation_errors()):'';
        }
        
        $this->data['subview'] = 'admin/create_customer_page';
        $this->load->view('admin/_layout_main',  $this->data);
    }
    
    public function resetPassword($uniqueid=NULL){
        $this->data['uniqueid']=$uniqueid;
        //IF UNIQUE ID IS NULL, INIT NEW USER
        if($uniqueid==NULL){
            redirect('admin/customers');
        }
        
        $this->data['user'] = $user = $this->customers_model->get_by(['uniqueid'=>$uniqueid],TRUE);    
        
        if(!count($user)){
            redirect('admin/customers');
        }
        
        $id = $user->id;
        //GET VALIDATION RULES
        
        

        //SET VALIDATION RULES
        $this->form_validation->set_rules('password','Password','trim|required');
        $this->form_validation->set_rules('confirm_password','Confirm Password','trim|required|matches[password]');
        //FIRE RULES
        if($this->form_validation->run()==TRUE){
            $data = $this->input->post();
            unset($data['confirm_password']);            
            $data['password']=  $this->customers_model->hash($data['password']);    
            $saved = $this->customers_model->save($data,$id);
                if($saved){                    
                    $this->session->set_flashdata('success','Password Updated Successfully');
                    redirect('admin/customers');
                }
                else{
                    $this->data['error_message'] = getAlertMessage('No Vex o, An Error occurred while creating User, You should try again');
                }
        }
        else{
            $this->data['error_message'] = trim(validation_errors())!=FALSE?getAlertMessage(validation_errors()):'';
        }
        
        $this->data['subview'] = 'admin/customers/reset_password_page';
        $this->load->view('admin/_layout_main',  $this->data);
    }
    
    public function delete($uniqueid){
        $where = array('uniqueid'=>$uniqueid);
        $userData = $this->customers_model->get_by($where,TRUE);
        
        if(count($userData)){
            $deleted = $this->customers_model->delete($userData->id);
            if($deleted){
                //PERFORM A TABULAR RAZA...GOOD IMPLEMENTATION BUT BAD FOR NIGERIAN USERS
                    //$this->Admin_model->tabularRaza($userid);
                //LOG AUDIT
                $action_details = "A new user with username : ".$userData->email_address." was deleted by user:".$this->currentUsername." on ".date('Y-m-d h:i:s');
                $this->Audit_model->logAction("USER DELETION",$action_details,$institution_id=NULL);
                //REDIRECT 
                $this->session->set_flashdata('success','User deleted Successfully');
                redirect('admin/customers');
            }
            else{
                //THE VILLAGE PEOPLE DIDNOT ALLOW IT TO DELETE
                //REDIRECT 
                $this->session->set_flashdata('error','An Error Occurred while trying to delete, You shoud try again');
                redirect('admin/customers');
            }
        }
        else{
            $this->session->set_flashdata('error','No Records Found');
            redirect('admin/customers');
        }
    }
    
    public function activate($id,$deactivate=NULL){        
        if(isset($id)){
            //DEACTIVE CUSTOMER IN ON CLICK
            if($deactivate != NULL){
                $data = ['status'=>'0'];
                $saved = $this->customers_model->save($data,$id);
                $message = 'Customer Deactivated Successfully';
            }
            else{
                //GET DETAILS
                $customer = $this->customers_model->get($id,true);
                if(count($customer)){
                    $data = ['status'=>'1'];
                    $message = 'Customer Activated Successfully';
                    $saved = $this->customers_model->save($data,$id);
                //SEND EMAIL
                    $content = "<div style='font-size:20px'> Congratulations  ".$customer->fullname.", <br/>"
                            . "Your account has been activated Successfully <br/>"
                            . "Click the button below to Login to your Account.  <br/>"
                            . "<a href='".site_url('customer/auth/login/')."' target='_blank'> <button style='padding:1em;color:white; border-radius: 50px 50px 50px 50px; background:green;font-size:20px;'> Login to your Account </button></a> </div>";                
                    
                    $emailSent = $this->sendEmail($customer->fullname, $customer->email_address, $content, $subject = "YOUR ACCOUNT HAS BEEN ACTIVATED");                                    
                }
            }
            
            if($saved){
                $this->session->set_flashdata('success',$message);
                redirect('admin/customers');
            }
        }
        else{
            redirect('admin/customers');
        }
    }
}
