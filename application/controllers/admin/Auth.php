<?php
/*Code is Poetry, write to Entertain the next programmer
Auth Controller handles Authentication for the Admin Users
 */
class Auth extends Admin_controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Admin_model');
    }
    
    public function login() {
        //GET VALIDATION RULES FROM ADMIN MODEL
        $rules = $this->Admin_model->login_rules;
        //SET THE RULES
        $this->form_validation->set_rules($rules);
        //CHECK IF THE RULES ARE MET
        if($this->form_validation->run()==TRUE){
            //COLLECT DATA FROM LOGIN FORM
            $data = $this->input->post();
            
            //AUTHENTICATE THE USER
            $status = $this->Admin_model->performLogin($data['email_address'],$data['password']);
            
            if($status===false){               
                $this->data['error_message'] = getAlertMessage('Invalid Username/Password <br/> Please try again or Register if You dont have an Account yet');
            }
            else if($status==="disabled"){
                $this->data['error_message'] = getAlertMessage('Your Account has been Disabled. Please contact the Admin/Support Line for more information');
            }
            else if($status===true){
                //LOG AUDIT ACTION
                $action_details = "A user with username : ".$data['email_address']." logged in Successfully at ".date('Y-m-d h:i:s');
                $this->Audit_model->logAction("LOGIN",$action_details);
                redirect('admin/dashboard');
            }        
        }
        //TRIGGER AN ERROR IF VALIDATION ERRORS EXIST
        else {
            $this->data['error_message'] = trim(validation_errors())!=FALSE?getAlertMessage(validation_errors()):'';
        }
        //LOAD LOGIN VIEW
        $this->load->view('admin/login_page',  $this->data);
    }
    
    public function logout(){
        //DESTROY ALL ACTIVE SESSION DATA
        $this->session->unset_userdata('userLoggedin');
        $this->session->unset_userdata('activeUserdata');
        //LOG AUDIT ACTION
//        $action_details = "A user with username : ".$data['email_address']." logged out Successfully at ".date('Y-m-d h:i:s');
//        $this->Audit_model->logAction("LOGOUT",$action_details);
        //REDIRECT TO LOGIN
        redirect('admin/auth/login');
    }
}
