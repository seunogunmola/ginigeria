<?php

class Debit extends Admin_controller{
    //You know what a Constructor Function does dont You?
    //If You dont know, kindly go and return your Salary
    public function __construct() {
        parent::__construct();
    }
    
    public function index(){
        $rules = $this->debit_model->rules;
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run() == true){
            $data = $this->input->post();
            $data['performedBy'] = $this->currentUsername;
            
            $saved = $this->debit_model->save($data);
            
            if($saved){
                //UPDATE LEDGER
                $this->balance_model->updateLedger($data['customerid'],$data['amount'],'DEBIT');
                
                $this->session->set_flashdata('success','Transaction Saved Successfully');
                redirect('admin/debit');
            }
            else{
                $this->session->set_flashdata('error','An error occurred');
                redirect('admin/debit');                
            }
            
        }
        $this->data['existing_customers'] = $this->customers_model->get();
        $this->data['subview'] = 'admin/operations/debit_page';
        $this->load->view('admin/_layout_main',  $this->data);
    }    
    
    public function history(){
        $this->db->select('t_customers.fullname,t_debits.*');
        $this->db->join('t_customers','t_customers.uniqueid = t_debits.customerid','inner');
        $this->data['transactions'] = $this->debit_model->get();
        $this->data['subview'] = 'admin/operations/debit_history_page';
        $this->load->view('admin/_layout_main',  $this->data);
    }    
    
    public function receipt($transactionid){
        if(!isset($transactionid)){
            redirect(site_url);
        }
        else{       
            $this->db->select('t_customers.fullname,t_debits.*');
            $this->db->join('t_customers','t_customers.uniqueid = t_debits.customerid','inner');
            $this->data['transaction'] = $this->debit_model->get_by(['MD5(t_debits.id)'=>$transactionid],true);
            $this->data['transaction_type'] = "DEBIT";
            $this->data['subview'] = 'admin/operations/receipt_page';
            $this->load->view('admin/_layout_main',  $this->data);
        }
    }    
    
}
