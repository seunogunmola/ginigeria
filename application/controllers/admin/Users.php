<?php
/*Make the world a better place
 * Write Code the developer coming after You and the ones coming after them
 */
//THIS CONTROLLER MANAGES ADMIN USERS
class Users extends Admin_controller{
    //You know what a Constructor Function does dont You?
    //If You dont know, kindly go and return your Salary
    public function __construct() {
        parent::__construct();
        $this->load->model('Institutions_model');
        $this->load->model('Faculties_model');
    }
    
    public function create($uniqueid=NULL){
        $this->data['uniqueid']=$uniqueid;
        //IF UNIQUE ID IS NULL, INIT NEW USER
        if($uniqueid==NULL){
            $this->data['user'] = $this->Admin_model->get_new();    
        }
        else{
            $this->data['user'] = $this->Admin_model->get_by(['MD5(id)'=>$uniqueid],TRUE);        
        }
        //GET VALIDATION RULES
        $rules = $this->Admin_model->userCreation_rules;

        $id=NULL;
        if($uniqueid!=NULL){
            $id = $this->data['user']->id;
            unset($rules['password']);
        }
        //SET VALIDATION RULES
        $this->form_validation->set_rules($rules);
        //FIRE RULES
        if($this->form_validation->run()==TRUE){
            //RULES PASSED
            $data = $this->input->post();
            //CHECK IF EMAIL EXISTS
            $emailExists = $this->Admin_model->checkEmail($data['email_address'],$id);
            if($emailExists==TRUE){
                $this->data['error_message'] = getAlertMessage('Email <mark>'.$data['email_address'].'</mark> already exists on the Database');
            }
            else{
                $data['password']=  $this->Admin_model->hash($data['password']);    
                if($uniqueid==NULL){
                    $data['uniqueid']=  $this->Admin_model->generate_unique_id($len = 12, 'uniqueid');
                }
                $saved = $this->Admin_model->save($data,$id);
                if($saved){
                    $message = $uniqueid==NULL?'New User created Successfully':'User Updated Successfully';
                    $this->session->set_flashdata('success',$message);
                    if($uniqueid==NULL){
                    //LOG AUDIT ACTION
                        $action_details = "A new user with username : ".$data['email_address']." was created by user:".$this->currentUsername." on ".date('Y-m-d h:i:s');
                        $this->Audit_model->logAction("USER CREATION",$action_details,$institution_id=NULL);
                    }
                    else{
                        $action_details = "A new user with username : ".$data['email_address']." was updated  by user:".$this->currentUsername." on ".date('Y-m-d h:i:s');
                        $this->Audit_model->logAction("USER UPDATE",$action_details,$institution_id=NULL);                    
                    }
                    redirect('admin/users/create');
                }
                else{
                    $this->data['error_message'] = getAlertMessage('No Vex o, An Error occurred while creating User, You should try again');
                }
            }
        }
        else{
            $this->data['error_message'] = trim(validation_errors())!=FALSE?getAlertMessage(validation_errors()):'';
        }
        
        //GET EXISTING INSTITUTIONS
        $this->data['institutions'] = $this->Institutions_model->get();
        //GET EXISTING FACULTIES
        $this->data['faculties'] = $this->Faculties_model->get();
        //GET EXISTING USERS
        $this->data['existing_users'] = $this->Admin_model->get();
        $this->data['subview'] = 'admin/users_page';
        $this->load->view('admin/_layout_main',  $this->data);
    }
    
    public function delete($hashedId){
        $where = array('MD5(id)'=>$hashedId);
        $userData = $this->Admin_model->get_by($where,TRUE);
        if(count($userData)){
            $deleted = $this->Admin_model->delete($userData->id);
            if($deleted){
                //PERFORM A TABULAR RAZA...GOOD IMPLEMENTATION BUT BAD FOR NIGERIAN USERS
                    //$this->Admin_model->tabularRaza($userid);
                //LOG AUDIT
                $action_details = "A new user with username : ".$data['email_address']." was created by user:".$this->currentUsername." on ".date('Y-m-d h:i:s');
                $this->Audit_model->logAction("USER CREATION",$action_details,$institution_id=NULL);
                //REDIRECT 
                $this->session->set_flashdata('success','User deleted Successfully');
                redirect('admin/users/create');
            }
            else{
                //THE VILLAGE PEOPLE DIDNOT ALLOW IT TO DELETE
                //REDIRECT 
                $this->session->set_flashdata('error','An Error Occurred while trying to delete, You shoud try again');
                redirect('admin/users/create');
            }
        }
        else{
            $this->session->set_flashdata('error','No Records Found');
            redirect('admin/users/create');
        }
    }
}
