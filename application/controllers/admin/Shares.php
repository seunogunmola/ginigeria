<?php
class Shares extends Admin_controller{
    //You know what a Constructor Function does dont You?
    //If You dont know, kindly go and return your Salary
    public function __construct() {
        parent::__construct();
    }
    
    public function index(){
        $rules = $this->credit_model->rules;
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run() == true){
            $data = $this->input->post();
            $data['performedBy'] = $this->currentUsername;
            $data['status'] = 0;
            $saved = $this->shares_model->save($data);
            
            if($saved){             
                $this->session->set_flashdata('success','Transaction Posted Successfully');
                redirect('admin/shares');
            }
            else{
                $this->session->set_flashdata('error','An error occurred');
                redirect('admin/shares');                
            }
            
        }
        $this->data['existing_customers'] = $this->customers_model->get_by(['status'=>'1']);
        $this->data['subview'] = 'admin/operations/shares_page';
        $this->load->view('admin/_layout_main',  $this->data);
    }    
    
    public function pending(){
        $this->db->select('t_customers.fullname,t_shares.*');
        $this->db->join('t_customers','t_customers.uniqueid = t_shares.customerid','inner');
        $this->db->where('t_shares.status = 0');
        $this->data['transactions'] = $this->shares_model->get();
        $this->data['subview'] = 'admin/operations/pending_shares_page';
        $this->load->view('admin/_layout_main',  $this->data);
    }    
    
    public function history(){
        $this->db->select('t_customers.fullname,t_shares.*');
        $this->db->join('t_customers','t_customers.uniqueid = t_shares.customerid','inner');
        $this->data['transactions'] = $this->shares_model->get();
        $this->data['subview'] = 'admin/operations/shares_history_page';
        $this->load->view('admin/_layout_main',  $this->data);
    }    
    
    public function approve($transactionid){
        if(!isset($transactionid)){
            redirect('admin/shares/pending');
        }
        else{
            //GET TRANSACTION DATA
            $transactionData = $this->shares_model->get_by(['MD5(t_shares.id)'=>$transactionid],true);
            
            if(count($transactionData)) {
                //UPDATE LEDGER                
                $ledgerUpdated = $this->shares_balance_model->updateLedger($transactionData->customerid,$transactionData->amount,'CREDIT');
                
                if($ledgerUpdated){
                    //APPROVE TRANSACTION
                    $approved = ['status'=>'1'];
                    $saved = $this->shares_model->save($approved,$transactionData->id);
                    
                    if($saved){
                        //UPDATE AUDIT LOG
                        $this->session->set_flashdata('success','Transaction Approved Successfully');
                        redirect('admin/shares/pending');
                    }
                    else{
                        $this->session->set_flashdata('error','An error occurred. Please try again');
                        redirect('admin/shares/pending');                        
                    }
                }
                else{
                        $this->session->set_flashdata('error','An error occurred. Please try again');
                        redirect('admin/shares/pending');                      
                }
                
                $this->data['transaction_type'] = "CREDIT";
                $this->data['subview'] = 'admin/operations/receipt_page';
                $this->load->view('admin/_layout_main',  $this->data);
            }
            else{
                $this->session->set_flashdata('error','Invalid Transaction Data');
                redirect('admin/credit/pending');
            }
        }
    }     
    
    public function receipt($transactionid){
        if(!isset($transactionid)){
            redirect(site_url);
        }
        else{       
            $this->db->select('t_customers.fullname,t_shares.*');
            $this->db->join('t_customers','t_customers.uniqueid = t_shares.customerid','inner');
//            $this->db->where('t_shares.status=1');
            $this->data['transaction'] = $this->shares_model->get_by(['MD5(t_shares.id)'=>$transactionid],true);
            $this->data['transaction_type'] = "CREDIT";
            $this->data['subview'] = 'admin/operations/receipt_page';
            $this->load->view('admin/_layout_main',  $this->data);
        }
    }    
    
}
