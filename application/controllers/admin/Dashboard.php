<?php
//THIS CONTROLLER MANAGES ADMIN USER DASHBOARD
class Dashboard extends Admin_controller{
    function __construct() {
        parent::__construct();
    }
    
    public function index(){
        $this->data['customers'] = count($this->customers_model->get_by(['status'=>1]));
        $this->data['inactive_customers'] = count($this->customers_model->get_by(['status'=>0]));
               
        $this->data['credit_transactions'] = count($this->credit_model->get_by(['status'=>1]));
        $this->data['pending_credit_transactions'] = count($this->credit_model->get_by(['status'=>0]));
       
        $this->data['pending_shares'] = count($this->shares_model->get_by(['status'=>0]));
        
        $this->data['loanApplications'] = count($this->loans_model->get());
        $this->data['debit_transactions'] = count($this->debit_model->get());
        
        $this->data['sharesVolume'] = $this->getSharesVolume();
        $this->data['creditTransactionsVolume'] = $this->getCreditsVolume();
        
        $this->data['subview'] = 'admin/dashboard_page';
        $this->load->view('admin/_layout_main',  $this->data);
    }
    
    public function getSharesVolume(){
        $shares = $this->shares_balance_model->get();
        
        if(count($shares)){
            $total = 0;
            foreach($shares as $share){
                $total+=$share->balance;
            }
            return $total;
        }
        else{
            return '0';
        }
    }
    
    public function getCreditsVolume(){
        $shares = $this->balance_model->get();
        if(count($shares)){
            $total = 0;
            foreach($shares as $share){
                $total+=$share->balance;
            }
            return $total;
        }
        else{
            return '0';
        }
    }
    

}
