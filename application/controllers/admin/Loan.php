<?php

class Loan extends Admin_controller{
    //You know what a Constructor Function does dont You?
    //If You dont know, kindly go and return your Salary
    public function __construct() {
        parent::__construct();
    }
    
    public function index(){
        $this->db->select('t_customers.fullname,t_customers.phone_number,t_loan_applications.*');
        $this->db->join('t_customers','t_customers.uniqueid = t_loan_applications.customerid','inner');
        $this->data['loans'] = $loans = $this->loans_model->get();
        $this->data['subview'] = 'admin/loans/applications';
        $this->load->view('admin/_layout_main',  $this->data);
    }    
      
    
  
    
    public function process($loanid){
        if(!isset($loanid)){
            redirect(site_url);
        }
        else{       
            $this->db->select('t_customers.fullname,t_customers.phone_number,t_loan_applications.*,BALANCE_LOG.balance');
            $this->db->join('t_customers','t_customers.uniqueid = t_loan_applications.customerid','inner');
            $this->db->join('BALANCE_LOG','t_customers.uniqueid = BALANCE_LOG.customerid','inner');
            $where = ['MD5(t_loan_applications.id)'=>$loanid];
            $loanData = $this->loans_model->get_by($where,true);
            if(count($loanData)){
            
                    if($this->input->post()){
                        $data = $this->input->post();
                        $updated = $this->loans_model->save($data,$loanData->id);
                        if($updated){
                            $this->session->set_flashdata('success','Loan Status updated Successfully');
                        }
                        else{
                            $this->session->set_flashdata('error','An error occurred. Please try again');
                        }
                        redirect('admin/loan/process/'.$loanid);
                    }                
                $this->data['loanData'] = $loanData;
                $this->data['subview'] = 'admin/loans/process';
                $this->load->view('admin/_layout_main',  $this->data);
            }
            else{
                redirect('admin/loan');
            }
        }
    }    
    public function receipt($transactionid){
        if(!isset($transactionid)){
            redirect(site_url);
        }
        else{       
            $this->db->select('t_admins.fullname,t_credits.*');
            $this->db->join('t_admins','t_admins.uniqueid = t_credits.adminid','inner');
            $this->data['transaction'] = $this->credit_model->get_by(['MD5(t_credits.id)'=>$transactionid],true);
            $this->data['transaction_type'] = "CREDIT";
            $this->data['subview'] = 'admin/operations/receipt_page';
            $this->load->view('admin/_layout_main',  $this->data);
        }
    }    
    
}
