<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Web';

$route['admin'] = 'admin/auth/login';
$route['customer'] = 'customer/auth/login';
$route['school/accept/(:any)'] = "school/accept/index";
$route['school'] = "school/home";
$route['school/chatroom/(:any)'] = "school/chatroom/index/$1";
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
