<?php
defined('BASEPATH') OR exit('No direct script access allowed');
    $autoload['packages'] = array();
    $autoload['libraries'] = array('database','form_validation','session');
    $autoload['drivers'] = array();
    $autoload['helper'] = array('utilities','url','html','form','download', 'string', 'text','jwt','authorization');
    $autoload['config'] = array('_params','jwt');
    $autoload['language'] = array();
    $autoload['model'] = array();
