<?php
class Admin_model extends Base_Model{
    protected $_tablename = 't_users';
    protected $_primary_key = 'id';
    protected $primary_filter = 'intval';
    protected $_ordey_by = 't_users.fullname ASC';
    public $login_rules = array(
        'email_address' => array(
                'field' => 'email_address',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email'
        ),
        'password' => array(
                'field' => 'password',
                'label' => 'password',
                'rules' => 'trim|required'
        )
        );
    public $userCreation_rules = array(
        'email_address' => array(
                'field' => 'email_address',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email'
        ),
        'fullname' => array(
                'field' => 'fullname',
                'label' => 'Fullname',
                'rules' => 'trim|required'
        ),
        'phone_number' => array(
                'field' => 'phone_number',
                'label' => 'Phone Number',
                'rules' => 'trim|required'
        )
        );

    function __construct() {
        parent::__construct();
    }
    //INITIALISE A NEW USER
    function get_new() {
        $user = new stdClass();
        $user->fullname = '';
        $user->email_address = '';
        $user->phone_number = '';
        $user->password = '';
        $user->status = 1;
        $user->gender = '';
        $user->privileges = '';
        $user->institution_id = '';
        $user->faculty_id = '';
        return $user;
    }

    function performLogin($email_address,$password){
        //HAS PASSWORD
        $password = $this->hash($password);        
        
        //CHECK FOR USER
        $where = ['email_address'=>$email_address,'password'=>$password];
        $userData = $this->get_by($where,TRUE);
        
        if(count($userData)){
            if($userData->status==0){
                return "disabled";
            }
            else{
                $this->session->set_userdata('userLoggedin',true);
                $this->session->set_userdata('activeUserdata',$userData);
                return true;
            }
        }
        else{
            return false;
        }
    }
    
    function checkEmail($email,$id){
        $this->db->where('id !=',$id);
        $data = $this->get_by(['email_address'=>$email]);
        if(count($data)>0){
            //EMAIL EXISTS
            return true;
        }
        else{
            //EMAIL DOESNT EXIST
            return false;
        }
    }

}
