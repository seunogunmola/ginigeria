<?php
class Users_model extends Base_Model{
    protected $_tablename = 't_users';
    protected $_primary_key = 't_users.id';
    protected $primary_filter = 'intval';
    protected $_ordey_by = 't_users.surname ASC';
    public $rules = array(
        'email_address' => array(
                'field' => 'email_address',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email'
        ),
        'password' => array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required'
        ),
        'confirm_password' => array(
                'field' => 'confirm_password',
                'label' => 'Password',
                'rules' => 'trim|matches[password]'
        ),
        'surname' => array(
                'field' => 'surname',
                'label' => 'Surname',
                'rules' => 'trim|required'
        ),
        'firstname' => array(
                'field' => 'firstname',
                'label' => 'Firstname',
                'rules' => 'trim|required'
        ),
        'Othernames' => array(
                'field' => 'othernames',
                'label' => 'Othernames',
                'rules' => 'trim|required'
        ),
        'phone_number' => array(
                'field' => 'phone_number',
                'label' => 'Phone Number',
                'rules' => 'trim|required'
        ),
        'gender' => array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'trim|required'
        ),
    );

    public $login_rules = array(
        'email_address' => array(
                'field' => 'email_address',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email'
        ),
        'password' => array(
                'field' => 'password',
                'label' => 'password',
                'rules' => 'trim|required'
        )
        );    
    
    function __construct() {
        parent::__construct();
    }

    function performLogin($email_address,$password,$apiRequest=NULL){
        //HAS PASSWORD
        $password = $this->hash($password);
        //CHECK FOR USER
        $where = ['email_address'=>$email_address,'password'=>$password];
        $this->db->select('id,uniqueid,email_address,title,surname,othernames,firstname,gender,phone_number,profile,passport,privileges,status');
        $userData = $this->get_by($where,TRUE);
        
        if(count($userData)){
            if($userData->status==0){
                return "disabled";
            }
            else{
                session_unset();
                $this->session->set_userdata('userLoggedin',true);
                $this->session->set_userdata('activeUserdata',$userData);
                if($apiRequest!=NULL){
                    return $userData;
                }
                else{
                    return true;    
                }
            }
        }
        else{
            return false;
        }
    }
    
    function checkNucNumber($nucNumber){
        if(isset($nucNumber)){
            $where=array('nucnumber'=>$nucNumber);
            $data=$this->get_by($where,true);
            if(count($data)){
                return $data;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }
    
    function getPayload($id){
        $select = "t_users.id,t_users.uniqueid,email_address,username,title,fullname,gender,phone_number,dob,profile,passport,regno,nucnumber,t_users.institution_id,"
                . "t_users.faculty_id,department_id,graduation_year,t_users.status,t_users.datecreated,t_users.privileges,t_departments.department_name";
        $this->db->select($select);
        $this->db->join('t_departments','t_users.department_id = t_departments.id','left');
        $payload = $this->get($id,true);
        if(count($payload)){
            return $payload;
        }
        else{
            $payload = new stdClass();
            return $payload;
        }
    }

    function get_new() {
        $user = new stdClass();
        $user->fullname = '';
        $user->email_address = '';
        $user->phone_number = '';
        $user->password = '';
        $user->status = 1;
        $user->gender = '';
        $user->privileges = '';
        $user->institution_id = '';
        $user->faculty_id = '';
        return $user;
    }
    
}
