<?php
class Shares_balance_model extends Base_Model{
    protected $_tablename = 'SHARES_LOG';
    protected $_primary_key = 'id';
    protected $primary_filter = 'intval';
    protected $_ordey_by = 'SHARES_LOG.id ASC';


    function __construct() {
        parent::__construct();
    }
    
    public function updateLedger($customerid,$amount,$transaction_type){
        $customerData = $this->get_by(['customerid'=>$customerid],true);

        if(count($customerData)){
            if($transaction_type==="CREDIT"){
                $data['balance'] = $amount + $customerData->balance;
                $saved = $this->save($data,$customerData->id);
            }
            else{
                $data['balance'] = $customerData->balance-$amount;
                $saved = $this->save($data,$customerData->id);
            }
            if($saved){
                return true;
            }
            else{
                return false;
            }            
        }
        else{
            if($transaction_type==="CREDIT"){
                $data['customerid'] = $customerid;
                $data['balance'] = $amount;
            }
            else{
               $data['customerid'] = $customerid;
                $data['balance'] = -$amount;                
            }
            $saved = $this->save($data);
            if($saved){
                return true;
            }
            else{
                return false;
            }
            
        }
    }


}
