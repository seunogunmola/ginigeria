<?php
class Debit_model extends Base_Model{
    protected $_tablename = 't_debits';
    protected $_primary_key = 'id';
    protected $primary_filter = 'intval';
    protected $_ordey_by = 't_debits.transaction_date DESC';
    public $rules = array(
        'customerid' => array(
                'field' => 'customerid',
                'label' => 'Customer Name',
                'rules' => 'trim|required'
        ),
        'amount' => array(
                'field' => 'amount',
                'label' => 'Amount',
                'rules' => 'trim|required'
        ),
        'transaction_date' => array(
                'field' => 'transaction_date',
                'label' => 'Transaction Date',
                'rules' => 'trim|required'
        ),
        'channel' => array(
                'field' => 'channel',
                'label' => 'Transaction Channel',
                'rules' => 'trim|required'
        )
        );


    function __construct() {
        parent::__construct();
    }


}
