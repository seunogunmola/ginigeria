<?php
class Customers_model extends Base_Model{
    protected $_tablename = 't_customers';
    protected $_primary_key = 't_customers.id';
    protected $primary_filter = 'intval';
    protected $_ordey_by = 't_customers.fullname ASC';
    public $rules = array(
        'email_address' => array(
                'field' => 'email_address',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email'
        ),
        'title' => array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'trim|required'
        ),
        'fullname' => array(
                'field' => 'fullname',
                'label' => 'Fullname',
                'rules' => 'trim|required'
        ),
        'phone_number' => array(
                'field' => 'phone_number',
                'label' => 'Phone Number',
                'rules' => 'trim|required'
        ),
        'gender' => array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'trim|required'
        ),
        'dob' => array(
                'field' => 'dob',
                'label' => 'Date of Birth',
                'rules' => 'trim|required'
        ),
        'bvn' => array(
                'field' => 'bvn',
                'label' => 'BVN',
                'rules' => 'trim|required|numeric'
        ),
        'bankName' => array(
                'field' => 'bankName',
                'label' => 'Bank Name',
                'rules' => 'trim|required'
        ),
        'accountName' => array(
                'field' => 'accountName',
                'label' => 'Account Name',
                'rules' => 'trim|required'
        ),
        'accountNumber' => array(
                'field' => 'accountNumber',
                'label' => 'Account Number',
                'rules' => 'trim|required'
        ),
        'nok_name' => array(
                'field' => 'nok_name',
                'label' => 'Next of Kin Name',
                'rules' => 'trim|required'
        ),
        'nok_phone' => array(
                'field' => 'nok_phone',
                'label' => 'Next of Kin Phone',
                'rules' => 'trim|required'
        ),
        'nok_address' => array(
                'field' => 'nok_address',
                'label' => 'Next of Kin Address',
                'rules' => 'trim|required'
        ),
        'nok_relationship' => array(
                'field' => 'nok_relationship',
                'label' => 'Next of Kin Relationship',
                'rules' => 'trim|required'
        ),
        'status' => array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim|required|numeric'
        ),
    );
    
    public $login_rules = array(
        'email_address' => array(
                'field' => 'email_address',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email'
        ),
        'password' => array(
                'field' => 'password',
                'label' => 'password',
                'rules' => 'trim|required'
        )
        );    

    function __construct() {
        parent::__construct();
    }

    function performLogin($email_address,$password,$apiRequest=NULL){
        //HAS PASSWORD
        $password = $this->hash($password);
        //CHECK FOR USER
        $where = ['email_address'=>$email_address,'password'=>$password];
        $this->db->select('*');
        $userData = $this->get_by($where,TRUE);
        
        if(count($userData)){
            if($userData->status==0){
                return "disabled";
            }
            else{
                session_unset();
                $this->session->set_userdata('customerLoggedIn',true);
                $this->session->set_userdata('activeUserdata',$userData);
                if($apiRequest!=NULL){
                    return $userData;
                }
                else{
                    return true;    
                }
            }
        }
        else{
            return false;
        }
    }

    

    function get_new() {
        $user = new stdClass();
        $user->title = '';
        $user->fullname = '';
        $user->dob = '';
        $user->bvn = '';
        $user->email_address = '';
        $user->phone_number = '';
        $user->password = '';
        $user->status = 1;
        $user->gender = '';
        $user->bankName = '';
        $user->accountName = '';
        $user->accountNumber = '';
        $user->nok_name = '';
        $user->nok_phone = '';
        $user->nok_address = '';
        $user->nok_relationship = '';
        $user->status = '';
        return $user;
    }
  
    public function getNetworth($uniqueid){
        $sharesBalance = $savingsBalance = 0;
        $this->load->model('balance_model','shares_balance_model');
        //GET SAVINGS WORTH
        $savingsData = $this->balance_model->get_by(['customerid'=>$uniqueid],true);        
        if(count($savingsData)){
            $savingsBalance = $savingsData->balance;
        }
        
        //GET SHARES WORTH
        $sharesData = $this->shares_balance_model->get_by(['customerid'=>$uniqueid],true);
        if(count($sharesData)){
            $sharesBalance = $sharesData->balance;
        }        
        
        return $sharesBalance + $savingsBalance;
    }
}
