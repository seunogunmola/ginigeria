<?php
class Password_model extends Base_Model{
    protected $_tablename = 't_password_change_requests';
    protected $_primary_key = 'id';
    protected $primary_filter = 'intval';
    protected $_ordey_by = 't_password_change_requests.id ASC';


    function __construct() {
        parent::__construct();
    }

}
