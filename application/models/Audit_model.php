<?php
class Audit_model extends Base_Model{
    protected $_tablename = 't_audit_log';
    protected $_primary_key = 'id';
    protected $primary_filter = 'intval';
    protected $_ordey_by = 't_audit_log.id ASC';


    public function __construct() {
        parent::__construct();
    }
    
    public function logAction($action_type,$action_details,$institution_id=NULL){
        $data = ['action_type'=>$action_type,'action_details'=>$action_details,'institution_id'=>$institution_id];
        $this->save($data);
    }    
    
}
