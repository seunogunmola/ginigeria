<?php
class Loans_model extends Base_Model{
    protected $_tablename = 't_loan_applications';
    protected $_primary_key = 'id';
    protected $primary_filter = 'intval';
    protected $_ordey_by = 't_loan_applications.id DESC';
    public $rules = array(
        'loanAmount' => array(
                'field' => 'loanAmount',
                'label' => 'Loan Amount',
                'rules' => 'trim|required'
        ),
        'duration' => array(
                'field' => 'duration',
                'label' => 'Duration',
                'rules' => 'trim|required'
        )
        );

        public $applicationRules = array(
        'loanAmount' => array(
                'field' => 'loanAmount',
                'label' => 'Loan Amount',
                'rules' => 'trim|required|greater_than[0]'
        ),
        'duration' => array(
                'field' => 'duration',
                'label' => 'Duration',
                'rules' => 'trim|required'
        ),
        'guarantors[]' => array(
                'field' => 'guarantors[]',
                'label' => 'Guarantors',
                'rules' => 'trim'
        ),
            
        );

    function __construct() {
        parent::__construct();
    }


}
