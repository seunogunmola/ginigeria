<?php
class Base_Controller extends CI_Controller {

    public $data = array();
    function __construct() {
        parent::__construct();
        //PREVENT BROWSER CACHING
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        #GET APP DATA
        $this->data['errors']= '';
        $this->data['sitename']= config_item('sitename');
        $this->data['sitelogo']= base_url(config_item('sitelogo'));
        $this->data['description']= config_item('description');
        $this->data['siteslug']= config_item('siteslug');
        $this->data['currency']= config_item('currency');
        $this->now = date('Y-m-d H:i:s');
        $this->data['error_message'] = '';
        
        $this->load->model('Customers_model');
        $this->load->model('Users_model');
        $this->load->model('Audit_model');
        #RUN ANY NEW MIGRATIONS

        $this->load->library('migration');
//        $migration_run = $this->migration->current();
        
        if ($this->migration->current() === FALSE){
            show_error($this->migration->error_string());
        } 
        $this->load->model('Audit_model');
    }

    public function analyzeUrl(){
        $url = str_replace('http://', '', $this->data['current_url']);
        
        $this->db->where('url',$this->data['current_url']);
        $urlData = $this->db->get('t_institutions')->row();

        if(count($urlData)){
            return $urlData;
        }
        
    }
    


   //SINCE MORE THAN ONE METHOD IS MAKING USE OF THE SAME SCRIPTS IT IS GOOD PRACTICE FROM KEEP IT IN ONE PLACE
    public function getDataTableScripts() {
        #scripts for the dataTable
        $this->data['page_level_scripts'] = '<script src="' . getResource("js/advanced-datatable/js/jquery.dataTables.js") . '" ></script>';
        $this->data['page_level_scripts'] .= '<script src="' . getResource("js/data-tables/DT_bootstrap.js") . '" ></script>';
        $this->data['page_level_scripts'] .= '<script src="' . getResource("js/dynamic_table_init.js") . '" ></script>';
    }

        //THIS IS BEING USED BY MORE THAN ONE CHILD CLASS
    protected function _getAccountBalance($studentid){
        $account_balance = 0;
        $this->db->select('account_balance');
        $this->db->where('account_number',$studentid);
        $account_balance_data = $this->db->get('t_ledger')->row();
        if(count($account_balance_data)){
            $account_balance = $account_balance_data->account_balance;
        }
        return $account_balance;
    }


    public function sendEmail($fullname, $email, $details, $subject = NULL) {
        require(APPPATH . "/third_party/sendgrid/sendgrid-php.php");
        $from = new SendGrid\Email(config_item('sitename'), config_item('official_email'));
        $to = new SendGrid\Email($fullname, $email);
        $content = new SendGrid\Content("text/html", $details);
        $mail = new SendGrid\Mail($from, $subject, $to, $content, config_item('official_email'));
        // $mail->setTemplateId('d-52e5f3e875be4596b2aefe302c639abd');
        $mail->setTemplateId('12365a09-1f01-4b19-8ae2-8cac06a34633');
        $apiKey = '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';
        $sg = new \SendGrid($apiKey);
        $response = $sg->client->mail()->send()->post($mail);
        return $response->statusCode();
    }
    

    public function do_upload($sourceFileName,$location,$allowedTypes,$allowedSize)
    {
        $response=[];
        $config['upload_path']     = $location;
        $config['allowed_types']   = $allowedTypes;
        $config['allowed_size']    = $allowedSize;
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);

        if(!is_dir($location)){
            mkdir($location);
        }

        if ( ! $this->upload->do_upload($sourceFileName))
        {
                $error = array('error' => $this->upload->display_errors());
                $response['status']=false;
                $response['data']=$error;
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                $response['status']=true;
                $response['data']=$data;
        }
        return $response;
    }


}
