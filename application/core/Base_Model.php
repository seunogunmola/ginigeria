<?php
//THE BASE MODEL HOUSING ALL THE NEEDED CORE METHODS.
class Base_Model extends CI_Model {

    protected $_tablename = '';
    protected $_primary_key = '';
    protected $primary_filter = 'intval';
    protected $_ordey_by = '';
    public $rules = '';
    protected $_timestamps = '';
    public $_old_data_model = NULL;
    public $_old_data_stock_field = NULL;
    public $_old_data_key = NULL;

    function __construct() {
        parent::__construct();
    }

    function get($id = NULL, $single = FALSE) {
        #LITTLE HACK HERE
        #IF A STORE ID EXISTS,FILTER EVERY QUERY WITH THE STOREID
        //IF ID IS NOT NULL THAT MEANS WE ARE RETURNING A SINGLE ROW
        if ($id != NULL) {
            $filter = $this->primary_filter;
            $this->db->where($this->_primary_key, $id);
            $id = $filter != "" ? $filter($id) : "";
            $method = 'row';
        }
        // NO ID IS SUPPLIED BUT WE STILL WANT A SINGLE ROW RETURNED
        elseif ($single == TRUE) {
            $method = 'row';
        }
        //NO ID IS SUPPLIED SHOW ME ALL THE ROWS
        else {
            $method = 'result';
        }

        $this->db->order_by($this->_ordey_by);
        //IF STORES ARE INVOLVED, ALWAYS QUERY WITH STOREID

        return $this->db->get($this->_tablename)->$method();
    }

    function get_by($where, $single = FALSE) {
        $this->db->where($where);
        return $this->get(NULL, $single);
    }

    function save($data, $id = NULL) {
        if ($this->_timestamps == TRUE) {
            $now = date('Y-m-d H:i:s');
            $id || $data['datecreated'] = $now;
            $data['datemodified'] = $now;
        }
        //INSERT
        if ($id === NULL) {
            !isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
            $this->db->set($data);
            $this->db->insert($this->_tablename);
            $id = $this->db->insert_id();
        }
        //UPDATE
        else {
            $filter = $this->primary_filter;
            $id = $filter($id);
            $this->db->set($data);
            $this->db->where($this->_primary_key, $id);
            $this->db->update($this->_tablename);
        }
        return $id;
    }

    function delete($id) {
        $filter = $this->primary_filter;
        $id = $filter($id);
        if (!$id) {
            return FALSE;
        } else {
            $this->db->where($this->_primary_key, $id);
            $this->db->limit(1);
            if ($this->db->delete($this->_tablename))
                return TRUE;
        }
    }
    
    function delete_where($where) {
        $this->db->where($where);
        if ($this->db->delete($this->_tablename))
            return TRUE;        
    }

    function array_from_post($fields) {
        $data = array();
        foreach ($fields as $field) :
            $data[$field] = $this->input->post($field);
        endforeach;

        return $data;
    }

    public function generate_unique_id($len = 5, $fieldname) {
        $source = ""
                . "012345678901234567890123456789"
                . "012345678901234567890123456789"
                . "012345678901234567890123456789";
        $range = strlen($source);
        $output = '';
        for ($i = 0; $i < $len; $i++) {
            $output .= substr($source, rand(0, $range - 1), 1);
        }

        while (count($this->get_by(array($fieldname => $output), true)) > 0) {
            $this->generate_unique_id($len);
        }
        return $output;
    }

    public function hash($stringToBeHashed) {
        return hash('sha1', $stringToBeHashed);
    }

    public function _checkUniqueValue($value, $rowname, $tablename, $id = NULL) {
        $value_status = '';
        if ($id != NULL) {
            $this->db->where($this->_primary_key . '!=', $id);
        }
        $value_data = $this->db->where($rowname, $value)->get($tablename);
        $value_data_array = $value_data->result();

        if (count($value_data_array) && $value_data->num_rows() > 0) {
            $value_status = false; #VALUE ALREADY EXISTS
        } else {
            $value_status = true; #VALUE DOESNT EXIST
        }
        return $value_status;
    }

    function getMerchantLedgerData($merchantid){
        $where = array('merchant_uniqueid' => $merchantid);
        $ledger_data = $this->Merchant_ledger_model->get_by($where, TRUE);
        return $ledger_data;
    }

    function checkValue($field,$data,$id=NULL){
        if($id!=NULL){
            $this->db->where('id!=',$id);
        }
        $data = $this->get_by([$field=>$data]);
        if(count($data)>0){
            return true;
        }
        else{
            return false;
        }
    }

    function _get_unique_code($table, $field, $type, $length)
    {
        $new_code = random_string($type, $length);

        $match_code = $this->db->get_where($table, array($field=>$new_code))->row();
        if(count($match_code)==1){
            return $this->_get_unique_code($table, $field, $type, $length);
        }else{
            return $new_code;
        }
    }

    
}
