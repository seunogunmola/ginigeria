<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Loans
                </h3>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">    
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                Apply for Loan
                            </div>
                            <div class="panel-body">                                
                                <div class="row">
                                    <h4 style="color:crimson;border: 1px dashed red; padding:1em;">Note : Before You Apply for a Loan, Kindly use the <a href="<?= site_url('customer/loan');?>"> Loan Calculator </a> to know how much You will be repaying and the capacity of guarantors You will need.</h4>
                                    <?php
                                        $total_balance = $savings_balance + $shares_balance;
                                    ?>
                                    <h2 class="alert alert-info"> Your Total Balance is <?= $currency. number_format($total_balance,2) ?>  </h2>
                                    <h4 class="alert alert-danger">
                                        You are eligible to apply for between  <?= $currency. number_format($total_balance * 2,2) ?> and <?= $currency. number_format($total_balance * 3,2) ?> 
                                    </h4>
                                    <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'info') : '' ?>
                                    <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                    <form action='<?=  site_url('customer/loan/apply')?>' class="form-horizontal" method="post">
                                    <div class="col-lg-8">
                                        <h4>Apply for Loan Below</h4>
                                        <hr/>  
                                        <div class="form-group">
                                            <label class="col-md-3">Loan Amount</label>
                                            <div class="col-md-9">
                                                <input class="form-control" value="<?=set_value('loanAmount',$loanAmount)?>"  name = "loanAmount" type = "number" placeholder="Enter Amount of Transaction" value = "<?php echo set_value('loanAmount') ?>" required = "">    
                                            </div>
                                        </div>                                                                               
                                        <div class="form-group">
                                            <label class="col-md-3">Loan Duration(in Months)</label>
                                            <div class="col-md-9">
                                                <?php
                                                        $durations = array(''=>'Select Duration');
                                                        for($i=1; $i<=36; $i++):
                                                            $durations[$i] = $i;
                                                        endfor;  
                                                        echo form_dropdown('duration', $durations, set_value('duration',$loanDuration), 'class="form-control" required' );
                                                ?>
                                            </div>
                                        </div>  
                                       
                                        <div class="pull-right">
                                            <button class="btn btn-primary" type="submit"> <i class="fa fa-save"></i> Apply for Loan</button>
                                        </div>
                                </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    
            </div>
        </div>
        <!--body wrapper end-->

