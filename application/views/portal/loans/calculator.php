
<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Loans
                </h3>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">    
                    <div class="col-md-4">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                Loan Calculator <i class="fa fa-calculator"></i>
                            </div>
                            <div class="panel-body">                                
                                <div class="row">
                                    <?php
                                        $total_balance = $savings_balance;
                                    ?>
                                    <h2 class="" style="color:crimson;border: 2px solid red; padding:0.5em;text-align:center;"> Your Total Balance is <?= $currency. number_format($total_balance,2) ?>  </h2>
                                    <h4 style="color:crimson;border: 1px solid black; padding:0.5em;">
                                        <?php
                                            if($total_balance == 0){
                                                echo "You are not eligible to apply for any loan";
                                            }
                                            else {
                                        ?>
                                        You are eligible to apply for between  <br/> <?= $currency. number_format($total_balance * 2,2) ?> (at 1% interest) and <?= $currency. number_format($total_balance * 3,2) ?> (at 2% interest)  
                                        <?php                                        
                                            }
                                        ?>
                                    </h4>
                                    <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'info') : '' ?>
                                    <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                    <form action='<?=  site_url('customer/loan')?>' class="form-horizontal" method="post">
                                    <div class="col-lg-12">
                                        <h4>Calculate your repayment here</h4>
                                        <hr/>  
                                        <div class="form-group">
                                            <label class="col-md-3">Loan Amount</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "loanAmount" type = "number" placeholder="Enter Amount of Transaction" value = "<?php echo set_value('loanAmount') ?>" required = "">    
                                            </div>
                                        </div>                                                                               
                                        <div class="form-group">
                                            <label class="col-md-3">Loan Duration(in Months)</label>
                                            <div class="col-md-9">
                                                <?php
                                                        $durations = array(''=>'Select Duration');
                                                        for($i=1; $i<=12; $i++):
                                                            $durations[$i] = $i;
                                                        endfor;  
                                                        echo form_dropdown('duration', $durations, set_value('duration'), 'class="form-control" required' );
                                                ?>
                                            </div>
                                        </div>  
                                       
                                        <div class="pull-right">
                                            <button class="btn btn-primary" type="submit"> <i class="fa fa-calculator"></i> Calculate</button>
                                        </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if(isset($loanAmount)) { ?>
                    <div class="col-md-8">
                        <div class="panel panel-info">
                            <button class="btn btn-primary" onclick="PrintDiv()"> Print <i class="fa fa-print"></i></button>
                            
                            <div class="panel-heading">
                                Loan Repayment Plan
                            </div>
                            <div class="panel-body">                                
                                <div id="printArea" style="border:1px solid black;padding:1em;">
                                <div class="row">
                                    <div align="center">
                                        <img src="<?=$sitelogo;?>" style="width:200px;"/>
                                    </div>
                                    <?php
                                        if($loanAmount > $totalNetworth):
                                    ?>
                                    <div style="font-size:18px;color:crimson;padding:1em;">
                                        Important Info : <br/>
                                        You need up a Maximum of 3 Guarantors, who are members of this Cooperative with a total Networth of  <b> <?=$currency. number_format($loanAmount-$totalNetworth,2);?> </b>
                                        to apply for this loan.
                                    </div> 
                                    <?php
                                        endif;
                                    ?>
                                    <table class="table table-bordered table-hover table-condensed" id="printTable">
                                                <caption>
                                                    
                                                    <h4> Loan Repayment Schedule </h4>
                                                </caption>                                        
                                        <tr>
                                            <td>Your Networth </td>
                                            <td><?=$currency. number_format($totalNetworth,2);?></td>
                                        </tr>
                                        <tr>
                                            <td>Loan Amount</td>
                                            <td><?=$currency. number_format($loanAmount,2);?></td>
                                        </tr>
                                        <tr>
                                            <td>Duration</td>
                                            <td><?=$duration;?> Months</td>
                                        </tr>
                                        <tr>
                                            <td>Interest Rate</td>
                                            <td><?=$interest_rate;?> %</td>
                                        </tr>
                                        <tr>
                                            <td>Total Interest</td>
                                            <td><?php
                                            $interest = ($interest_rate/100) * $loanAmount;
                                            echo $currency.number_format($interest,2);?></td>
                                        </tr>
                                        <tr>
                                            <td>Total Repayments</td>
                                            <td><?php
                                            $totalRepayment = $loanAmount + $interest;
                                            echo $currency.number_format($totalRepayment,2);?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="background:black;color:white;"> Repayment Schedule</td>
                                        </tr>
                                        <?php
                                            for($i=1; $i<=$duration;$i++):
                                        ?>
                                        <tr>
                                            <td>
                                                Month <?= $i;?>
                                            </td>
                                            <td>
                                                <?= $currency.number_format($totalRepayment/$duration,2);?>
                                            </td>
                                        </tr>
                                        <?php
                                            endfor;
                                        ?>                                        
                                    </table>
                                    <a href="<?= site_url('customer/loan/apply/'.$loanAmount.'/'.$duration);?>"> <button class="btn btn-primary"> Click Here to Apply for this Loan <i class="fa fa-cogs"></i></button> </a>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
            </div>
        </div>
        <!--body wrapper end-->

