<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="<?= $description; ?>">
        <meta name="author" content="<?php echo config_item('sitename') ?>">
        <title><?php echo config_item('sitename') ?> </title>
        <link href="<?php echo getResource('css/style.css') ?>" rel="stylesheet">
        <link href="<?php echo getResource('css/style-responsive.css') ?>" rel="stylesheet">
        <link rel="shortcut icon" href="#" type="image/png">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="candidate-login-body">
        <div class="container">
            <?php
            echo form_open('', 'class="form-signin" role = "form" autocomplete="off"');
            ?>
            <div class="form-signin-heading text-center">
                <img src="<?php echo $sitelogo ?>" style="max-width:300px" alt=""/>
            </div>

            <div class="login-wrap">
                <?php echo $error_message; ?>
                <?= trim($this->session->flashdata('success')!=false ? getAlertMessage($this->session->flashdata('success'),'success') : '');?>
                <?= trim($this->session->flashdata('error')!=false ? getAlertMessage($this->session->flashdata('error'),'danger') : '');?>
                <div class="form-group">
                    <label for="Email address">Email address</label>
                    <input type="email" name ="email_address" autocomplete="new-password" class="form-control" value="<?=set_value('email_address');?>" placeholder="Email address" required="" data-toggle = "tooltip"  title="Enter Your Email Here">
                </div>
                <div class="form-group">
                    <label for="Password"> Password</label>
                    <input type="password" name ="password" autocomplete="new-password" class="form-control"   placeholder="Password" required="" data-toggle = "tooltip" title="Enter Your Password Here">
                </div>
                <div class="form-group">
                    <label for="Password"> Confirm Password</label>
                    <input type="password" name ="confirm_password" class="form-control"   placeholder="Confirm Password" required="" data-toggle = "tooltip" title="Confirm Your Password">
                </div>
                <div class="form-group">
                    <label for="Surname">Surname</label>
                    <input type="text" name ="surname" class="form-control" value="<?=set_value('surname');?>"  placeholder="Surname" required="" data-toggle = "tooltip"  title="Enter Your Surname Here">
                </div>
                <div class="form-group">
                    <label for="First Name">First Name</label>
                    <input type="text" name ="firstname" class="form-control" value="<?=set_value('firstname');?>"  placeholder="First Name" required="" data-toggle = "tooltip"  title="Enter Your First Name Here">
                </div>
                <div class="form-group">
                    <label for="Othernames">Other Names</label>
                    <input type="text" name ="othernames" class="form-control" value="<?=set_value('othernames');?>"  placeholder="Other Names"  data-toggle = "tooltip"  title="Enter Your Other Names Here">
                </div>                
                <div class="form-group">
                    <label for="Gender">Gender</label>
                    <?php
                    echo GenderDropDown('gender','');
                    ?>
                </div>                
                <div class="form-group">
                    <label for="Phone Number">Phone Number</label>
                    <input type="text" maxlength="11" name ="phone_number" value="<?=set_value('phone_number');?>" class="form-control"   placeholder="Phone Number" required="" data-toggle = "tooltip"  title="Enter Your Phone Number  Here">
                </div>                              
                <button class="btn btn-lg btn-login btn-block" type="submit" title="Click Here to Login" data-toggle = "tooltip">
                    Register
                </button>
            <div class="registration">
                Already Registered?
                <a class="" href="<?= site_url('portal/auth/login');?>">
                    Login Here
                </a>
            </div>                
                <hr/>                       
            </div>            
            <?php echo form_close();?>
        </form>
        
        

    </div>



    <!-- Placed js at the end of the document so the pages load faster -->

    <!-- Placed js at the end of the document so the pages load faster -->
    <script src="<?php echo getResource('js/jquery-1.10.2.min.js') ?>"></script>
    <script src="<?php echo getResource('js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo getResource('js/modernizr.min.js') ?>"></script>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</body>
</html>
