<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Edit your Profile
                </h3>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">
                    <div class="col-md-10">
                    <?= trim($this->session->flashdata('success')) != false ? getAlertMessage($this->session->flashdata('success'),'success') : ''?>
                    <?= trim($this->session->flashdata('error')) != false ? getAlertMessage($this->session->flashdata('error'),'danger') : ''?>
                    <?= trim($error_message)!=false?$error_message:'';?>                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Enter your Accurate Details
                            </div>
                            <div class="panel-body">
                                <?php echo $error_message;?>
                                <div class="row">
                                    <?php echo $message ?>
                                    <form action='<?=  site_url('portal/profile/edit/'.$uniqueid)?>' class="form-horizontal" method="post">
                                    <div class="col-lg-12">
                                        <h4>Bio Data</h4>
                                        <hr/>
                                        <div class="form-group">
                                            <label for="email" class="col-md-3">Email Address</label>
                                            <div class="col-md-9">
                                                <input class="form-control" readonly="" name = "email_address" type = "email"  placeholder="Email" value = "<?php echo set_value('email_address', $user->email_address) ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3">Title</label>
                                            <div class="col-md-9">
                                            <?php
                                                $title = array(''=>'Select Title','Mr'=>'Mr','Mrs'=>'Mrs','Miss'=>'Miss','Dr'=>'Dr');
                                                echo form_dropdown('title', $title, set_value('title',$user->title), 'required="" class="form-control"');
                                            ?>
                                            </div>
                                        </div>                                        
                                        <div class="form-group">
                                            <label class="col-md-3">Surname</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "surname" type = "text" placeholder="Your Surname" value = "<?php echo set_value('surname', $user->surname) ?>" required = "">    
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3">Firstname</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "firstname" type = "text" placeholder="Your Firstname" value = "<?php echo set_value('firstname', $user->firstname) ?>" required = "">    
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3">Othernames</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "othername" type = "text" placeholder="Your Othername" value = "<?php echo set_value('othernames', $user->othernames) ?>" required = "">    
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3">Phone</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "phone_number" maxlength="11" type = "text" placeholder="Enter User's Phone" value = "<?php echo set_value('phone_number', $user->phone_number) ?>" required = "">    
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3">Gender</label>
                                            <div class="col-md-9">
                                            <?php
                                                $gender = array(''=>'Select Gender','Male'=>'Male','Female'=>'Female');
                                                echo form_dropdown('gender', $gender, set_value('gender',$user->gender), 'required="" class="form-control"');
                                            ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3">Date of Birth</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "dob" type="date" placeholder="DOB" max="2000-01-01" value = "<?php echo set_value('dob', $user->dob) ?>" required = "">    
                                            </div>
                                        </div>                                        
                                                                               
                                        <div class="pull-right">
                                            <button class="btn btn-primary" type="submit"> <i class="fa fa-save"></i> Update Profile</button>
                                        </div>
                                </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>
                    
            </div>
        </div>
        <!--body wrapper end-->

