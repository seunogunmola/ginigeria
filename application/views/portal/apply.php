<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Loans
                </h3>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">    
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                Apply for Loan
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'info') : '' ?>
                                    <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                    <form action='<?=  site_url('customer/loan/apply')?>' class="form-horizontal" method="post">
                                    <div class="col-lg-8">
                                        <h4>Enter Transaction Details</h4>
                                        <hr/>  
                                        <div class="form-group">
                                            <label class="col-md-3">Loan Amount</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "loanAmount" type = "number" placeholder="Enter Amount of Transaction" value = "<?php echo set_value('loanAmount') ?>" required = "">    
                                            </div>
                                        </div>                                                                               
                                        <div class="form-group">
                                            <label class="col-md-3">Loan Duration(in Months)</label>
                                            <div class="col-md-9">
                                                <?php
                                                        $durations = array(''=>'Select Duration');
                                                        for($i=1; $i<=36; $i++):
                                                            $durations[$i] = $i;
                                                        endfor;  
                                                        echo form_dropdown('duration', $durations, set_value('duration'), 'class="form-control" required' );
                                                ?>
                                            </div>
                                        </div>  
                                       
                                        <div class="pull-right">
                                            <button class="btn btn-primary" type="submit"> <i class="fa fa-save"></i> Apply for Loan</button>
                                        </div>
                                </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    
            </div>
        </div>
        <!--body wrapper end-->

