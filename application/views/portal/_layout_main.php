<?php
#LOAD HEADER
$this->load->view('portal/_templates/_head');

#LOAD MENU
$this->load->view('portal/_templates/_menu');

#LOAD SUBVIEW
$this->load->view($subview);

#LOAD FOOTER
$this->load->view('portal/_templates/_foot');