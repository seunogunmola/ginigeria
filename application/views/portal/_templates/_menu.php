            <ul class="nav nav-pills nav-stacked custom-nav">
                <li class="active"><a href="<?php echo site_url('portal/dashboard') ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                
                
                <li class="menu-list"><a href=""><i class="fa fa-user"></i> <span>Profile</span></a>
                    <ul class="sub-menu-list">
                        <li> <a href="<?php echo site_url('portal/profile/edit/'.$userdata->uniqueid)?>"> <i class="fa fa-institution"></i> Edit Profile</a></li>
                    </ul>
                </li>
                <li><a href="<?php echo site_url('portal/auth/logout')?>" onclick="return confirm('Are You Sure?')"><i class="fa fa-close"></i> <span>Logout</span></a></li>
                                                      
            </ul>
        </div>
    </div>