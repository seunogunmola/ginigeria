<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="SchoolConnect - Nigeria's foremost Academic Social Network">
        <meta name="author" content="<?php echo config_item('sitename') ?>">
        <title><?php echo config_item('sitename') ?> </title>
        <link href="<?php echo getResource('css/style.css') ?>" rel="stylesheet">
        <link href="<?php echo getResource('css/style-responsive.css') ?>" rel="stylesheet">
        <link rel="shortcut icon" href="#" type="image/png">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="login-body">
        <div class="container">
            <?php
            echo form_open('', 'class="form-signin" role = "form"');
            ?>
            <div class="form-signin-heading text-center">                
                <img src="<?php echo $sitelogo ?>" style="max-width:300px" alt=""/>
            </div>

            <div class="login-wrap">
                <h4 style="color:crimson;">Reset your Password</h4>                
                <?php echo $error_message; ?>
                <?= trim($this->session->flashdata('success') != false) ? getAlertMessage($this->session->flashdata('success'),'info') : ''?>
                <?= trim($this->session->flashdata('error') != false) ? getAlertMessage($this->session->flashdata('error')) : ''?>                
                <div class="form-group">
                    <label for="Email address"> Email address</label>
                    <input type="email" name ="email_address" value="<?=$customerData->email_address?>" readonly="" class="form-control" id="username" placeholder="Email address" required="" data-toggle = "tooltip"  title="Enter Your Email Here">
                </div>
                <div class="form-group">
                    <label for="Password"> Enter New Password</label>
                    <input type="password" name ="password" class="form-control" id="password" placeholder="Password" required="" data-toggle = "tooltip" title="Enter Your Password Here">
                </div>
                <div class="form-group">
                    <label for="Password"> Confirm New Password</label>
                    <input type="password" name ="confirm_password" class="form-control"  placeholder="Confirm Password" required="" data-toggle = "tooltip" title="Confirm your Password Here">
                </div>
                <button class="btn btn-lg btn-login btn-block" type="submit" title="Click Here to Login" data-toggle = "tooltip">
                    RESET PASSWORD
                </button>
                <hr/>
                <a class="label label-danger" style="color:white!important;" href="<?= site_url('customer/auth/login')?>"> Login Here </a>
            </div>
            <a href="<?= site_url('') ?>" class="label label-primary" style="margin:1em;color:white;"> Go Home </a>    

            <?php echo form_close(); ?>

    </div>


    <!-- Placed js at the end of the document so the pages load faster -->
    <!-- Placed js at the end of the document so the pages load faster -->
    <script src="<?php echo getResource('js/jquery-1.10.2.min.js') ?>"></script>
    <script src="<?php echo getResource('js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo getResource('js/modernizr.min.js') ?>"></script>
     
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</body>
</html>
