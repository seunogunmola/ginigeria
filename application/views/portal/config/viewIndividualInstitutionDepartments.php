<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Institution's Department Management
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="<?= site_url('admin/config/attachInstitutionDepartments'); ?>">Attach Departments to Institutions</a>
                    </li>
                    <li class="active"> <a href="<?= site_url('admin/config/viewDepartmentAssignments'); ?>""> View All Department Assignments </a></li>
                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Departments Assigned to : <?= $departmentsAssigned[0]->name;?>
                            </div>
                            <div class="panel-body">
                                <?php if(count($departmentsAssigned)){?>
                                <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-lg-12">
                                                <table class="table table-hover table-bordered table-condensed">
                                                    <caption> <h4> Departments Assigned to : <?= $departmentsAssigned[0]->name;?> </h4> </caption>
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Faculty </th>
                                                            <th>Department Name</th>
                                                        </tr>
                                                    </thead>
                                                    <?php
                                                    $sn=1;
                                                        foreach($departmentsAssigned as $value):
                                                    ?>
                                                    <tr>
                                                        <td> <?=$sn;?></td>
                                                        <td> <?=$value->faculty_name;?></td>
                                                        <td> <?=$value->department_name;?> </td>
                                                    </tr>
                                                    <?php
                                                    $sn++;
                                                        endforeach;
                                                    ?>
                                                </table>
                                            </div
                                        </div>
                                </div>
                            </div>
                                <?php
                                    }
                                    else{
                                        echo getAlertMessage("No Institutions with assigned Departments yet");
                                    }
                                ?>
                        </div>
                    </div>
                </div>
            </div>
            <!--body wrapper end-->

