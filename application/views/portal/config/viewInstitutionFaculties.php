<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Institution's Faculty Management
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="<?= site_url('admin/config/attachInstitutionFaculties'); ?>">Attach Faculties to Institutions</a>
                    </li>
                    <li class="active"> <a href="<?= site_url('admin/config/viewFacultyAssignments'); ?>""> View All Faculty Assignments </a></li>
                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Institution with Faculties Assigned
                            </div>
                            <div class="panel-body">
                                <?php if(count($facultiesAssigned)){?>
                                <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-lg-12">
                                                <table class="table table-hover table-bordered table-condensed">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Institution Name</th>
                                                            <th>No of Assigned Faculties</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <?php
                                                    $sn=1;
                                                        foreach($facultiesAssigned as $value):
                                                    ?>
                                                    <tr>
                                                        <td> <?=$sn;?></td>
                                                        <td> <?=$value->name;?></td>
                                                        <td> <?=$value->facultiesCount;?></td>
                                                        <td> 
                                                            <!-- Single button -->
                                                            <div class="btn-group">
                                                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                Action <span class="caret"></span>
                                                              </button>
                                                              <ul class="dropdown-menu">
                                                                  <li><a target="_blank" href="<?=site_url('admin/config/viewFacultyAssignments/'.md5($value->institution_id));?>">View Assigned Faculties</a></li>
                                                              </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $sn++;
                                                        endforeach;
                                                    ?>
                                                </table>
                                            </div
                                        </div>
                                </div>
                            </div>
                                <?php
                                    }
                                    else{
                                        echo getAlertMessage("No Institutions with assigned Faculties yet");
                                    }
                                ?>
                        </div>
                    </div>
                </div>
            </div>
            <!--body wrapper end-->

