<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Institution's Faculty Management
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="<?= site_url('admin/config/attachInstitutionFaculties'); ?>">Attach Faculties to Institutions</a>
                    </li>
                    <li class="active"> <a href="<?= site_url('admin/config/viewFacultyAssignments'); ?>""> View All Faculty Assignments </a></li>
                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Faculties Assigned to : <?= $facultiesAssigned[0]->name;?>
                            </div>
                            <div class="panel-body">
                                <?php if(count($facultiesAssigned)){?>
                                <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-lg-12">
                                                <table class="table table-hover table-bordered table-condensed">
                                                    <caption> <h4> Faculties Assigned to : <?= $facultiesAssigned[0]->name;?> </h4> </caption>
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Faculty Name</th>
                                                        </tr>
                                                    </thead>
                                                    <?php
                                                    $sn=1;
                                                        foreach($facultiesAssigned as $value):
                                                    ?>
                                                    <tr>
                                                        <td> <?=$sn;?></td>
                                                        <td> <?=$value->faculty_name;?></td>
                                                    </tr>
                                                    <?php
                                                    $sn++;
                                                        endforeach;
                                                    ?>
                                                </table>
                                            </div
                                        </div>
                                </div>
                            </div>
                                <?php
                                    }
                                    else{
                                        echo getAlertMessage("No Institutions with assigned Faculties yet");
                                    }
                                ?>
                        </div>
                    </div>
                </div>
            </div>
            <!--body wrapper end-->

