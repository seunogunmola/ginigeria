<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Institution's Department Management
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="<?= site_url('admin/config/attachInstitutionDepartments'); ?>">Attach Departments to Institutions</a>
                    </li>
                    <li class="active"> <a href="<?= site_url('admin/config/viewDepartmentAssignments'); ?>""> View All Department Assignments </a></li>
                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Institution with Departments Assigned
                            </div>
                            <div class="panel-body">
                                <?php if(count($departmentsAssigned)){?>
                                <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-lg-12">
                                                <table class="table table-hover table-bordered table-condensed">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Institution Name</th>
                                                            <th>No of Assigned Departments</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <?php
                                                    $sn=1;
                                                        foreach($departmentsAssigned as $value):
                                                    ?>
                                                    <tr>
                                                        <td> <?=$sn;?></td>
                                                        <td> <?=$value->name;?></td>
                                                        <td> <?=$value->departmentsCount;?></td>
                                                        <td> 
                                                            <!-- Single button -->
                                                            <div class="btn-group">
                                                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                Action <span class="caret"></span>
                                                              </button>
                                                              <ul class="dropdown-menu">
                                                                  <li><a target="_blank" href="<?=site_url('admin/config/viewDepartmentAssignments/'.md5($value->institution_id));?>">View Assigned Departments</a></li>
                                                              </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $sn++;
                                                        endforeach;
                                                    ?>
                                                </table>
                                            </div
                                        </div>
                                </div>
                            </div>
                                <?php
                                    }
                                    else{
                                        echo getAlertMessage("No Institutions with assigned Departments yet");
                                    }
                                ?>
                        </div>
                    </div>
                </div>
            </div>
            <!--body wrapper end-->

