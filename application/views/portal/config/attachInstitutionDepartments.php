<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Institution's Department Management
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="<?= site_url('admin/config/attachInstitutionDepartments'); ?>">Attach Departments to Institutions</a>
                    </li>
                    <li class="active"> <a href="<?= site_url('admin/config/viewDepartmentAssignments'); ?>""> View All Department Assignments </a></li>
                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <?php echo!empty($department->department_name) ? '<i class = "fa fa-edit"></i> Editing Department : ' . $department->department_name : '<i class = "fa fa-plus"></i> Add New Institution' ?>
                            </div>
                            <div class="panel-body">
                                <?php echo $error_message; ?>
                                <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'info') : '' ?>
                                <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                <div class="row">
                                    <?php echo $message ?>
                                    <form action='' class="form-horizontal" method="post">
                                        <div class="col-lg-12">
                                            <div class="col-lg-8">
                                                <div class="form-group">
                                                    <label class="col-md-3">Select Institution</label>
                                                    <div class="col-md-9">
                                                        <?php echo form_dropdown('institution_id',$institutions,set_value('institution_id'),'class="form-control" required="" onchange="getFaculties(this.value)"');?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3">Select Department</label>
                                                    <div class="col-md-9">
                                                        <div id="facultyPlaceholder">
                                                            <select class="form-control">
                                                                <option>Select Institution to load Faculites</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Department Name</th>
                                                            <th>Check Box to Assign</th>
                                                        </tr>
                                                    </thead>
                                                    <?php
                                                    $sn=1;
                                                        foreach($departments as $department):
                                                    ?>
                                                    <tr>
                                                        <td> <?=$sn;?></td>
                                                        <td> <?=$department->department_name;?></td>
                                                        <td>
                                                            <input type="checkbox" name="departments[]" value="<?=$department->id?>">
                                                            <span></span>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $sn++;
                                                        endforeach;
                                                    ?>
                                                </table>
                                                <div class="pull-right">
                                                    <button class="btn btn-danger" type="reset"> <i class="fa fa-remove"></i> Clear</button>
                                                    <button class="btn btn-primary" type="submit"> <i class="fa fa-save"></i> Save </button>
                                                </div>
                                            </div
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>