<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Create Session
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="<?= site_url('admin/config/createSession'); ?>">Create Session</a>
                    </li>
                    <li class="active"> <a href="<?= site_url('admin/config/activateSession'); ?>"> Set Active Session </a></li>
                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Create Session
                            </div>
                            <div class="panel-body">
                                <?php echo $error_message; ?>
                                <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'info') : '' ?>
                                <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                <div class="row">
                                    <?php echo $message ?>
                                    <form action="" class="form-horizontal" method="post">
                                        <div class="col-lg-12">
                                            <div class="col-lg-8">
                                                <div class="form-group">
                                                    <label for="sessionName">Session Name</label>
                                                    <div>
                                                        <input type="text" name="session_name" class="form-control" placeholder="e.g 2017/2018" required=""/>
                                                    </div>
                                                </div>
                                                <div class="pull-right">
                                                    <button class="btn btn-danger" type="reset"> <i class="fa fa-remove"></i> Clear</button>
                                                    <button class="btn btn-primary" type="submit"> <i class="fa fa-save"></i> Save </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    if (count($sessions)):
                        ?>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Existing Sessions
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Session Name</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $sn=0;
                                                    foreach($sessions as $session):
                                                ?>
                                                <tr>
                                                    <td><?= ++$sn;?></td>
                                                    <td><?= $session->session_name;?></td>
                                                    <td>
                                                            <!-- Single button -->
                                                            <div class="btn-group">
                                                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                Action <span class="caret"></span>
                                                              </button>
                                                              <ul class="dropdown-menu">
                                                                <li><a href<?=site_url('admin/config/createSession/'.$session->id);?>">Edit</a></li>
                                                                <li><a href="<?=site_url('admin/config/createSession/'.$session->id.'/true');?>">Delete</a></li>
                                                              </ul>
                                                            </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                    endforeach;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    endif;
                    ?>
                </div>
            </div>
            <!--body wrapper end-->