<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Credit Transactions
                </h3>
                <a id = "add_users"></a>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">    
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                Credit Transactions History
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'info') : '' ?>
                                    <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                    <?php
                                        if(count($transactions)){
                                    ?>    
                                        <table class="table table-hover table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Fullname</th>
                                                    <th>Amount</th>
                                                    <th>Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sn=1;
                                                    foreach($transactions as $transaction):
                                                ?>
                                                <tr>
                                                    <td><?=$sn++;?></td>
                                                    <td><?=$transaction->fullname;?></td>
                                                    <td><?=$transaction->amount;?></td>
                                                    <td><?=$transaction->transaction_date;?></td>
                                                    <td>
                                                        <!-- Single button -->
                                                        <div class="btn-group">
                                                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action <span class="caret"></span>
                                                          </button>
                                                          <ul class="dropdown-menu">
                                                              <li><a href="<?=  site_url('customer/shares/receipt/'.md5($transaction->id))?>" target="_blank"> <i class="fa fa-print"></i> Print Receipt</a></li>
                                                            <li role="separator" class="divider"></li>
                                                          </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                    endforeach;
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Fullname</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    <?php
                                        }
                                        else{
                                            echo getAlertMessage('No Record Found');
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
            </div>
        </div>
        <!--body wrapper end-->

