<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Transactions
                </h3>

            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">    
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                Transaction Receipt
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'info') : '' ?>
                                    <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                       <?php if(count($transaction)): ?>
                                    <button class="btn btn-primary" onclick="PrintDiv()"> Print <i class="fa fa-print"></i></button>
                                    <div id="printArea" style="border:1px solid black;padding:1em;">
                                        <table class="table table-hover table-striped table-bordered" border="1px" style="border-collapse:collapse;width:100%;">
                                                <caption>
                                                    <img src="<?=$sitelogo;?>" style="width:200px;"/>
                                                    <h4> Transaction Receipt </h4>
                                                </caption>
                                                <tbody>
                                                    <tr><td> Customer Name </td> <td><?=$transaction->fullname;?></td></tr>
                                                    <tr><td> Transaction Type </td> <td> <b> <?=$transaction_type;?> </b></td> </tr>
                                                    <tr><td> Transaction Amount </td> <td><?=$currency;?> <?=$transaction->amount;?></td></tr>
                                                    <tr><td> Transaction Channel </td> <td><?=$transaction->channel;?></td></tr>
                                                    <tr><td> Transaction  Date </td> <td><?=$transaction->transaction_date;?></td></tr>
                                                    <tr><td> Recorded By </td> <td><?=$transaction->performedBy;?></td></tr>
                                                    <tr><td> Status</td> <td><?= getApprovalLabel($transaction->status);?></td></tr>
                                                </tbody>
                                            </table>
                                        <?php endif;?>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
            </div>
        </div>
        <!--body wrapper end-->

