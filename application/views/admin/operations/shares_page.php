<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Shares
                </h3>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">    
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                RECORD SHARE CONTRIBUTIONS
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'info') : '' ?>
                                    <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                    <form action='<?=  site_url('admin/shares')?>' class="form-horizontal" method="post">
                                    <div class="col-lg-6">
                                        <h4>Enter Transaction Details</h4>
                                        <hr/>
                                        <div class="form-group">
                                            <label class="col-md-3">Customer</label>
                                            <div class="col-md-9">
                                            <?php
                                                $customers = array(''=>'Select Customer');
                                                if(count($existing_customers)){
                                                    foreach ($existing_customers as $customer):
                                                        $customers[$customer->uniqueid] = ucwords(strtolower($customer->fullname));
                                                    endforeach;                                                
                                                echo form_dropdown('customerid', $customers, set_value('customerid'), 'required="" class="form-control"');
                                                }
                                                else{
                                                    echo getAlertMessage('No Active Customers available');
                                                }
                                            ?>
                                            </div>
                                        </div>    
                                        <div class="form-group">
                                            <label class="col-md-3">Amount Contributed</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "amount" type = "number" placeholder="Enter Amount of Transaction" value = "<?php echo set_value('amount') ?>" required = "">    
                                            </div>
                                        </div>                                        
                                        
                                        <div class="form-group">
                                            <label class="col-md-3">Transaction Date</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "transaction_date" type = "date" placeholder="Transaction Date" value = "<?php echo set_value('transaction_date') ?>" required = "">    
                                            </div>
                                        </div>                                        
                                        <div class="form-group">
                                            <label class="col-md-3">Payment Channel</label>
                                            <div class="col-md-9">
                                            <?php
                                                $channel = array(''=>'Select Channel','ONLINE'=>'Online','BANK'=>'BANK','TRANSFER'=>'TRANSFER');
                                                echo form_dropdown('channel', $channel, set_value('channel'), 'required="" class="form-control"');
                                            ?>
                                            </div>
                                        </div>                                        
                                        <hr/>
                                       
                                        <div class="pull-right">
                                            <button class="btn btn-primary" type="submit"> <i class="fa fa-save"></i> Save Transaction</button>
                                        </div>
                                </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    
            </div>
        </div>
        <!--body wrapper end-->

