<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Pending Shares Transactions
                </h3>
                <a id = "add_users"></a>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">    
                    <div class="col-md-12">
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                Pending Shares Transactions
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'info') : '' ?>
                                    <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                        <?php if(count($transactions)) {?>
                                    <table class="table table-hover table-striped table-bordered" id="dynamic-table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Customer Name</th>
                                                    <th>Amount</th>
                                                    <th>Date</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sn=1;
                                                    foreach($transactions as $transaction):
                                                ?>
                                                <tr>
                                                    <td><?=$sn++;?></td>
                                                    <td><?= ucwords($transaction->fullname);?></td>
                                                    <td><?=$transaction->amount;?></td>
                                                    <td><?=$transaction->transaction_date;?></td>
                                                    <td><?= getApprovalLabel($transaction->status);?></td>
                                                    <td>
                                                        <!-- Single button -->
                                                        <div class="btn-group">
                                                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action <span class="caret"></span>
                                                          </button>
                                                          <ul class="dropdown-menu">
                                                              <li><a href="<?=  site_url('admin/shares/receipt/'.md5($transaction->id))?>" target="_blank"> <i class="fa fa-print"></i> View Details</a></li>
                                                              <li><a href="<?=  site_url('admin/shares/approve/'.md5($transaction->id))?>" onclick="return confirm('Are You Sure?')"> <i class="fa fa-gavel"></i> Approve Transaction</a></li>
                                                            <li role="separator" class="divider"></li>
                                                          </ul>
                                                        </div> 
                                                    </td>
                                                </tr>
                                                <?php
                                                    endforeach;
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Fullname</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <?php
                                            }
                                            else{
                                                echo getAlertMessage("No Pending Credit Transactions");
                                            }
                                        ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
            </div>
        </div>
        <!--body wrapper end-->

