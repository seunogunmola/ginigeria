<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Customer Management
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="<?=site_url('admin/customers');?>">Manage Customers</a>
                    </li>
                    <li class="active"> <a href="#existing_users"> <?= $title;?> </a></li>
                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">                    
                    <?php
                        if(count($existing_customers)){
                    ?>
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <?= $title;?> 
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'info') : '' ?>
                                    <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                    <table class="table table-hover table-striped table-bordered" id="dynamic-table">
                                            <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Fullname</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sn=1;
                                                    foreach($existing_customers as $user):
                                                ?>
                                                <tr>
                                                    <td><?=$sn++;?></td>
                                                    <td><?=ucwords( strtolower($user->fullname) );?></td>
                                                    <td><?=$user->email_address;?></td>
                                                    <td><?=$user->phone_number;?></td>
                                                    <td> <?= getStatusLabel($user->status);?>
                                                        <?php
                                                            if($user->status==0){
                                                        ?>
                                                        <br/>
                                                            <a href="<?=site_url('admin/customers/activate/'.$user->id)?>" onclick="return confirm('Are You Sure?')">Activate Customer</a>
                                                        <?php
                                                            }
                                                            else{
                                                        ?>
                                                            <br/>
                                                        <a href="<?=site_url('admin/customers/activate/'.$user->id.'/'.true)?>" onclick="return confirm('Are You Sure?')">Deactivate Customer</a>
                                                        <?php
                                                            }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <!-- Single button -->
                                                        <div class="btn-group">
                                                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action <span class="caret"></span>
                                                          </button>
                                                          <ul class="dropdown-menu">
                                                            <li><a href="#">Full Profile</a></li>
                                                            <li><a href="<?=  site_url('admin/customers/create/'.$user->uniqueid)?>" > <i class="fa fa-edit"></i> Edit</a></li>
                                                            <li><a href="<?=  site_url('admin/customers/resetPassword/'.$user->uniqueid)?>" > <i class="fa fa-refresh"></i> Reset Password</a></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><a href="<?=  site_url('admin/customers/delete/'.$user->uniqueid)?>" onclick="return(confirm('Are You sure You want to delete this user? All records will be deleted'))"> <i class="fa fa-trash"></i> Delete</a></li>
                                                          </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                    endforeach;
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Fullname</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                        }
                        else{
                            echo getAlertMessage("No Records Found!");
                        }
                    ?>
            </div>
        </div>
        <!--body wrapper end-->

