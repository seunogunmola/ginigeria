<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Customer Management
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Manage Customers</a>
                    </li>
                    <li class="active"> <a href="#existing_users"> View All Users </a></li>
                </ul>
            </div>
            <!-- page heading end-->
        
<div class="wrapper">
                <div class="row">
                    <div class="col-md-10">
                    <?= trim($this->session->flashdata('success')) != false ? getAlertMessage($this->session->flashdata('success'),'success') : ''?>
                    <?= trim($this->session->flashdata('error')) != false ? getAlertMessage($this->session->flashdata('error'),'danger') : ''?>
                    <?= trim($error_message)!=false?$error_message:'';?>                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Reset Password for : <?= $user->fullname ?>
                            </div>
                            <div class="panel-body">
                                <?php echo $error_message;?>
                                <div class="row">
                                    <?php echo $message ?>
                                    <form action='' class="form-horizontal" method="post">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="email" class="col-md-3">New Password</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "password" type = "password"  required ="" placeholder="Enter New Password">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="email" class="col-md-3">Confirm New Password</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "confirm_password" type = "password"  required ="" placeholder="Confirm New Password">
                                            </div>
                                        </div>

                                                                             
                                        <div class="pull-right">
                                            <button class="btn btn-primary" type="submit"> <i class="fa fa-refresh"></i> Reset Password</button>
                                        </div>
                                </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>
                    
            </div>
        </div>
