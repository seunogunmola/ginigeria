<!DOCTYPE html>
<html lang="en">
<head>
  <title><?php echo config_item('sitename') ?> </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="keywords" content="<?= $sitename ?>, <?php echo config_item('keywords') ?>">
  <meta name="description" content="<?= $sitename ?>">
  <meta name="author" content="<?= $sitename ?>,<?php echo config_item('keywords') ?>">
  <link rel="shortcut icon" href="#" type="image/png">
  <link href="<?php echo getResource('js/iCheck/skins/minimal/minimal.css')?>" rel="stylesheet">
  <link href="<?php echo getResource('css/style.css')?>" rel="stylesheet">
  <link href="<?php echo getResource('css/style-responsive.css')?>" rel="stylesheet">
  <link href="<?php echo getResource('css/bootstrap-fileupload.min.css')?>" rel="stylesheet">
  <link href="<?php echo getResource('css/bootstrap-formhelpers.min.css')?>" rel="stylesheet">
  <link href="<?php echo getResource('vendors/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
  <script src="<?php echo getResource('js/jquery-1.10.2.min.js') ?>"></script>
  <script src="<?php echo getResource('js/bootstrap-formhelpers.js') ?>"></script>
  <script src="<?php echo getResource('js/custom_scripts.js') ?>"></script>
    <?php //$page_level_styles; ?>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
  <style>
#imagePreview {
    width: 180px;
    height: 180px;
    background-position: center center;
    background-size: cover;
    -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
    display: inline-block;
}
</style>
</head>



        <!-- header section start-->
        <div class="header-section">

            <!--toggle button start-->
            <a class="toggle-btn"><i class="fa fa-bars"></i></a>
            <!--toggle button end-->
            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="" alt="" />
                                <?php echo $userdata->fullname;?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                            <li><a href="<?php echo site_url('admin/auth/logout')?>" onclick="return(confirm('Are You Sure You want to Logout?'))"><i class="fa fa-sign-out" ></i> Log Out</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->


        <div class="left-side sticky-left-side">

        <!--logo and iconic logo start-->
        <div class="logo" style="background-color:white">
            <a href="<?php echo site_url('admin/dashboard'); ?>"> <img src="<?php echo $sitelogo ?>" style="max-width:150px;margin-bottom:5px" alt=""/> </a>
        </div>

        <div class="logo-icon text-center">
            <a href="index.html"><img src="<?php echo $sitelogo ?>" alt=""></a>
        </div>
        <!--logo and iconic logo end-->

        <div class="left-side-inner">

            <!-- visible to small devices only -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media logged-user">
                    <img alt="" src="" class="media-object">
                    <div class="media-body">
                        <h4><a href="#"> <?php echo $this->session->userdata['fullname'];?></a></h4>
                    </div>
                </div>

                <h5 class="left-nav-title">Account Information</h5>
                <ul class="nav nav-pills nav-stacked custom-nav">
                  <li><a href="#"><i class="fa fa-user"></i> <span>Profile</span></a></li>
                  <li><a href="#"><i class="fa fa-cog"></i> <span>Settings</span></a></li>
                  <li><a href="#"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
                </ul>
            </div>
