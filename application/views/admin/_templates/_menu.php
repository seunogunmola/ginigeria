            <ul class="nav nav-pills nav-stacked custom-nav">
                <li class="active"><a href="<?php echo site_url('admin/dashboard') ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>

                <?php if(in_array('config', $privileges) || in_array('god', $privileges)):?>
                <li class="menu-list"><a href=""><i class="fa fa-users"></i> <span>Customers</span></a>
                    <ul class="sub-menu-list">
                        <li> <a href="<?php echo site_url('admin/customers')?>"> <i class="fa fa-eye"></i> View All Customers</a></li>
                        <li> <a href="<?php echo site_url('admin/customers/index/1')?>"> <i class="fa fa-eye"></i> View Pending Customers</a></li>
                        <li> <a href="<?php echo site_url('admin/customers/create')?>"> <i class="fa fa-user"></i> Add New Customer</a></li>
                    </ul>
                </li>
                 <?php endif;?>
                
                <?php if(in_array('config', $privileges) || in_array('god', $privileges)):?>
                <li class="menu-list"><a href=""><i class="fa fa-pie-chart"></i> <span>Shares Transactions</span></a>
                    <ul class="sub-menu-list">
                        <li> <a href="<?php echo site_url('admin/shares')?>"> <i class="fa fa-plus-circle"></i> Post Share Contribution</a></li>
                        <li> <a href="<?php echo site_url('admin/shares/pending')?>"> <i class="fa fa-plus-circle"></i> Pending Share Contributions</a></li>
                        <li> <a href="<?php echo site_url('admin/shares/history')?>"> <i class="fa fa-history"></i> Shares History</a></li>
                    </ul>
                </li>
                 <?php endif;?>
                <?php if(in_array('config', $privileges) || in_array('god', $privileges)):?>
                <li class="menu-list"><a href=""><i class="fa fa-money"></i> <span>Credit Transactions</span></a>
                    <ul class="sub-menu-list">
                        <li> <a href="<?php echo site_url('admin/credit')?>"> <i class="fa fa-plus-circle"></i> Post Contribution</a></li>
                        <li> <a href="<?php echo site_url('admin/credit/pending')?>"> <i class="fa fa-plus-circle"></i> Pending Contributions</a></li>
                        <li> <a href="<?php echo site_url('admin/credit/history')?>"> <i class="fa fa-history"></i> Contributions History</a></li>
                    </ul>
                </li>
                 <?php endif;?>
                <?php if(in_array('config', $privileges) || in_array('god', $privileges)):?>
                <li class="menu-list"><a href=""><i class="fa fa-money"></i> <span>Debit Transactions</span></a>
                    <ul class="sub-menu-list">
                        <li> <a href="<?php echo site_url('admin/debit')?>"> <i class="fa fa-minus-circle"></i> Debit Customer</a></li>
                        <li> <a href="<?php echo site_url('admin/debit/history')?>"> <i class="fa fa-history"></i> Debit History</a></li>
                    </ul>
                </li>
                 <?php endif;?>
                
                <?php if(in_array('config', $privileges) || in_array('god', $privileges)):?>
                <li class="menu-list"><a href=""><i class="fa fa-dollar"></i> <span>Loans</span></a>
                    <ul class="sub-menu-list">
                        <li> <a href="<?php echo site_url('admin/loan')?>"> <i class="fa fa-minus-circle"></i> Loan Applications</a></li>                       
                    </ul>
                </li>
                 <?php endif;?>
                
                <?php if(in_array('report', $privileges) || in_array('god', $privileges)):?>
                <li class="menu-list"><a href=""><i class="fa fa-line-chart"></i> <span>Reports</span></a>
                    <ul class="sub-menu-list">
                        <li> <a href="<?php echo site_url('admin/reports/credit')?>"> <i class="fa fa-plus-circle"></i> Credit Transactions</a></li>                       
                        <li> <a href="<?php echo site_url('admin/reports/debit')?>"> <i class="fa fa-minus-circle"></i> Debit Transactions</a></li>                       
                        <li> <a href="<?php echo site_url('admin/reports/individual')?>"> <i class="fa fa-user-plus"></i> Individual Transactions</a></li>                       
<!--                        <li> <a href="<?php echo site_url('admin/loan')?>"> <i class="fa fa-minus-circle"></i> Loans Report</a></li>                       
                        <li> <a href="<?php echo site_url('admin/loan')?>"> <i class="fa fa-minus-circle"></i> Purchases Report</a></li>                       -->
                    </ul>
                </li>
                 <?php endif;?>
              
                <li><a onclick="return confirm('Are You Sure?')" href="<?php echo site_url('admin/auth/logout') ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
                
            </ul>
        </div>
    </div>