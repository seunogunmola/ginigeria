<style>
    @media print {
        .table {
            border:1px solid black;
            width:100%!important;
            border-collapse:collapse!important;
        }
    }
</style>
<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Debit Transactions
                </h3>
                <a id = "add_users"></a>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">    
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                Debit Transactions Reports
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div style="border:1px solid crimson;padding:1em;">
                                        <?= trim($error_message) != false ? getAlertMessage($error_message): ''; ?>
                                        <form action="" method="post">
                                        <fieldset>
                                            <legend>Filter</legend>
                                            <div class="form-group col-md-3">
                                                <label for="dateFrom">
                                                    From
                                                </label>
                                                <input type="date" name="fromDate" value="<?= set_value('fromDate')?>" required="" class="form-control col-md-3" placeholder="From"/>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="dateTo"> To</label>
                                                <input type="date" name="toDate" required="" value="<?= set_value('toDate')?>" class="form-control col-md-3" placeholder="From"/>
                                            </div>
                                            <div class="form-group col-md-3">
                                            <label>Payment Channel</label>
                                            <?php
                                                $channel = array('all'=>'All','ONLINE'=>'Online','BANK'=>'BANK','TRANSFER'=>'TRANSFER');
                                                echo form_dropdown('channel', $channel, set_value('channel'), 'required="" class="form-control"');
                                            ?>
                                            </div>                                             
                                            <div class="form-group col-md-3">
                                                <label for="button"> </label>
                                                <br/>
                                                <button class="btn btn-primary" name="filter" type="submit"> Filter <i class="fa fa-filter"></i></button>        
                                            </div>                                    
                                        </fieldset>
                                        </form>
                                    </div>
                                    <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'info') : '' ?>
                                    <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                    <div style="padding:1em">
                                        <button class="btn btn-primary" onclick="PrintDiv()"> Print <i class="fa fa-print"></i></button>
                                    </div>
                                    <div id="printArea" style="border:1px solid black;padding:1em;">
                                        <?php
                                            if(!count($transactions)){
                                                echo getAlertMessage("No Transaction Records found!");
                                            }
                                            else{
                                        ?>
                                        <table class="table table-hover table-striped table-bordered" width="100%" style="border-collapse:collapse" border="1">
                                        <caption>
                                            <img src="<?=$sitelogo;?>" style="width:200px;"/>    
                                            <h4><?=$report_title?></h4>
                                        </caption>
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Customer Name</th>
                                                <th>Amount</th>
                                                <th>Transaction Channel</th>
                                                <th>Transaction Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $total = 0;
                                            $sn = 1;
                                            foreach ($transactions as $transaction):
                                                $total+=$transaction->amount;
                                                ?>
                                                <tr>
                                                    <td><?= $sn++; ?></td>
                                                    <td><?= ucwords(strtolower($transaction->fullname)); ?></td>
                                                    <td><?= $transaction->amount; ?></td>
                                                    <td><?= $transaction->channel; ?></td>
                                                    <td><?= $transaction->transaction_date; ?></td>
                                                </tr>
                                                <?php
                                            endforeach;
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th>Total</th>
                                                <th><?= $currency.number_format($total,2); ?></th>
                                            </tr>
                                            </tr>
                                        </tfoot>
                                    </table>
                                        <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!--body wrapper end-->

