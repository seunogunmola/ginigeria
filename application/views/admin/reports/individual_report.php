<style>
    @media print {
        .table {
            border:1px solid black;
            width:100%!important;
            border-collapse:collapse!important;
        }
    }
</style>
<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Individual Transactions
                </h3>
                <a id = "add_users"></a>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">    
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                Individual Reports
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div style="border:1px solid crimson;padding:1em;">

                                        <?= trim($error_message) != false ? getAlertMessage($error_message) : ''; ?>
                                        <form action="" method="post">
                                       <i style="color:crimson">
                                                    Leave dates blank if You want to view All Transactions
                                                </i>                                            
                                            <fieldset>
                                                <legend>Filter</legend>
         
                                                <div class="form-group col-md-3">
                                                    <label for="dateFrom">
                                                        Customer Name
                                                    </label>
                                                    <?php
                                                    $customers = array('' => 'Select Customer');
                                                    if (count($existing_customers)) {
                                                        foreach ($existing_customers as $customer):
                                                            $customers[$customer->uniqueid] = ucwords(strtolower($customer->fullname));
                                                        endforeach;
                                                        echo form_dropdown('customerid', $customers, set_value('customerid'), 'required="" class="form-control"');
                                                    }
                                                    else {
                                                        echo getAlertMessage('No Active Customers available');
                                                    }
                                                    ?>                     

                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="dateFrom">
                                                        From
                                                    </label>
                                                    <input type="date" name="fromDate" value="<?= set_value('fromDate') ?>"  class="form-control col-md-3" placeholder="From"/>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="dateTo"> To</label>
                                                    <input type="date" name="toDate"  value="<?= set_value('toDate') ?>" class="form-control col-md-3" placeholder="From"/>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="button"> </label>
                                                    <br/>
                                                    <button class="btn btn-primary" name="filter" type="submit"> Filter <i class="fa fa-filter"></i></button>        
                                                </div>                                    
                                            </fieldset>
                                        </form>
                                    </div>
                                    <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'info') : '' ?>
                                    <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                    <div style="padding:1em">
                                        <button class="btn btn-primary" onclick="PrintDiv()"> Print <i class="fa fa-print"></i></button>
                                    </div>
                                    <div id="printArea" style="border:1px solid black;padding:1em;">
                                        <?php
                                        if (!isset($credit_transactions) || !isset($debit_transactions)) {
                                            echo getAlertMessage("No Transaction Records found!");
                                        } else {
                                            ?>
                                            <table class="table table-hover table-striped table-bordered" width="100%" style="border-collapse:collapse" border="1">
                                                <caption>
                                                    <img src="<?= $sitelogo; ?>" style="width:200px;"/>    
                                                    <h4><?= $report_title ?></h4>
                                                    <hr/>
                                                </caption>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <h4>Credit Transactions</h4>
                                                            <?php
                                                                if(!isset($credit_transactions)){
                                                                    $total_credit = 0;
                                                                    echo getAlertMessage('No Credit Transactions Found!');
                                                                }
                                                                else{
                                                            ?>
                                                                <table border="1" style="border-collapse:collapse;" width="100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>
                                                                                Transaction Date
                                                                            </th>
                                                                            <th>
                                                                                Transaction Channel
                                                                            </th>
                                                                            <th>
                                                                                Amount
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                            $total_credit = 0;
                                                                            foreach($credit_transactions as $credit){
                                                                                $total_credit += $credit->amount;
                                                                        ?>
                                                                                <tr>
                                                                                    <td>
                                                                                        <?= $credit->transaction_date;?>
                                                                                    </td>
                                                                                    <td> <?= $credit->channel;?></td>
                                                                                    <td>
                                                                                        <?= $credit->amount;?>
                                                                                    </td>
                                                                                </tr>                                                                        
                                                                        <?php
                                                                            }
                                                                        ?>
                                                                    </tbody>
                                                                    <tfoot>
                                                                        <tr>
                                                                            <th> </th>
                                                                            <th> Sub-total</th>
                                                                            <th><?= number_format($total_credit,2); ?></th>
                                                                        </tr>
                                                                    </tfoot>
                                                                </table>
                                                            <?php
                                                                }
                                                            ?>
                                                        </td>
                                                        
                                                        <td>
                                                            <h4>Debit Transactions</h4>
                                                            <?php
                                                                if(!isset($debit_transactions)){
                                                                    $total_debit = 0;
                                                                    echo getAlertMessage('No Credit Transactions Found!');
                                                                }
                                                                else{
                                                            ?>
                                                                <table border="1" style="border-collapse:collapse;" width="100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>
                                                                                Transaction Date
                                                                            </th>
                                                                            <th>
                                                                                Transaction Channel
                                                                            </th>
                                                                            <th>
                                                                                Amount
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                        $total_debit = 0;
                                                                            foreach($debit_transactions as $debit){
                                                                                $total_debit+=$debit->amount;
                                                                        ?>
                                                                                <tr>
                                                                                    <td>
                                                                                        <?= $debit->transaction_date;?>
                                                                                    </td>
                                                                                    <td> <?= $debit->channel;?></td>
                                                                                    <td>
                                                                                        <?= $debit->amount;?>
                                                                                    </td>
                                                                                </tr>                                                                        
                                                                        <?php
                                                                            }
                                                                        ?>
                                                                                
                                                                    </tbody>
                                                                    <tfoot>
                                                                        <tr>
                                                                            <th> </th>
                                                                            <th> Sub-total</th>
                                                                            <th><?= number_format($total_debit,2); ?></th>
                                                                        </tr>
                                                                    </tfoot>
                                                                </table>
                                                            <?php
                                                                }
                                                            ?>                                                            
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td style="text-align:right;">
                                                            Net Worth
                                                        </td>
                                                        <td>
                                                            <?= $currency. number_format($total_credit - $total_debit,2); ?>
                                                        </td>
                                                    </tr>
                                                </tfoot>                                                
                                                
                                            </table>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!--body wrapper end-->

