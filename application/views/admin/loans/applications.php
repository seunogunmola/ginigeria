<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Loans
                </h3>
                <a id = "add_users"></a>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">    
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                Loan Applications
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'info') : '' ?>
                                    <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                        <table class="table table-hover table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Fullname</th>
                                                    <th>Phone Number</th>
                                                    <th>Amount (<?=$currency;?>)</th>
                                                    <th>Duration (Months)</th>
                                                    <th>Application Date</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sn=1;
                                                    foreach($loans as $loan):
                                                ?>
                                                <tr>
                                                    <td><?=$sn++;?></td>
                                                    <td><?=$loan->fullname;?></td>
                                                    <td><?=$loan->phone_number;?></td>
                                                    <td><?=$loan->loanAmount;?></td>
                                                    <td><?=$loan->duration;?></td>
                                                    <td><?=date($loan->datecreated);?></td>
                                                    <td><?php          
                                                        
                                                        switch ($loan->status) :
                                                                case('0'): $message='<label class="label label-warning"> Pending </label>';break;
                                                                case('1'): $message='<label class="label label-success"> Approved </label>';break;
                                                                case('2'): $message='<label class="label label-danger"> Declined </label>';break;
                                                            endswitch;
                                                            echo $message;
                                                        ?>                                                            
                                                    <td>
                                                        <!-- Single button -->
                                                        <div class="btn-group">
                                                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action <span class="caret"></span>
                                                          </button>
                                                          <ul class="dropdown-menu">
                                                              <li><a href="<?=  site_url('admin/loan/process/'.md5($loan->id))?>" target="_blank"> <i class="fa fa-cogs"></i> Process Loan</a></li>
                                                            <li role="separator" class="divider"></li>
                                                          </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                    endforeach;
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Fullname</th>
                                                    <th>Phone Number</th>
                                                    <th>Amount</th>
                                                    <th>Duration</th>
                                                    <th>Application Date</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                                </tr>
                                            </tfoot>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
            </div>
        </div>
        <!--body wrapper end-->

