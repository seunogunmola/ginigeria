<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Process Loan
                </h3>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">    
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                Process Loan
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'info') : '' ?>
                                    <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                    
                                    <div class="col-lg-6">
                                        <h4>Loan Application Detials</h4>
                                        <hr/>
                                        <table class="table table-bordered">
                                            <tr>
                                                <td>Customer Name</td>
                                                <td><?=$loanData->fullname;?></td>
                                            </tr>
                                            <tr>
                                                <td>Amount  Requested</td>
                                                <td><?=$loanData->loanAmount;?></td>
                                            </tr>
                                            <tr>
                                                <td>Payment Duration</td>
                                                <td><?=$loanData->duration;?></td>
                                            </tr>
                                            <tr>
                                                <td>Application Date</td>
                                                <td><?=$loanData->datecreated;?></td>
                                            </tr>
                                            <tr style="background:black;color:white;">
                                                <td colspan="2"> Customers Track Record</td>
                                            </tr>
                                            <tr>
                                                <td>Customers Account Balance</td>
                                                <td><?= $currency.' '. trim($loanData->balance)!=false? number_format($loanData->balance,2) :'0';?></td>
                                            </tr>
                                        </table>
                                        
                                        <form action="" method="post">
                                            <div class="form-group">
                                                <label for="status">
                                                    Loan Application Status
                                                </label>
                                                <?php
                                                    $status = array(
                                                        ''=>'Select Status',
                                                        '0'=>'Pending',
                                                        '2'=>'Declined',
                                                        '1'=>'Approved'
                                                    );
                                                    echo form_dropdown('status', $status, set_value('status',$loanData->status), 'class="form-control" required');
                                                ?>
                                            <div class="form-group">
                                                <label for="status">
                                                    Comments/ Reasons for Decline if Declined
                                                </label>
                                                <textarea name="comments" class="form-control"><?=set_value('comments',$loanData->comments);?></textarea>
                                            </div>
                                        <div class="pull-right">
                                            <button class="btn btn-primary" type="submit"> <i class="fa fa-save"></i> Save Status</button>
                                        </div>                                            
                                        </form>
                                       

                                </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
            </div>
        </div>
        <!--body wrapper end-->

