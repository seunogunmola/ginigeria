<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Activate Session
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="<?= site_url('admin/config/createSession'); ?>">Create Session</a>
                    </li>
                    <li class="active"> <a href="<?= site_url('admin/config/activateSession'); ?>"> Set Active Session </a></li>
                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Set Active Session
                            </div>
                            <div class="panel-body">
                                <?php echo $error_message; ?>
                                <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'info') : '' ?>
                                <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                <div class="row">
                                    <?php echo $message ?>
                                    <form action='' class="form-horizontal" method="post">
                                        <div class="col-lg-12">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label class="col-md-3">Select Institution</label>
                                                    <div class="col-md-9">
                                                    <div class="bfh-selectbox" data-name="institution_id" data-value="" data-filter="true">
                                                        <div data-value="">Select Institution</div>
                                                        <?php
                                                            foreach($institutions as $institution):
                                                        ?>
                                                      <div data-value="<?=$institution->id;?>"><?=$institution->name;?></div>
                                                      <?php
                                                        endforeach;
                                                      ?>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3">Active Session</label>
                                                    <div class="col-md-9">
                                                    <div class="bfh-selectbox" data-name="session_id" data-value="" data-filter="true">
                                                        <div data-value="">Select Session</div>
                                                        <?php
                                                            foreach($sessions as $session):
                                                        ?>
                                                      <div data-value="<?=$session->id;?>"><?=$session->session_name;?></div>
                                                      <?php
                                                        endforeach;
                                                      ?>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="pull-right">
                                                    <button class="btn btn-danger" type="reset"> <i class="fa fa-remove"></i> Clear</button>
                                                    <button class="btn btn-primary" type="submit"> <i class="fa fa-save"></i> Save </button>
                                                </div>
                                            </div
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                    <?php
                        if(count($activeSessions)):
                    ?>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Active Sessions
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Institution Name</th>
                                                <th>Active Session</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $sn=0;
                                                foreach($activeSessions as $activeSession):
                                            ?>
                                            
                                                <tr>
                                                    <th><?=++$sn;?></th>
                                                    <th><?=$activeSession->institution_name;?></th>
                                                    <th><?=$activeSession->session_name;?></th>
                                                </tr>
                                            
                                            <?php
                                                endforeach;
                                            ?>
                                        </tbody>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                    endif;
                ?>
                </div>
            </div>
            <!--body wrapper end-->

