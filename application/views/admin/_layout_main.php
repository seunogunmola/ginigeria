<?php
#LOAD HEADER
$this->load->view('admin/_templates/_head');

#LOAD MENU
$this->load->view('admin/_templates/_menu');

#LOAD SUBVIEW
$this->load->view($subview);

#LOAD FOOTER
$this->load->view('admin/_templates/_foot');