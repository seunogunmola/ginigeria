<body class="sticky-header">
    <section>
        <!-- main content start-->
        <div class="main-content" >
            <!-- page heading start-->
            <div class="page-heading">
                <h3>
                    Dashboard
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                    <li class="active"> My Dashboard </li>
                </ul>
            </div>
            <!-- page heading end-->

            <!--body wrapper start-->
            <div class="wrapper" style="background-color: white;">
                <div class="row">
                    <div class="col-md-12">
                        <!--statistics start-->
                        <div class="row state-overview">
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <a href="<?= site_url('admin/credit/history') ?>" style="color:white!important;">
                                    <section class="panel blue">
                                        <div class="panel-body">
                                            <div class="summary">
                                                <span>Total Credit Transactions</span>
                                                <h2 class="red-txt"><?= $currency . number_format($creditTransactionsVolume, 2) ?></h2credi>
                                            </div>                                        
                                        </div>
                                    </section>
                                </a>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <a href="<?= site_url('admin/shares/history') ?>" style="color:white!important;">
                                    <section class="panel orange">
                                        <div class="panel-body">
                                            <div class="summary">
                                                <span>Total Shares Contributed</span>
                                                <h3 class="red-txt"><?= $currency . number_format($sharesVolume, 2) ?></h3>
                                            </div>                                        
                                        </div>
                                    </section>                                    
                                </a>
                            </div>
                        </div>
                        <!--statistics end-->
                    </div>
                    <div class="col-md-6">
                        <h4 class="alert alert-info">Quick Stats</h4>
                        <!--statistics start-->
                        <div class="row state-overview">
                            <div class="col-md-6 col-xs-12 col-sm-6">

                                <a href="<?php echo site_url('admin/customers') ?>" style="color:white">
                                    <div class="panel purple">
                                        <div class="symbol">
                                            <i class="fa fa-users"></i>
                                        </div>
                                        <div class="state-value">
                                            <div class="value"> <?= isset($customers) ? $customers : '0' ?> </div>
                                            <div class="title">Active Customers</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <a href="<?php echo site_url('admin/credit/history') ?>" style="color:white">
                                    <div class="panel green">
                                        <div class="symbol">
                                            <i class="fa fa-money"></i>
                                        </div>
                                        <div class="state-value">
                                            <div class="value"> <?= isset($credit_transactions) ? $credit_transactions : '0' ?> </div>
                                            <div class="title">Credit Transactions</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <a href="<?php echo site_url('admin/debit/history') ?>" style="color:white">
                                    <div class="panel red">
                                        <div class="symbol">
                                            <i class="fa fa-money"></i>
                                        </div>
                                        <div class="state-value">
                                            <div class="value"> <?= isset($debit_transactions) ? $debit_transactions : '0' ?> </div>
                                            <div class="title">Debit Transactions</div>
                                        </div>
                                    </div>
                                </a>
                            </div>                 
                        </div>
                        <!--statistics end-->
                    </div>
                    <div class="col-md-6">
                        <h4 class="alert alert-danger">Pending Actions</h4>
                        <!--statistics start-->
                        
                        <div class="row state-overview">
                            <?php if ($inactive_customers > 0) {?>
                                <div class="col-md-6 col-xs-12 col-sm-6">
                                    <a href="<?php echo site_url('admin/customers/index/1') ?>" style="color:white">
                                        <div class="panel purple">
                                            <div class="symbol">
                                                <i class="fa fa-users"></i>
                                            </div>
                                            <div class="state-value">
                                                <div class="value"> <?= isset($inactive_customers) ? $inactive_customers : '0' ?> </div>
                                                <div class="title">In Active Customers</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                        <?php } ?>
                            
                            <?php if ($pending_credit_transactions > 0) {?>
                                <!--//CREDIT TRANSACTIONS-->
                                <div class="col-md-6 col-xs-12 col-sm-6">
                                    <a href="<?php echo site_url('admin/credit/pending') ?>" style="color:white">
                                        <div class="panel red">
                                            <div class="symbol">
                                                <i class="fa fa-money"></i>
                                            </div>
                                            <div class="state-value">
                                                <div class="value"> <?= isset($pending_credit_transactions) ? $pending_credit_transactions : '0' ?> </div>
                                                <div class="title">Pending Credit Transactions</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>
                                
                                <!--//CREDIT TRANSACTIONS-->
                                <?php if ($pending_shares > 0) {?>
                                <div class="col-md-6 col-xs-12 col-sm-6">
                                    <a href="<?php echo site_url('admin/shares/pending') ?>" style="color:white">
                                        <div class="panel red">
                                            <div class="symbol">
                                                <i class="fa fa-pie-chart"></i>
                                            </div>
                                            <div class="state-value">
                                                <div class="value"> <?= isset($pending_shares) ? $pending_shares : '0' ?> </div>
                                                <div class="title">Pending Shares Transactions</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <?php } ?>
                                
                                
                        </div>
                        <!--statistics end-->
                    </div>
                    
                </div>

