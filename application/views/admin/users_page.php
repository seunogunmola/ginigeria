<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Admin User Management
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="<?= site_url('admin/users/create'); ?>">Manage Admin Users</a>
                    </li>
                    <li class="active"> <a href="#existing_users"> <?= $title; ?> </a></li>
                </ul>
            </div>
            <!-- page heading end-->
            <!--body wrapper start-->

            <div class="wrapper">
                <div class="row">                    
                    <?php
                    if (count($existing_users)) {
                        ?>
                        <div class="col-md-6">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    Add New User
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <form class="" action="" method="post">
                                            <div class="col-lg-12">
                                        <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'info') : '' ?>
                                        <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>                                                
                                                <?= isset($error_message) ? $error_message : ''; ?>
                                                <div class="form-group">
                                                    <label for="fullname">Fullname</label>
                                                    <input type="text" name="fullname" placeholder="Enter Fullname" class="form-control" required="" value="<?= set_value('fullname',$user->fullname) ?>"/>
                                                </div>
                                                <div class="form-group">
                                                    <label class="">Email Address</label>
                                                    <div class="">
                                                        <input class="form-control"  name = "email_address"  type = "email" placeholder="Enter User's Email" value = "<?php echo set_value('email_address', $user->email_address) ?>" required = "">    
                                                    </div>
                                                </div>                                        
                                                <div class="form-group">
                                                    <label class="">Phone</label>
                                                    <div class="">
                                                        <input class="form-control"  name = "phone_number" maxlength="11" type = "text" placeholder="Enter User's Phone" value = "<?php echo set_value('phone_number', $user->phone_number) ?>" required = "">    
                                                    </div>
                                                </div>   
                                                <h4>Privileges</h4>
                                                <?php
                                                    $users_privilege = explode(',', $user->privileges);
                                                ?>
                                                <hr/>                                            
                                                <div>
                                                    <label> <input type="checkbox" class="" <?= in_array('superadmin',$users_privilege) ? "checked" : "";?> value="superadmin" name="privileges[]" onchange="togglePrivileges()" id="superAdminPrivilege" /> Super Admin</label>
                                                </div>
                                                <div>
                                                    <label> <input type="checkbox" class="" name="privileges[]" value="customers" <?= in_array('superadmin',$users_privilege) || in_array('customers',$users_privilege)  ? "checked" : "";?> /> Customer Management</label>
                                                    <label> <input type="checkbox" class="" name="privileges[]" value="shares" <?= in_array('superadmin',$users_privilege) || in_array('shares',$users_privilege) ? "checked" : "";?> /> Shares Management</label>
                                                </div>
                                                <div>
                                                    <label> <input type="checkbox" class="" name="privileges[]"  value="transactionPosting" <?= in_array('superadmin',$users_privilege) || in_array('transactionPosting',$users_privilege)  ? "checked" : "";?>/> Posting Transactions</label>
                                                    <label> <input type="checkbox" class="" name="privileges[]" value="transactionApproval" <?= in_array('superadmin',$users_privilege) || in_array('transactionApproval',$users_privilege)  ? "checked" : "";?> /> Approving Transactions</label>
                                                </div>
                                                <div>
                                                    <label> <input type="checkbox" class="" name="privileges[]" value="debit" <?= in_array('superadmin',$users_privilege) || in_array('debit',$users_privilege)  ? "checked" : "";?> /> Debit Transactions</label>
                                                </div>
                                                <div>
                                                    <label> <input type="checkbox" class="" name="privileges[]" value="report" <?= in_array('superadmin',$users_privilege) || in_array('report',$users_privilege) ? "checked" : "";?>  /> Generating Reports</label>
                                                </div>
                                                <div>
                                                    <label> <input type="checkbox" class="" name="privileges[]" value="loan" <?= in_array('superadmin',$users_privilege) || in_array('loan',$users_privilege)  ? "checked" : "";?> /> Loan Approval</label>
                                                </div>
                                            </div>                                        
                                            <div align="center">
                                                <button type="submit" class="btn btn-primary"> Save <i class="fa fa-save"></i></button>
                                                <button type="reset" class="btn btn-danger"> Reset <i class="fa fa-refresh"></i></button>
                                            </div>
                                        </form>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <?= $title; ?> 
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <?php echo $this->session->flashdata('success') ? getAlertMessage($this->session->flashdata('success'), 'info') : '' ?>
                                        <?php echo $this->session->flashdata('error') ? getAlertMessage($this->session->flashdata('error'), 'danger') : '' ?>
                                        <table class="table table-hover table-striped table-bordered" id="dynamic-table">
                                            <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Fullname</th>
                                                    <th>Email</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sn = 1;
                                                foreach ($existing_users as $user):
                                                    ?>
                                                    <tr>
                                                        <td><?= $sn++; ?></td>
                                                        <td><?= ucwords(strtolower($user->fullname)); ?></td>
                                                        <td><?= $user->email_address; ?></td>
                                                        <td> <?= getStatusLabel($user->status); ?>
                                                            <?php
                                                            if ($user->status == 0) {
                                                                ?>
                                                                <br/>
                                                                <a href="<?= site_url('admin/users/activate/' . $user->id) ?>" onclick="return confirm('Are You Sure?')">Activate User</a>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <br/>
                                                                <a href="<?= site_url('admin/users/activate/' . $user->id . '/' . true) ?>" onclick="return confirm('Are You Sure?')">Deactivate User</a>
                                                                <?php
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <!-- Single button -->
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Action <span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="#">Full Profile</a></li>
                                                                    <li><a href="<?= site_url('admin/users/create/' . $user->uniqueid) ?>" > <i class="fa fa-edit"></i> Edit</a></li>
                                                                    <li><a href="<?= site_url('admin/users/resetPassword/' . $user->uniqueid) ?>" > <i class="fa fa-refresh"></i> Reset Password</a></li>
                                                                    <li role="separator" class="divider"></li>
                                                                    <li><a href="<?= site_url('admin/users/delete/' . $user->uniqueid) ?>" onclick="return(confirm('Are You sure You want to delete this user? All records will be deleted'))"> <i class="fa fa-trash"></i> Delete</a></li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Fullname</th>
                                                    <th>Email</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    } else {
                        echo getAlertMessage("No Records Found!");
                    }
                    ?>
                </div>
            </div>
            <!--body wrapper end-->
            <script>
                function togglePrivileges(){
                    var status = $("#superAdminPrivilege").prop("checked");
                    alert(status);
                }
            </script>
