<body class="sticky-header">
    <section>
        <div class="main-content" >
            <div class="page-heading">
                <h3>
                    Customer Management
                </h3>
                <a id = "add_users"></a>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Manage Customers</a>
                    </li>
                    <li class="active"> <a href="#existing_users"> View All Users </a></li>
                </ul>
            </div>
            <!-- page heading end-->
        
<div class="wrapper">
                <div class="row">
                    <div class="col-md-10">
                    <?= trim($this->session->flashdata('success')) != false ? getAlertMessage($this->session->flashdata('success'),'success') : ''?>
                    <?= trim($this->session->flashdata('error')) != false ? getAlertMessage($this->session->flashdata('error'),'danger') : ''?>
                    <?= trim($error_message)!=false?$error_message:'';?>                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Enter your Accurate Details
                            </div>
                            <div class="panel-body">
                                <?php echo $error_message;?>
                                <div class="row">
                                    <?php echo $message ?>
                                    <form action='<?=  site_url('admin/customers/create/'.$uniqueid)?>' class="form-horizontal" method="post">
                                    <div class="col-lg-12">
                                        <h4>Bio Data</h4>
                                        <hr/>
                                        <div class="form-group">
                                            <label for="email" class="col-md-3">Email Address</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "email_address" type = "email" required ="" placeholder="Email" value = "<?php echo set_value('email_address', $user->email_address) ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3">Title</label>
                                            <div class="col-md-9">
                                            <?php
                                                $title = array(''=>'Select Title','Mr'=>'Mr','Mrs'=>'Mrs','Miss'=>'Miss','Dr'=>'Dr');
                                                echo form_dropdown('title', $title, set_value('title',$user->title), 'required="" class="form-control"');
                                            ?>
                                            </div>
                                        </div>                                        
                                        <div class="form-group">
                                            <label class="col-md-3">Fullname</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "fullname" type = "text" placeholder="Enter User's Fullname" value = "<?php echo set_value('fullname', $user->fullname) ?>" required = "">    
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3">Phone</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "phone_number" maxlength="11" type = "text" placeholder="Enter User's Phone" value = "<?php echo set_value('phone_number', $user->phone_number) ?>" required = "">    
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3">Gender</label>
                                            <div class="col-md-9">
                                            <?php
                                                $gender = array(''=>'Select Gender','Male'=>'Male','Female'=>'Female');
                                                echo form_dropdown('gender', $gender, set_value('gender',$user->gender), 'required="" class="form-control"');
                                            ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3">Date of Birth</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "dob" type="date" placeholder="DOB" max="2000-01-01" value = "<?php echo set_value('dob', $user->dob) ?>" required = "">    
                                            </div>
                                        </div>                                        
                                        
                                        <hr/>
                                        <h4>Bank Account Details</h4>
                                        <hr/>
                                        
                                        <div class="form-group">
                                            <label class="col-md-3">BVN</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "bvn" maxlength="12" type = "number" placeholder="Enter your BVN" value = "<?php echo set_value('bvn', $user->bvn) ?>" required = "">    
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-md-3">Bank Name</label>
                                            <div class="col-md-9">
                                            <?php
                                                $banks = array(''=>'Select Bank','GTB'=>'GTB','FBN'=>'First Bank','Access'=>'Access');
                                                echo form_dropdown('bankName', $banks, set_value('bankName',$user->bankName), 'required="" class="form-control"');
                                            ?>
                                            </div>
                                        </div>  
                                        <div class="form-group">
                                            <label for="email" class="col-md-3">Account Name</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "accountName" type = "text" required ="" placeholder="Bank Account Name" value = "<?php echo set_value('accountName', $user->accountName) ?>">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-md-3"> Account Number</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "accountNumber" maxlength="10" type = "number" placeholder="Bank Account Number" value = "<?php echo set_value('accountNumber', $user->accountNumber) ?>" required = "">    
                                            </div>
                                        </div>
                                       
                                        <hr/>
                                        <h4>Next of Kin Details</h4>
                                        <hr/>
                                        
                                        <div class="form-group">
                                            <label class="col-md-3">Next of Kin Name</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "nok_name" type = "text" placeholder="Enter Next of Kin Fullname" value = "<?php echo set_value('nok_name', $user->nok_name) ?>" required = "">    
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3">Next of Kin Relationship</label>
                                            <div class="col-md-9">
                                            <?php
                                                $relationship = array(''=>'Select Relationship','Father'=>'Father','Mother'=>'Mother','Brother'=>'Brother','Sister'=>'Sister','Spouse'=>'Spouse','Friend'=>'Friend');
                                                echo form_dropdown('nok_relationship', $relationship, set_value('nok_relationship',$user->nok_relationship), 'required="" class="form-control"');
                                            ?>
                                            </div>
                                        </div>                                         

                                        <div class="form-group">
                                            <label for="email" class="col-md-3">Next of Kin Phone</label>
                                            <div class="col-md-9">
                                                <input class="form-control"  name = "nok_phone" type = "tel" maxlength="11" required ="" placeholder="Next of Kin Phone" value = "<?php echo set_value('nok_phone', $user->nok_phone) ?>">
                                            </div>
                                        </div>

                                        
                                        <div class="form-group">
                                            <label class="col-md-3"> Next of Kin Address</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" name="nok_address" required=""><?=set_value('nok_address',$user->nok_address);?></textarea>
                                            </div>
                                        </div>
                                        
                                        <h4>Admin Decision</h4>
                                        <div class="form-group">
                                            <label class="col-md-3">Account Status</label>
                                            <div class="col-md-9">
                                            <?php
                                                $relationship = array(''=>'Select Status','0'=>'Deactivate','1'=>'Activate');
                                                echo form_dropdown('status', $relationship, set_value('status',$user->status), 'required="" class="form-control"');
                                            ?>
                                            </div>
                                        </div>                                        
                                        <div class="pull-right">
                                            <button class="btn btn-primary" type="submit"> <i class="fa fa-save"></i> Update Profile</button>
                                        </div>
                                </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>
                    
            </div>
        </div>
