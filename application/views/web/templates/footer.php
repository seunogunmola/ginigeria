<!-- footer -->
    <footer class="cpy-right bg-theme text-center">
        <div class="container">
            <a href="#home" class="move-top text-center"><span class="fa fa-angle-double-up" aria-hidden="true"></span></a>
            <div class="footer-logo">
                <h2>
                    <a class="navbar-brand" href="<?= site_url(); ?>">
                        <img src="<?= getResource('images/logo.png');?>" style="width:250px;"/>
                    </a>
                </h2>
            </div>
            <div class="wthree-social">
                <ul>
                    <li>
                        <a href="#">
                            <span class="fa fa-facebook-f icon_facebook"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="fa fa-twitter icon_twitter"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="fa fa-dribbble icon_dribbble"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="fa fa-google-plus icon_g_plus"></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
    <div class="text-center py-3">
        <p class="cpy-wthree"> <?= $sitename; ?> © <?= date('Y');?>. All rights reserved | 
            <a href="http://bit.ly/buildMyWebsite" target="_blank" class="color-theme"> Click here to Get a Website like this one..</a>
        </p>
    </div>
    
</body>

</html>