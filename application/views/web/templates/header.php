<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= $sitename; ?> | Home </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <meta name="keywords" content="<?=$description;?>" />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Custom Theme files -->
    <link href="<?= getResource('web/css/bootstrap.css'); ?>" type="text/css" rel="stylesheet" media="all">
    <link href="<?= getResource('web/css/style.css');?>" type="text/css" rel="stylesheet" media="all">
    <!-- navigation -->
    <link href="<?= getResource('web/css/nav.css'); ?>" type="text/css" rel="stylesheet" media="all">
    <!-- font-awesome icons -->
    <link href="<?= getResource('web/css/font-awesome.min.css');?>" rel="stylesheet">
    <!-- online-fonts -->
    <!--<link href="//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">-->
    <!--<link href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">-->
</head>

<body>
    <div id="home">
        <!-- header -->
        <header>
            <div class="container">
                <div class="header d-lg-flex justify-content-between align-items-center">
                    <div class="header-wthree">
                        <h1>
                            <a class="navbar-brand editContent logo" href="<?= site_url(); ?>">
                                <img src="<?= getResource('images/logo.png');?>" style="width:250px;"/>
                            </a>
                        </h1>
                    </div>
                    <div class="nav_w3ls">
                        <nav>
                            <input class="menu-btn" type="checkbox" id="menu-btn" />
                            <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
                            <ul class="menu">
                                <li class="nav-item active"><a class="nav-link" href="<?= site_url(); ?>">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="#about">About</a></li>
                                <li class="nav-item"><a class="nav-link" href="#services">Services</a></li>
                                <li class="nav-item"><a class="nav-link" href="#stats">Our Impacts</a></li>
                                <li class="nav-item"><a class="nav-link" target="_blank" href="<?= site_url('portal/auth/register'); ?>">Register</a></li>
<!--                                <li class="dropdown nav-item">
                                    <a href="#" class="dropbtn nav-link">Dropdown</a>
                                    <div class="dropdown-content  show">
                                        <a href="#plans" class="nav-link">Pricing</a>
                                        <a class="nav-link" href="#portfolio">Portfolio</a>
                                        <a href="#stats" class="nav-link">stats</a>
                                        <a href="#testi" class="nav-link">Testimonials</a>
                                    </div>
                                </li>-->
                                <li class="nav-item"><a class="nav-link" href="#team">Team</a></li>
                                <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li>

                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!-- //header -->