<!-- Banner Area --> 
<section class="banner_area">
    <div class="container">
        <h6>About Us</h6>
        <ol class="breadcrumb">
            <li><a href="<?=site_url();?>">Home</a></li>     
            <li><a href="<?=site_url('web/about');?>" class="active">About Us</a></li> 
        </ol>
    </div>
</section>
<!-- Banner Area --> 

<!-- Previous Record -->
<section class="previous_record_area pra_2">
    <div class="previous_right">
        <div class="previous_content"> 
            <div class="alons_text">
                <h4>About Us</h4>
                <p>
                    We are the best and most reliable hub to your wealth creation. 
                    Be a part of our  wealth creating community to enjoy the following financial services /offers.
                    As a member you are entitled to the following and lots more:      
                <ol>
                    <li>
                        Buy a share of #10,000.00 per annum payable on monthly installment basis
                    </li>
                    <li>
                        Save minimum of #2,000.00 per month, #500.00 per week, #100 per day and be on your way to building an enduring wealth.                                         
                    </li>
                    <li>
                        Save and get loan of times two or times three of your savings as the case  may be.                   
                    </li>
                    <li>
                        Purchase goods, food stuffs, and other items based on your networth and pay back later. 
                    </li>
                    <li>
                        Capacity Based Lending as needs arises regardless of your savings   (Terms and Conditions apply).              
                    </li>
                    <li>
                        Enjoy profit sharing as dividend on yearly basis.
                    </li>
                </ol>
                <a href="#" class="theme_btn">Apply Now</a>
                </p>
                <!--<a href="#" class="theme_btn">Read more details</a>-->
            </div>
        </div>

        <div class="previous_img_2">
            <img src="<?= getResource('web/images/about.jpg') ?>" alt="">
        </div>
    </div>
</section>
<!-- Previous Record --> 

<!-- Our Purpose Area -->
<section class="purpose_area">
    <div class="container">
        <div class="media mission">
            <img src="<?= getResource('web/images/mission.jpg'); ?>" alt="">
            <div class="media-body">
                <h2 class="p-0">OUR MISSION, VISION AND VALUES</h2>                
                
                <h2>Our Vision</h2>
                <p>
                    Manifolds Multipurpose Cooperative Society creates financial inclusion and promotes the economic interests of its members, customers, and shareholders.                    
                </p>
                
                <h2>Our Mission</h2>
                <p>We help you grow your wealth.</p>
                
                <h2>Our Core Values</h2>
                <ol>
                    <li>Excellent service delivery</li>
                    <li>Timelinessand cost effective</li>
                    <li>Excellent Customer Service</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- End Our Purpose Area -->


<section class="join_conversation">
    <div class="container">
        <h2 class="p-0" style="text-align:center;">OUR EXECUTIVES </h2>
        <div class="owl-carousel conversation">
            <div class="item">
                <img src="<?= getResource('web/images/board/tundeOmolere.jpeg');?>" alt="">  
                <h4 style="text-align:center"> Omolere Tunde <br/>( Chairman ) </h4>
                <div class="hover_item">
                    <a href="#" class="name_item"> Omolere Tunde <small>( Chairman )</small></a>
                    <ul class="socail_icon">
                        <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="item"> 
                <img src="<?= getResource('web/images/board/rotimiSonde.jpeg');?>" alt=""  style="max-height:350px;"> 
                <h4 style="text-align:center"> Sonde Rotimi <br/>( Vice Chairman ) </h4>
                <div class="hover_item">
                    <a href="#" class="name_item">Sonde Rotimi <small>( Vice Chairman )</small></a>
                    <ul class="socail_icon">
                        <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="item">
                <img src="<?= getResource('web/images/board/sanusiLateef.jpeg');?>" alt=""  style="max-height:350px;">
                <h4 style="text-align:center"> Barrister Sanusi Lateef <br/>( General Secretary ) </h4>
                <div class="hover_item">
                    <a href="#" class="name_item">Barrister Sanusi Lateef<small>( General Secretary)</small></a>
                    <ul class="socail_icon">
                        <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="item">
                <img src="<?= getResource('web/images/board/olagbamiAdetoun.jpeg');?>" alt=""  style="max-height:350px;">  
                <h4 style="text-align:center"> Olagbami Adetoun <br/>( Assitant General Secretary ) </h4>
                <div class="hover_item">
                    <a href="#" class="name_item">Olagbami Adetoun<small>( Assitant General Secretary)</small></a>
                    <ul class="socail_icon">
                        <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="item">
                <img src="<?= getResource('web/images/board/adebayoSunday.jpeg');?>" alt="" style="max-height:350px;">
                <h4 style="text-align:center"> Adebayo Sunday <br/>( Treasurer ) </h4>
                <div class="hover_item">
                    <a href="#" class="name_item">Adebayo Sunday  <small>( Treasurer)</small></a>
                    <ul class="socail_icon">
                        <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="item">                
                <img src="<?= getResource('web/images/board/olaFikayo.jpeg');?>" alt="" style="max-height:350px;">
                <h4 style="text-align:center"> Ola Fikayo <br/>( Financial secretary ) </h4>
                <div class="hover_item">
                    <a href="#" class="name_item">Ola Fikayo<small>( Financial secretary )</small></a>
                    <ul class="socail_icon">
                        <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
                    </ul>
                </div>
            </div> 

        </div>
    </div>
</section>

<!--OUR LAWS-->
<section class="join_conversation" style="padding:2em;">
    <div class="container">
        <h2 class="p-0" style="text-align:center;">OUR TERMS AND BYE LAW</h2>
        <div class="row">
            <div class="col-md-6">
                <h4 style="color:crimson;"> Our Bye Law </h4>
                <br/>
                <a href="<?= getResource('/uploads/documents/ManifoldSpectrumByeLaws.pdf')?>"> <button class="btn btn-primary">Download Our Byelaw <i class="fa fa-download"></i> </button> </a>
                <br/>
                <!--Read it Online-->
                <!--<iframe src="<?= getResource('/uploads/documents/ManifoldSpectrumByeLaws.pdf')?>" width="100%" height="500px"></iframe>-->
            </div>
            <div class="col-md-6">
                <h4 style="color:blue;"> Our Terms </h4>
                <br/>
                <a href="<?= getResource('/uploads/documents/ManifoldSpectrumTerms.pdf')?>"> <button class="btn btn-primary">Download Our Terms <i class="fa fa-download"></i> </button> </a>
                <br/>
                <!--Read it Online-->                
                <!--<iframe style="border:2px solid crimson;" src="<?= getResource('/uploads/documents/ManifoldSpectrumTerms.pdf')?>" width="100%" height="500px"></iframe>-->
            </div>
        </div>
    </div>
</section>