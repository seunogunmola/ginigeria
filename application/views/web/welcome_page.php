        <!-- banner -->
        <div class="banner">
            <div class="container">
                <div class="banner-text">
                    <div class="slider-info">
                        <h3>Graduates Initiative Network</h3>
                        <span class="line"></span>
                        <p>Largest channel management network in Africa</p>
                        <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll bg-theme" href="<?= site_url('portal/auth/register'); ?>" role="button">Get Started</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- //banner -->
    </div>
    <!--  about -->
    <section class="wthree-slide-btm pt-lg-5" id="about">
        <div class="container pt-sm-5 pt-4">
            <div class="title-desc text-center pb-3 pb-lg-5 mb-lg-5">
                <h3 class="main-title-w3pvt">about us</h3>
                <p>Largest channel management network in Africa</p>
            </div>
            <div class="row flex-row-reverse no-gutters">
                <div class="col-lg-5">
                    <img src="<?= getResource('web/images/unemployedGraduates.jpg');?>" style="width:100%;height:100%;" />
                </div>
                <div class="col-lg-7">
                    <div class="bg-abt">
                        <div class="container">
                            <div class="title-desc  pb-sm-3">
                                <h4 class="main-title-w3pvt">Background</h4>
                            </div>
                            <div class="row flex-column mt-lg-4 mt-3">
                                <div class="abt-grid">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="abt-icon">
                                                <span class="fa fa-ravelry"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-9 pl-sm-0">
                                            <div class="abt-txt ml-sm-0">
                                                <p>
Graduate initiative Network is a Business setup to solve the greatest challenges of unemployment facing the Nigerian Government and Citizens. <br/>
This initiative is the first of its kind, targeting 5,000,000 graduates/youth for self SME’S.
We realized that over 5million youth mostly graduate are either not working or out of job and this number can actually produce a positive impact in our economy if manage collectively than individuals struggling as our collective effort will produce Synergy and individuals growth beyond their imagination. <br/>
We have established business network and still building relationship with different manufacturing and servicing companies in Nigeria, Africa, Europe and America on this Project.                                                    
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="wthree-slide-btm pb-lg-5">
        <div class="container pb-md-5 pb-4">
            <div class="row no-gutters">
                <div class="col-lg-5">
                    <img src="<?= getResource('web/images/opportunity.jpg');?>" style="width:100%;height:100%;" />
                </div>
                <div class="col-lg-7">
                    <div class="bg-abt ">
                        <div class="container">
                            <div class="title-desc  pb-sm-3">
                                <h4 class="main-title-w3pvt"> The Situation / Opportunities</h4>
                            </div>
                            <div class="row flex-column mt-lg-4 mt-3">
                                <div class="abt-grid">
                                    <div class="row">
                                        <div class="col-sm-9 pl-sm-0">
                                            <div class="abt-txt ml-sm-0">
                                                <p>
Most wholesale, direct purchase from the manufacturer requires some initial deposit, guarantor, Insurance and collateral, creating a barrier to entry.<br/>
Most company has the capacity to produce / expand their product if there is an assurance of increase in sales Nigeria is fast becoming the emerging market, our over 160million as research creating a global market for all products globally. Foreign companies want to come to Nigeria to do Business with one common challenge on how to win the market.                                                    
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="abt-icon">
                                                <span class="fa fa-binoculars"></span>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wthree-slide-btm pb-lg-5" style="margin-top:-100px;">
        <div class="container pb-md-5 pb-4">
            <div class="row no-gutters">
<div class="col-lg-7">
                    <div class="bg-abt ">
                        <div class="container">
                            <div class="title-desc  pb-sm-3">
                                <h4 class="main-title-w3pvt"> Our Solution</h4>                                
                            </div>
                            <div class="row flex-column mt-lg-4 mt-3">
                                <div class="abt-grid">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="abt-icon">
                                                <span class="fa fa-ravelry"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-9 pl-sm-0">
                                            <div class="abt-txt ml-sm-0">
                                                <p>
Our job is to help indigenous and foreign business discover new market, with our large network that cut across all state of the federation.
Developing and managing the market channel for their extra capacity production.
To create other source of income for the working class 
To create social services for the unemployed from excess profit
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
                <div class="col-lg-5">
<img src="<?= getResource('web/images/solution.jpg');?>" style="width:100%;max-height:300px;" />
                </div>
                
            </div>
        </div>
    </section>
    
    

    
    <!-- //about -->
    <!-- slide -->
    <div class="abt_bottom py-lg-5 bg-theme">
        <div class="container py-sm-4 py-3">
            <h4 class="abt-text text-capitalize text-sm-center">If you are still unemployed, it’s your choice.</h4>
            <div class="d-sm-flex justify-content-center">
                <a class="btn  mt-4 w3_pvt-link-bnr scroll" href="<?= site_url('portal/auth/register'); ?>" role="button">Get Started
                </a>
            </div>
        </div>
    </div>
    <!-- //slide -->
    <!-- services -->
    <div class="w3lspvt-about py-md-5 py-5" id="services">
        <div class="container pt-lg-5">
            <div class="title-desc text-center pb-3 pb-lg-5">
                <h3 class="main-title-w3pvt">Our Services</h3>
                <p>Inspired By Passion. Driven By Results.</p>
            </div>
            <div class="w3lspvt-about-row row  text-center pt-md-0 pt-5 mt-lg-5">
                <div class="col-lg-4 col-sm-6 w3lspvt-about-grids">
                    <div class="p-md-5 p-sm-3">
                        <span class="fa fa-map-marker"></span>
                        <h4 class="mt-2 mb-3">VOLUNTARY SERVICES </h4>
                        <p>
                            Powered by foundation  for the unemployed graduate
                        <ul style="list-style-type: square;">
                            <li>
                                Mass Literacy. 
                            </li>
                            <li>
                                HIV/AIDS campaign.
                            </li>
                            <li>
                                Rescue & first Aids.
                            </li>
                            <li>
                                Environmental / sanitary services.
                            </li>
                            <li>
                                Social health worker.
                            </li>
                            <li>
                                Counselor team.
                            </li>
                            <li>
                                Support member’s unemployed graduates to raise capital for business (SME’S).  
                            </li>
                        </ul>
                            





                          
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 w3lspvt-about-grids  border-left border-right my-sm-0 my-5">
                    <div class="p-md-5 p-sm-3">
                        <span class="fa fa-check-circle-o"></span>
                        <h4 class="mt-2 mb-3">FOUNDATION  FOR UNEMPLOYED GRADUATES</h4>
                        <ul style="list-style-type: circle;">
                            <li> Provides welfare services for the unemployed graduates. </li>
                            <li>Organizes free training & skill acquisition programme.    </li>                        
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 w3lspvt-about-grids">
                    <div class="p-md-5 p-sm-3">
                        <span class="fa fa-money"></span>
                        <h4 class="mt-2 mb-3">Investment</h4>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <!-- //services -->
    <!-- stats -->
    <section class="w3_stats py-sm-5 py-4" id="stats">
        <div class="container">
            <div class="py-lg-5 w3-stats">
                <h4 class="w3pvt-title"> Our Impact Stats</h4>
                <p class="my-4 color-white stat-txt"></p>
                <div class="row py-4">
                    <div class="col-md-3 col-6">
                        <div class="counter">
                            <span class="fa fa-users"></span>
                            <div class="timer count-title count-number mt-2 color-white">
                                <h6>133</h6>
                            </div>
                            <p class="count-text">Registered Graduates</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="counter">
                            <span class="fa fa-money"></span>
                            <div class="timer count-title count-number mt-2 color-white">
                                <h6>533</h6>
                            </div>
                            <p class="count-text">Investors</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-6 mt-md-0 mt-4">
                        <div class="counter">
                            <span class="fa fa-building"></span>
                            <div class="timer count-title count-number mt-2 color-white">
                                <h6>243</h6>
                            </div>
                            <p class="count-text">Businesses</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-6 mt-md-0 mt-4">
                        <div class="counter">
                            <span class="fa fa-thumbs-up"></span>
                            <div class="timer count-title count-number mt-2 color-white">
                                <h6>13</h6>
                            </div>
                            <p class="count-text">Partners</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- //stats -->
    <!-- team -->
    <section class="team py-4 py-lg-5" id="team">
        <div class="container py-lg-5 py-sm-4">
            <div class="title-desc text-center pb-sm-3">
                <h3 class="main-title-w3pvt">our team</h3>
<!--                <p>Inspired By Passion. Driven By Results.</p>-->
            </div>
            <div class="row py-4 mt-lg-5  team-grid">
                <div class="col-lg-4 col-sm-6">
                    <div class="box13">
                        <img src="images/t1.jpg" class="img-fluid img-thumbnail" alt="" />
                        <div class="box-content">
                            <h3 class="title">Williamson</h3>
                            <span class="post">role in detail</span>
                            <ul class="social">
                                <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- //team -->
    <!-- testimonials -->
    <div class="testimonials py-lg-5 py-4" id="testi">
        <div class="container">
            <div class="title-desc text-center pb-3 pb-lg-5">
                <h3 class="main-title-w3pvt">testimonials</h3>
                <p>Inspired By Passion. Driven By Results.</p>
            </div>
            <div class="row mt-lg-5">
                <div class="col-lg-4">
                    <div class="testimonials_grid">
                        <div class="testi-text text-center">
                            <p><span class="fa fa-quote-left"></span>Stet clita kasd gubergren, no sea
                                takimata sanctus est Lorem ipsum dolor sit amet<span class="fa fa-quote-right"></span>
                            </p>
                        </div>
                        <div class="d-flex align-items-center justify-content-center">
                            <div class="testi-desc">
                                <span class="fa fa-user"></span>
                                <h5>Aliquyam</h5>
                                <p>some text</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonials_grid my-lg-0 my-4">
                        <div class="testi-text text-center">
                            <p><span class="fa fa-quote-left"></span>Stet clita kasd gubergren, no sea
                                takimata sanctus est Lorem ipsum dolor sit amet<span class="fa fa-quote-right"></span>
                            </p>
                        </div>
                        <div class="d-flex align-items-center justify-content-center">
                            <div class="testi-desc">
                                <span class="fa fa-user"></span>
                                <h5>Aliquyam</h5>
                                <p>some text</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonials_grid">
                        <div class="testi-text text-center">
                            <p><span class="fa fa-quote-left"></span>Stet clita kasd gubergren, no sea
                                takimata sanctus est Lorem ipsum dolor sit amet<span class="fa fa-quote-right"></span>
                            </p>
                        </div>
                        <div class="d-flex align-items-center justify-content-center">
                            <div class="testi-desc">
                                <span class="fa fa-user"></span>
                                <h5>Aliquyam</h5>
                                <p>some text</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
    </div>
    <!-- //testimonials -->
   
    <!-- //slide -->
    <!-- contact -->
    <section class="contact-wthree py-sm-5 py-4" id="contact">
        <div class="container pt-lg-5">
            <div class="title-desc text-center pb-sm-3">
                <h3 class="main-title-w3pvt">contact us</h3>                
            </div>
            <div class="row mt-4">
                <div class="col-lg-5 text-center">
                    <h5 class="cont-form">get in touch</h5>
                    <div class="row flex-column">
                        <div class="contact-w3">
                            <span class="fa fa-envelope-open  mb-3"></span>
                            <div class="d-flex flex-column">
                                <a href="mailto:example@email.com" class="d-block">info@ginigeria.com</a>
                            </div>
                        </div>
                        <div class="contact-w3 my-4">
                            <span class="fa fa-phone mb-3"></span>
                            <div class="d-flex flex-column">
                                <p>+234 123 7890</p>
                            </div>
                        </div>
                        <div class="contact-w3">
                            <span class="fa fa-home mb-3"></span>
                            <address>Lagos Nigeria</address>
                        </div>
                    </div>

                </div>
                <div class="col-lg-7">
                    <h5 class="cont-form">contact form</h5>
                    <div class="contact-form-wthreelayouts">
                        <form action="#" method="post" class="register-wthree">
                            <div class="form-group">
                                <label>
                                    Your Name
                                </label>
                                <input class="form-control" type="text" placeholder="Your Name" name="name" required="">
                            </div>
                            <div class="form-group">
                                <label>
                                    Mobile
                                </label>
                                <input class="form-control" type="text" placeholder="Your Phone Number" name="phone" required="">
                            </div>
                            <div class="form-group">
                                <label>
                                    Email
                                </label>
                                <input class="form-control" type="email" placeholder="Your Email" name="email" required="">
                            </div>
                            <div class="form-group">
                                <label>
                                    Your message
                                </label>
                                <textarea placeholder="Type your message here" name="message" class="form-control"></textarea>
                            </div>
                            <div class="form-group mb-0">
                                <button type="submit" class="btn btn-w3layouts btn-block  bg-theme1 color-white w-100 font-weight-bold text-uppercase">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- //contact -->