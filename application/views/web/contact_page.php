    <!-- Slider Area -->
    <section class="main_slider_area"> 
        <div id="main_slider" class="rev_slider" data-version="5.1.1RC">
            <ul> 
                <!-- SLIDE 1 -->  
                <li data-index="rs-1" data-transition="fade" data-slotamount="default"  data-easein="default" data-easeout="default" data-masterspeed="2000"  data-thumb="images/slider-1.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Business" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="<?= getResource('web/images/sliders/1.jpg')?>"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                        
                    <!-- LAYER NR. 2 -->   
                    <div class="tp-caption concept_title" 
                        id="slide-1-layer-2" 
                        data-x="['left','left','left','left']" 
                        data-hoffset="['0','0','0','15']" 
                        data-y="['top','top','top','top','top']" 
                        data-voffset="['390','390','300','180']" 
                        data-fontsize="['60','60','50','35','25']" 
                        data-lineheight="['64','64','64','50']" 
                        data-width="['none','none','none','485','350']" 
                        data-height="none" 
                        data-whitespace="['nowrap','nowrap','nowrap','normal']" 
                        data-transform_idle="o:1;"  
                        data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeOut;" 
                        data-transform_out="opacity:0;s:1000;s:1000;" 
                        data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                        data-start="1000" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" 
                        style="z-index: 7; white-space: nowrap; text-align:left;"> Manifolds Multipurpose
                    </div>
                    <!-- LAYER NR. 3 --> 
                    <div class="tp-caption concept_title" 
                        id="slide-1-layer-3" 
                        data-x="['left','left','left','left']" 
                        data-hoffset="['0','0','0','15']" 
                        data-y="['top','top','top','top']" 
                        data-voffset="['455','455','360','220']" 
                        data-fontsize="['60','60','50','35','25']" 
                        data-lineheight="['64','64','64','50']" 
                        data-width="['none','none','none','490','490']" 
                        data-height="none" 
                        data-whitespace="['nowrap','nowrap','nowrap','normal']" 
                        data-transform_idle="o:1;"   
                        data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeOut;" 
                        data-transform_out="opacity:0;s:1000;s:1000;" 
                        data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                        data-start="1300" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" 
                        style="z-index: 7; white-space: nowrap; text-align:left;"> Cooperative <span>Society Ltd.</span>
                    </div>
                    <!-- LAYER NR. 4 -->  
                    <div class="tp-caption paragraph_heding" 
                        id="slide-1-layer-4" 
                        data-x="['left','left','left','left']"  
                        data-hoffset="['0','0','0','15']" 
                        data-y="['top','top','top','top']" 
                        data-voffset="['565','565','450','280']"  
                        data-fontsize="['20','20','18','16',16']" 
                        data-lineheight="['30','30','25','20']" 
                        data-width="['640','640','600','500', '400']"
                        data-height="none"
                        data-whitespace="normal"
                        data-transform_idle="o:1;" 
                        data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeOut;" 
                        data-transform_out="opacity:0;s:1000;s:1000;" 
                        data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                        data-start="1500" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on"   
                        style="z-index: 8; min-width: 680px; max-width: 680px; white-space: normal;">We are a reliable hub to your wealth creation. 
                    </div> 
                    
                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption get_started_btn" 
                        id="slide-1-layer-5" 
                        data-x="['left','left','left','left']" 
                        data-hoffset="['0','0','0','15']" 
                        data-y="['bottom','bottom','bottom','bottom']" 
                        data-voffset="['150','150','100','70']"  
                        data-width="none" data-height="none" data-whitespace="nowrap" 
                        data-transform_idle="o:1;" 
                        data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeOut;" 
                        data-transform_out="opacity:0;s:1000;s:1000;" 
                        data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                        data-start="1700" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on"
                        style="z-index: 6; white-space: nowrap;">
                        <a href="services.html" class="theme_btn">All Services</a> 
                    </div>  
                    
                    <!-- LAYER NR. 6 -->
                    <div class="tp-caption get_started_btn" 
                        id="slide-1-layer-6" 
                        data-x="['left','left','left','left']" 
                        data-hoffset="['220','220','220','235']" 
                        data-y="['bottom','bottom','bottom','bottom']" 
                        data-voffset="['150','150','100','70']"  
                        data-width="none" data-height="none" data-whitespace="nowrap" 
                        data-transform_idle="o:1;" 
                        data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeOut;" 
                        data-transform_out="opacity:0;s:1000;s:1000;" 
                        data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                        data-start="1700" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on"
                        style="z-index: 6; white-space: nowrap;">
                        <a href="#" class="theme_btn apply_btn">Register Now</a> 
                    </div>  
                </li>  
                <!-- SLIDE 2 -->  
                <li data-index="rs-2" data-transition="slideoverup" data-slotamount="default"  data-easein="default" data-easeout="default" data-masterspeed="2000"  data-thumb="images/slider-1.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Business" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="<?= getResource('web/images/sliders/2.jpg')?>"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                        
                    <!-- LAYER NR. 2 -->   
                    <div class="tp-caption concept_title" 
                        id="slide-1-layer-2" 
                        data-x="['left','left','left','left']" 
                        data-hoffset="['0','0','0','15']" 
                        data-y="['top','top','top','top','top']" 
                        data-voffset="['390','390','300','180']" 
                        data-fontsize="['60','60','50','35','25']" 
                        data-lineheight="['64','64','64','50']" 
                        data-width="['none','none','none','485','350']" 
                        data-height="none" 
                        data-whitespace="['nowrap','nowrap','nowrap','normal']" 
                        data-transform_idle="o:1;"  
                        data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeOut;" 
                        data-transform_out="opacity:0;s:1000;s:1000;" 
                        data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                        data-start="1000" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" 
                        style="z-index: 7; white-space: nowrap; text-align:left;"> Manifolds Multipurpose
                    </div>
                    <!-- LAYER NR. 3 --> 
                    <div class="tp-caption concept_title" 
                        id="slide-1-layer-3" 
                        data-x="['left','left','left','left']" 
                        data-hoffset="['0','0','0','15']" 
                        data-y="['top','top','top','top']" 
                        data-voffset="['455','455','360','220']" 
                        data-fontsize="['60','60','50','35','25']" 
                        data-lineheight="['64','64','64','50']" 
                        data-width="['none','none','none','490','490']" 
                        data-height="none" 
                        data-whitespace="['nowrap','nowrap','nowrap','normal']" 
                        data-transform_idle="o:1;"   
                        data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeOut;" 
                        data-transform_out="opacity:0;s:1000;s:1000;" 
                        data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                        data-start="1300" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" 
                        style="z-index: 7; white-space: nowrap; text-align:left;"> Cooperative <span>Society Ltd.</span>
                    </div>
                    <!-- LAYER NR. 4 -->  
                    <div class="tp-caption paragraph_heding" 
                        id="slide-1-layer-4" 
                        data-x="['left','left','left','left']"  
                        data-hoffset="['0','0','0','15']" 
                        data-y="['top','top','top','top']" 
                        data-voffset="['565','565','450','280']"  
                        data-fontsize="['20','20','18','16',16']" 
                        data-lineheight="['30','30','25','20']" 
                        data-width="['640','640','600','500', '400']"
                        data-height="none"
                        data-whitespace="normal"
                        data-transform_idle="o:1;" 
                        data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeOut;" 
                        data-transform_out="opacity:0;s:1000;s:1000;" 
                        data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                        data-start="1500" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on"   
                        style="z-index: 8; min-width: 680px; max-width: 680px; white-space: normal;">We help You grow your wealth
                    </div> 
                    
                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption get_started_btn" 
                        id="slide-1-layer-5" 
                        data-x="['left','left','left','left']" 
                        data-hoffset="['0','0','0','15']" 
                        data-y="['bottom','bottom','bottom','bottom']" 
                        data-voffset="['150','150','100','70']"  
                        data-width="none" data-height="none" data-whitespace="nowrap" 
                        data-transform_idle="o:1;" 
                        data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeOut;" 
                        data-transform_out="opacity:0;s:1000;s:1000;" 
                        data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                        data-start="1700" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on"
                        style="z-index: 6; white-space: nowrap;">
                        <a href="services.html" class="theme_btn">All Services</a> 
                    </div>  
                    
                    <!-- LAYER NR. 6 -->
                    <div class="tp-caption get_started_btn" 
                        id="slide-1-layer-6" 
                        data-x="['left','left','left','left']" 
                        data-hoffset="['220','220','220','235']" 
                        data-y="['bottom','bottom','bottom','bottom']" 
                        data-voffset="['150','150','100','70']"  
                        data-width="none" data-height="none" data-whitespace="nowrap" 
                        data-transform_idle="o:1;" 
                        data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeOut;" 
                        data-transform_out="opacity:0;s:1000;s:1000;" 
                        data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                        data-start="1700" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on"
                        style="z-index: 6; white-space: nowrap;">
                        <a href="#" class="theme_btn apply_btn">Register Now</a> 
                    </div>  
                </li>  
 
            </ul> 
            <div class="tp-bannertimer tp-bottom"></div>	 
        </div>
    </section>
    <!-- End Slider Area -->  



    <!-- About  -->
    <section class="previous_record_area">
        <div class="previous_img">
            <img src="<?= getResource('web/images/about.jpg')?>" alt="">
        </div>
        <div class="previous_right">
            <div class="alons_text">
                <h4>Welcome to Manifold Multipurpose Cooperative  society Ltd</h4>
                <p>
We are the best and most reliable hub to your wealth creation. 
Be a part of our  wealth creating community to enjoy the following financial services /offers.
As a member you are entitled to the following and lots more:      
                <ol>
                    <li>
                        Buy a share of #10,000.00 per annum payable on monthly installment basis
                    </li>
                    <li>
                        Save minimum of #2,000.00 per month, #500.00 per week, #100 per day and be on your way to building an enduring wealth.                                         
                    </li>
                    <li>
                        Save and get loan of times two or times three of your savings as the case  may be.                   
                    </li>
                    <li>
                        Purchase goods, food stuffs, and other items based on your networth and pay back later. 
                    </li>
                    <li>
                        Capacity Based Lending as needs arises regardless of your savings   (Terms and Conditions apply).              
                    </li>
                    <li>
                        Enjoy profit sharing as dividend on yearly basis.
                    </li>
                </ol>
                <a href="#" class="theme_btn">Apply Now</a>
                </p>
                <!--<a href="#" class="theme_btn">Read more details</a>-->
            </div>

        </div>
    </section>
    <!-- Previous Record -->
    
        <!-- Achieve your financial -->
    <section class="achieve_financial_area">
        <div class="container">
            <div class="tittle">
                <img src="<?= getResource('web/images/icons/savings.png');?>" alt="">
                <h2>At Manifolds Cooperative,<span>We help you grow your wealth.</span></h2>
            </div>
            <div class="row achieve_inner">
                <div class="col-lg-4 col-md-6 wow fadeIn">
                    <div class="achieve"> 
                        <div class="icon_img"><img src="<?= getResource('web/images/icons/loan.png')?>" alt=""></div>
                        <a href="#">Get Fast Loans of #10,000 to #200,000</a>
                        <p>Coming Soon</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow fadeIn" data-wow-delay="0.3s">
                    <div class="achieve">
                        <div class="icon_img"><img src="<?=getResource('web/images/icons/savings.png')?>" alt=""></div> 
                        <a href="#">Save and get loan of times two or times three of your savings </a>
                        
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow fadeIn" data-wow-delay="0.6s">
                    <div class="achieve">
                        <div class="icon_img"><img src="<?=getResource('web/images/icons/card.png')?>" alt=""></div> 
                        <a href="#">Purchase Goods from your savings and pay back later.</a>
                        <!--<p>Get Credit Card Within 24 hours ! Up to 98% Cash Withdrawal Facilities by <br> Card Cheque</p>-->
                    </div>
                </div>
            </div>
            <div class="row achieve_inner">
                <div class="col-lg-4 col-md-6 wow fadeIn">
                    <div class="achieve"> 
                        <div class="icon_img"><img src="<?= getResource('web/images/icons/loan2.png')?>" alt=""></div>
                        <a href="#"> Save a minimum of N2000 monthly or #500 weekly or N100  daily</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow fadeIn" data-wow-delay="0.3s">
                    <div class="achieve">
                        <div class="icon_img"><img src="<?=getResource('web/images/icons/money-bag.png')?>" alt=""></div> 
                        <a href="#">Buy Shares of #10,000 per annum.</a>
                        
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow fadeIn" data-wow-delay="0.6s">

                </div>
            </div>
        </div>
    </section>
     
	<!-- Application Process-->
	<div class="application_process">
		<div class="container">
			<div class="tittle la">
				<h2>How to<span>become a Member</span></h2>
			</div>
			<div class="row">
				<!-- Step -->
				<div class="col-lg-4 col-md-6 wow fadeIn">
					<div class="step_item">
						<h3>Step 1</h3>
                                                <img src="<?= getResource('web/images/icons/step-1.png')?>" alt="">
						<p>Apply online in minutes</p>
						<a href="#" class="theme_btn">Apply Now</a>
					</div>
				</div>
				<!-- Step -->
				<div class="col-lg-4 col-md-6 wow fadeIn" data-wow-delay="0.3s">
					<div class="step_item">
						<h3>Step 2</h3>
						<img src="<?= getResource('web/images/icons/step-2.png')?>" alt="">
						<p>We Review your Application<br> <br></p>
					</div>
				</div>
				<!-- Step -->
				<div class="col-lg-4 col-md-6 wow fadeIn" data-wow-delay="0.6s">
					<div class="step_item">
						<h3>Step 3</h3>
						<img src="<?= getResource('web/images/icons/step-3.png')?>" alt="">
						<p>Get approved and Start Enjoying our packages</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Application Process-->
	
	<!-- Testimonial Area -->
	<section class="testimonial_area">
		<div class="container">
			<div class="tittle">
				<h2>Testimonials<span>from our members</span></h2>
			</div>
			<div class="owl-carousel testimonial">
				<div class="item">
					<a href="#">Member Name</a>
					<div class="client_info fast_i">
						<img src="images/testimonial-1.jpg" alt="">
						<h4>Occupation</h4>
						<p>Testimonial Goes here</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Testimonial Area -->
	

	
	<!-- Get started -->
	<section class="get_started_area">
		<div class="container">
			<h2>Ready to become a Member? </h2>
                        <br/>
			<a href="#" class="theme_btn">Click here to Apply </a>
		</div>
	</section>
	<!-- End get started -->
