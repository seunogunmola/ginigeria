<section class="banner_area">
    <div class="container">
        <h6>Register</h6>
        <h2>Become a Member</h2>
        <ol class="breadcrumb">
            <li><a href="<?= site_url(); ?>">Home</a></li>     
        </ol>
    </div>
</section>

<!-- Blog Area --> 
<section class="blog_area" id="registrationForm">
    
    <div class="container" >
        <h2>Become a Member</h2>
        <div class="row" >            
            <div class="col-md-6">
                <div style="padding:1em;background-color:lightgrey;">
                    <?= isset($error_message) ? $error_message : '';?>
                    <?= trim($this->session->flashdata('success')) != false ? getAlertMessage($this->session->flashdata('success'),'success') : ''?>
                    <?= trim($this->session->flashdata('error')) != false ? getAlertMessage($this->session->flashdata('error'),'danger') : ''?>                    
                <form action="" method="post" autocomplete="off">
                    <div class="form-group">
                        <label for="email"> Full Name  </label>
                        <div class="input-group col-lg-12">
                            <input type="text" name="fullname" class="form-control" value="<?=set_value('fullname');?>" placeholder="Your Fullname" required="" autocomplete="off">
                        </div>                      
                    </div>                      
                    <div class="form-group">
                        <label for="phone"> Phone Number </label>
                        <div class="input-group col-lg-12">
                            <input type="tel" name="phone_number" value="<?=set_value('phone_number');?>" maxlength="11" class="form-control" placeholder="Your Phone Number" required="" autocomplete="off">
                        </div>                      
                    </div>                      
                    <div class="form-group">
                        <label for="email"> Email Address </label>
                        <div class="input-group col-lg-12">
                            <input type="email" class="form-control" name="email_address" value="<?=set_value('email_address');?>" placeholder="Your Email address" required="" autocomplete="off">
                        </div>                      
                    </div>                      
                    <div class="form-group">
                        <label for="password"> Choose a Password </label>
                        <div class="input-group col-lg-12">
                            <input type="password" name="password" class="form-control" placeholder="Choose a Password" required="" autocomplete="off">
                        </div>                      
                    </div>                      
                    <div class="form-group">
                        <label for="confirm password"> Confirm Password </label>
                        <div class="input-group col-lg-12">
                            <input type="password" name="confirm_password" class="form-control" placeholder="Confirm Your Password" autocomplete="off">
                        </div>                      
                    </div> 
                    <button class="btn btn-primary" type="submit">
                        Submit Form <i class="fa fa-send"></i>
                    </button>
                </form>  
                </div>
            </div>
            <div class="col-md-6">
            <div class="alons_text">
                <h4>Benefits of our Membership</h4>
                <p>
                <ol>
                    <li>
                        Buy a share of #10,000.00 per annum payable on monthly installment basis
                    </li>
                    <li>
                        Save minimum of #2,000.00 per month, #500.00 per week, #100 per day and be on your way to building an enduring wealth.                                         
                    </li>
                    <li>
                        Save and get loan of times two or times three of your savings as the case  may be.                   
                    </li>
                    <li>
                        Purchase goods, food stuffs, and other items based on your networth and pay back later. 
                    </li>
                    <li>
                        Capacity Based Lending as needs arises regardless of your savings   (Terms and Conditions apply).              
                    </li>
                    <li>
                        Enjoy profit sharing as dividend on yearly basis.
                    </li>
                </ol>
                </p>
                <!--<a href="#" class="theme_btn">Read more details</a>-->
            </div>                
            </div>
        </div>
    </div>
</section> 
<!-- Blog Area --> 