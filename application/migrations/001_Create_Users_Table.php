<?php
    class Migration_Create_Users_Table extends CI_Migration {
    public function up(){
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'BIGINT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'uniqueid' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                        ),
                        'email_address' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'password' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                        ),
                        'title' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                                'NULL'=>TRUE 
                        ),
                        'surname' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'firstname' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'othernames' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'gender' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '6',
                                'NULL'=>TRUE
                        ),
                        'phone_number' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '11',
                                'NULL'=>TRUE
                        ),
                        'dob' => array(
                                'type' => 'DATE',
                                'NULL'=>TRUE
                        ),
                        'profile' => array(
                                'type' => 'TEXT',
                                'NULL'=>TRUE
                        ),
                        'passport' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                                'NULL'=>TRUE
                        ),
                        'privileges' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'status' => array(
                                'type' => 'INT',
                                'constraint' => '1',
                                'default'=>1
                        ),
                    'datecreated timestamp default now()',
                    'datemodified timestamp default now()',

                ));
                    $this->dbforge->add_key('id', TRUE);
                    $this->dbforge->create_table('t_users');
                }

    public function down(){
                $this->dbforge->drop_table('t_users');
        }
}
