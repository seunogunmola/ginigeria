<?php

class Migration_Create_Password_Change_Requests extends CI_Migration {
    public function up(){
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'customerid' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                        ),
                        'token' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                        ),
                        'status' => array(
                                'type' => 'INT',
                                'constraint' => '1',
                                'default'=>'0'
                        ),
                    'datecreated timestamp default now()',
                    'datemodified timestamp default now()',

                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('t_password_change_requests');
                }

    public function down(){
                $this->dbforge->drop_table('t_password_change_requests');
        }

}
