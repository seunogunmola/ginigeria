<?php

class Migration_Create_Audit_Log extends CI_Migration {
    public function up(){
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'action_type' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                        ),
                        'action_details' => array(
                                'type' => 'TEXT',
                        ),
                        'institution_id' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '20',
                                'NULL'=>TRUE
                        ),
                    'datecreated timestamp default now()',
                    'datemodified timestamp default now()',

                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('t_audit_log');
                }

    public function down(){
                $this->dbforge->drop_table('t_audit_log');
        }
}
